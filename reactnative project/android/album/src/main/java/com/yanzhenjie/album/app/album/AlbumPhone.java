package com.yanzhenjie.album.app.album;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.Point;
import android.os.Build;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.yanzhenjie.album.AlbumFolder;
import com.yanzhenjie.album.R;
import com.yanzhenjie.album.api.widget.Widget;
import com.yanzhenjie.album.app.Contract;
import com.yanzhenjie.album.impl.DoubleClickWrapper;
import com.yanzhenjie.album.impl.OnCheckedClickListener;
import com.yanzhenjie.album.impl.OnItemClickListener;
import com.yanzhenjie.album.util.SystemBar;
import com.yanzhenjie.album.widget.ColorProgressBar;
import com.yanzhenjie.album.widget.divider.Api21ItemDivider;

public class AlbumPhone extends Contract.AlbumView implements View.OnClickListener {

    private Activity mActivity;

    private RelativeLayout app_bar_layout;
    private View v_goback;
    private TextView tv_title;
    private LinearLayout ll_capture;

    private RecyclerView mRecyclerView;
    private GridLayoutManager mLayoutManager;
    private AlbumAdapter mAdapter;

    private LinearLayout mLayoutLoading;
    private ColorProgressBar mProgressBar;
    private LinearLayout  linear_path;


    public AlbumPhone(Activity activity, Contract.AlbumPresenter presenter) {
        super(activity, presenter);
        this.mActivity = activity;

        this.app_bar_layout = activity.findViewById(R.id.app_bar_layout);
        this.v_goback = activity.findViewById(R.id.v_goback);
        this.tv_title = activity.findViewById(R.id.tv_title);

        this.ll_capture = activity.findViewById(R.id.ll_capture);
        this.mRecyclerView = activity.findViewById(R.id.recycler_view);

//        this.mBtnSwitchFolder = activity.findViewById(R.id.btn_switch_dir);
        this.mLayoutLoading = activity.findViewById(R.id.layout_loading);
        this.mProgressBar = activity.findViewById(R.id.progress_bar);

        this.linear_path = activity.findViewById(R.id.linear_path);
        this.app_bar_layout.setOnClickListener(new DoubleClickWrapper(this));
//        this.mBtnSwitchFolder.setOnClickListener(this);
//        this.mBtnPreview.setOnClickListener(this);
        this.v_goback.setOnClickListener(this);
        this.ll_capture.setOnClickListener(this);
        this.linear_path.setOnClickListener(this);

    }

    @Override
    protected void onCreateOptionsMenu(Menu menu) {
    }

    @Override
    protected void onOptionsItemSelected(MenuItem item) {
//        int itemId = item.getItemId();
//        if (itemId == R.id.album_menu_finish) {
//            getPresenter().complete();
//        }
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    @Override
    public void setupViews(Widget widget, int column, boolean hasCamera, int choiceMode) {
        SystemBar.setNavigationBarColor(mActivity, widget.getNavigationBarColor());
        int statusBarColor = widget.getStatusBarColor();
        if (widget.getUiStyle() == Widget.STYLE_LIGHT) {
            if (SystemBar.setStatusBarDarkFont(mActivity, true)) {
                SystemBar.setStatusBarColor(mActivity, statusBarColor);
            } else {
                SystemBar.setStatusBarColor(mActivity, getColor(R.color.albumColorPrimaryBlack));
            }

            mProgressBar.setColorFilter(getColor(R.color.albumLoadingDark));

//            Drawable navigationIcon = getDrawable(R.drawable.album_ic_back_white);
//            AlbumUtils.setDrawableTint(navigationIcon, getColor(R.color.albumIconDark));
//            setHomeAsUpIndicator(navigationIcon);
//
//            Drawable completeIcon = mCompleteMenu.getIcon();
//            AlbumUtils.setDrawableTint(completeIcon, getColor(R.color.albumIconDark));
//            mCompleteMenu.setIcon(completeIcon);
        } else {
            mProgressBar.setColorFilter(widget.getToolBarColor());
            SystemBar.setStatusBarColor(mActivity, statusBarColor);
            setHomeAsUpIndicator(R.drawable.album_ic_back_white);
        }


        Configuration config = mActivity.getResources().getConfiguration();
        mLayoutManager = new GridLayoutManager(getContext(), column, getOrientation(config), false);
        mRecyclerView.setLayoutManager(mLayoutManager);
        int dividerSize = getResources().getDimensionPixelSize(R.dimen.album_dp_4);
        mRecyclerView.addItemDecoration(new Api21ItemDivider(Color.TRANSPARENT, dividerSize, dividerSize));
        mAdapter = new AlbumAdapter(getContext(), hasCamera, choiceMode, widget.getMediaItemCheckSelector());
        Point outSize = new Point();
        mActivity.getWindowManager().getDefaultDisplay().getRealSize(outSize);
        //当前设备屏宽
        int screen_width = outSize.x;
        if(column>1){
            mAdapter.setImageViewInfo((screen_width-(column-1)*dividerSize)/column,(screen_width-(column-1)*dividerSize)/column);
        }
        mAdapter.setAddClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                getPresenter().clickCamera(view);
            }
        });
        mAdapter.setCheckedClickListener(new OnCheckedClickListener() {
            @Override
            public void onCheckedClick(CompoundButton button, int position) {
                getPresenter().tryCheckItem(button, position);
            }
        });
        mAdapter.setItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                getPresenter().tryPreviewItem(position);
            }
        });
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public void setLoadingDisplay(boolean display) {
        mLayoutLoading.setVisibility(display ? View.VISIBLE : View.GONE);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        int position = mLayoutManager.findFirstVisibleItemPosition();
        mLayoutManager.setOrientation(getOrientation(newConfig));
        mRecyclerView.setAdapter(mAdapter);
        mLayoutManager.scrollToPosition(position);
    }

    @RecyclerView.Orientation
    private int getOrientation(Configuration config) {
        switch (config.orientation) {
            case Configuration.ORIENTATION_PORTRAIT: {
                return LinearLayoutManager.VERTICAL;
            }
            case Configuration.ORIENTATION_LANDSCAPE: {
                return LinearLayoutManager.HORIZONTAL;
            }
            default: {
                throw new AssertionError("This should not be the case.");
            }
        }
    }

    @Override
    public void setCompleteDisplay(boolean display) {
//        mCompleteMenu.setVisible(display);
//        if(display){
//            tv_finish.setVisibility(View.INVISIBLE);
//        }else{
//            tv_finish.setVisibility(View.VISIBLE);
//        }
    }

    @Override
    public void bindAlbumFolder(AlbumFolder albumFolder) {
        mAdapter.setAlbumFiles(albumFolder.getAlbumFiles());
        mAdapter.notifyDataSetChanged();
        mRecyclerView.scrollToPosition(0);
//        new Thread(){
//            public void run(){
//                try {
//                    Thread.sleep(200);
//                    mActivity.runOnUiThread(new Runnable() {
//                        @Override
//                        public void run() {
//                            mAdapter.notifyDataSetChanged();
//                        }
//                    });
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
//            }
//        }.start();
    }

    @Override
    public void notifyInsertItem(int position) {
        mAdapter.notifyItemInserted(position);
    }

    @Override
    public void notifyItem(int position) {
        mAdapter.notifyItemChanged(position);
    }

    @Override
    public void setCheckedCount(int count) {
    }

    @Override
    public void onClick(View v) {
        if (v == app_bar_layout) {
            mRecyclerView.smoothScrollToPosition(0);
        }else if (v == linear_path) {
            getPresenter().clickFolderSwitch();
        } else if(v==v_goback){
            mActivity.finish();
        }else if(v==ll_capture){
            ((AlbumPhoneActivity)mActivity).changToCapture();
        }
    }
}
