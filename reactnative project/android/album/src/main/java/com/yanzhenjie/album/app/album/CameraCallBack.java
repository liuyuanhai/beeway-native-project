package com.yanzhenjie.album.app.album;

public class CameraCallBack {
    private String mCameraFilePath;
    private String mUriPath;

    public String getmCameraFilePath() {
        return mCameraFilePath;
    }

    public void setmCameraFilePath(String mCameraFilePath) {
        this.mCameraFilePath = mCameraFilePath;
    }

    public String getmUriPath() {
        return mUriPath;
    }

    public void setmUriPath(String mUriPath) {
        this.mUriPath = mUriPath;
    }
}
