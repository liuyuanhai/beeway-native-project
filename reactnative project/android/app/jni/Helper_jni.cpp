#include <Helper_jni.h>
#include <opencv2/core.hpp>
#include <opencv2/objdetect.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <string>
#include <vector>
#include <Math.h>
#include <iostream>
#include <android/log.h>
#include <opencv2/features2d.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/core/types_c.h>
#include <opencv2/imgproc/types_c.h>
#include <opencv2/imgproc/imgproc_c.h>

#define LOG_TAG "FaceDetection/DetectionBasedTracker"
#define LOGD(...) ((void)__android_log_print(ANDROID_LOG_DEBUG, LOG_TAG, __VA_ARGS__))

using namespace std;
using namespace cv;
#include "ColorSpace.hpp"
#include "HSL.hpp"
//#define CLIP_RANGE(value, min, max)  ( (value) > (max) ? (max) : (((value) < (min)) ? (min) : (value)) )
//#define COLOR_RANGE(value)  CLIP_RANGE(value, 0, 255)inline void vector_Rect_to_Mat(vector<Rect>& v_rect, Mat& mat)

class CascadeDetectorAdapter: public DetectionBasedTracker::IDetector
{
public:
    CascadeDetectorAdapter(cv::Ptr<cv::CascadeClassifier> detector):
            IDetector(),
            Detector(detector)
    {
        LOGD("CascadeDetectorAdapter::Detect::Detect");
        CV_Assert(detector);
    }

    void detect(const cv::Mat &Image, std::vector<cv::Rect> &objects)
    {
        LOGD("CascadeDetectorAdapter::Detect: begin");
        LOGD("CascadeDetectorAdapter::Detect: scaleFactor=%.2f, minNeighbours=%d, minObjSize=(%dx%d), maxObjSize=(%dx%d)", scaleFactor, minNeighbours, minObjSize.width, minObjSize.height, maxObjSize.width, maxObjSize.height);
        Detector->detectMultiScale(Image, objects, scaleFactor, minNeighbours, 0, minObjSize, maxObjSize);
        LOGD("CascadeDetectorAdapter::Detect: end");
    }

    virtual ~CascadeDetectorAdapter()
    {
        LOGD("CascadeDetectorAdapter::Detect::~Detect");
    }

private:
    CascadeDetectorAdapter();
    cv::Ptr<cv::CascadeClassifier> Detector;
};

struct DetectorAgregator
{
    cv::Ptr<CascadeDetectorAdapter> mainDetector;
    cv::Ptr<CascadeDetectorAdapter> trackingDetector;

    cv::Ptr<DetectionBasedTracker> tracker;
    DetectorAgregator(cv::Ptr<CascadeDetectorAdapter>& _mainDetector, cv::Ptr<CascadeDetectorAdapter>& _trackingDetector):
            mainDetector(_mainDetector),
            trackingDetector(_trackingDetector)
    {
        CV_Assert(_mainDetector);
        CV_Assert(_trackingDetector);

        DetectionBasedTracker::Parameters DetectorParams;
        tracker = makePtr<DetectionBasedTracker>(mainDetector, trackingDetector, DetectorParams);
    }
};

JNIEXPORT jlong JNICALL Java_org_opencv_samples_facedetect_DetectionBasedTracker_nativeCreateObject
(JNIEnv * jenv, jclass, jstring jFileName, jint faceSize)
{
    LOGD("Java_org_opencv_samples_facedetect_DetectionBasedTracker_nativeCreateObject enter");
    const char* jnamestr = jenv->GetStringUTFChars(jFileName, NULL);
    string stdFileName(jnamestr);
    jlong result = 0;

    LOGD("Java_org_opencv_samples_facedetect_DetectionBasedTracker_nativeCreateObject");

    try
    {
        cv::Ptr<CascadeDetectorAdapter> mainDetector = makePtr<CascadeDetectorAdapter>(
            makePtr<CascadeClassifier>(stdFileName));
        cv::Ptr<CascadeDetectorAdapter> trackingDetector = makePtr<CascadeDetectorAdapter>(
            makePtr<CascadeClassifier>(stdFileName));
        result = (jlong)new DetectorAgregator(mainDetector, trackingDetector);
        if (faceSize > 0)
        {
            mainDetector->setMinObjectSize(Size(faceSize, faceSize));
            //trackingDetector->setMinObjectSize(Size(faceSize, faceSize));
        }
    }
    catch(const cv::Exception& e)
    {
        LOGD("nativeCreateObject caught cv::Exception: %s", e.what());
        jclass je = jenv->FindClass("org/opencv/core/CvException");
        if(!je)
            je = jenv->FindClass("java/lang/Exception");
        jenv->ThrowNew(je, e.what());
    }
    catch (...)
    {
        LOGD("nativeCreateObject caught unknown exception");
        jclass je = jenv->FindClass("java/lang/Exception");
        jenv->ThrowNew(je, "Unknown exception in JNI code of DetectionBasedTracker.nativeCreateObject()");
        return 0;
    }

    LOGD("Java_org_opencv_samples_facedetect_DetectionBasedTracker_nativeCreateObject exit");
    return result;
}

JNIEXPORT void JNICALL Java_org_opencv_samples_facedetect_DetectionBasedTracker_nativeDestroyObject
(JNIEnv * jenv, jclass, jlong thiz)
{
    LOGD("Java_org_opencv_samples_facedetect_DetectionBasedTracker_nativeDestroyObject");

    try
    {
        if(thiz != 0)
        {
            ((DetectorAgregator*)thiz)->tracker->stop();
            delete (DetectorAgregator*)thiz;
        }
    }
    catch(const cv::Exception& e)
    {
        LOGD("nativeestroyObject caught cv::Exception: %s", e.what());
        jclass je = jenv->FindClass("org/opencv/core/CvException");
        if(!je)
            je = jenv->FindClass("java/lang/Exception");
        jenv->ThrowNew(je, e.what());
    }
    catch (...)
    {
        LOGD("nativeDestroyObject caught unknown exception");
        jclass je = jenv->FindClass("java/lang/Exception");
        jenv->ThrowNew(je, "Unknown exception in JNI code of DetectionBasedTracker.nativeDestroyObject()");
    }
    LOGD("Java_org_opencv_samples_facedetect_DetectionBasedTracker_nativeDestroyObject exit");
}

JNIEXPORT void JNICALL Java_org_opencv_samples_facedetect_DetectionBasedTracker_nativeStart
(JNIEnv * jenv, jclass, jlong thiz)
{
    LOGD("Java_org_opencv_samples_facedetect_DetectionBasedTracker_nativeStart");

    try
    {
        ((DetectorAgregator*)thiz)->tracker->run();
    }
    catch(const cv::Exception& e)
    {
        LOGD("nativeStart caught cv::Exception: %s", e.what());
        jclass je = jenv->FindClass("org/opencv/core/CvException");
        if(!je)
            je = jenv->FindClass("java/lang/Exception");
        jenv->ThrowNew(je, e.what());
    }
    catch (...)
    {
        LOGD("nativeStart caught unknown exception");
        jclass je = jenv->FindClass("java/lang/Exception");
        jenv->ThrowNew(je, "Unknown exception in JNI code of DetectionBasedTracker.nativeStart()");
    }
    LOGD("Java_org_opencv_samples_facedetect_DetectionBasedTracker_nativeStart exit");
}

JNIEXPORT void JNICALL Java_org_opencv_samples_facedetect_DetectionBasedTracker_nativeStop
(JNIEnv * jenv, jclass, jlong thiz)
{
    LOGD("Java_org_opencv_samples_facedetect_DetectionBasedTracker_nativeStop");

    try
    {
        ((DetectorAgregator*)thiz)->tracker->stop();
    }
    catch(const cv::Exception& e)
    {
        LOGD("nativeStop caught cv::Exception: %s", e.what());
        jclass je = jenv->FindClass("org/opencv/core/CvException");
        if(!je)
            je = jenv->FindClass("java/lang/Exception");
        jenv->ThrowNew(je, e.what());
    }
    catch (...)
    {
        LOGD("nativeStop caught unknown exception");
        jclass je = jenv->FindClass("java/lang/Exception");
        jenv->ThrowNew(je, "Unknown exception in JNI code of DetectionBasedTracker.nativeStop()");
    }
    LOGD("Java_org_opencv_samples_facedetect_DetectionBasedTracker_nativeStop exit");
}

JNIEXPORT void JNICALL Java_org_opencv_samples_facedetect_DetectionBasedTracker_nativeSetFaceSize
(JNIEnv * jenv, jclass, jlong thiz, jint faceSize)
{
    LOGD("Java_org_opencv_samples_facedetect_DetectionBasedTracker_nativeSetFaceSize -- BEGIN");

    try
    {
        if (faceSize > 0)
        {
            ((DetectorAgregator*)thiz)->mainDetector->setMinObjectSize(Size(faceSize, faceSize));
            //((DetectorAgregator*)thiz)->trackingDetector->setMinObjectSize(Size(faceSize, faceSize));
        }
    }
    catch(const cv::Exception& e)
    {
        LOGD("nativeStop caught cv::Exception: %s", e.what());
        jclass je = jenv->FindClass("org/opencv/core/CvException");
        if(!je)
            je = jenv->FindClass("java/lang/Exception");
        jenv->ThrowNew(je, e.what());
    }
    catch (...)
    {
        LOGD("nativeSetFaceSize caught unknown exception");
        jclass je = jenv->FindClass("java/lang/Exception");
        jenv->ThrowNew(je, "Unknown exception in JNI code of DetectionBasedTracker.nativeSetFaceSize()");
    }
    LOGD("Java_org_opencv_samples_facedetect_DetectionBasedTracker_nativeSetFaceSize -- END");
}


JNIEXPORT void JNICALL Java_org_opencv_samples_facedetect_DetectionBasedTracker_nativeDetect
(JNIEnv * jenv, jclass, jlong thiz, jlong imageGray, jlong faces)
{
    LOGD("Java_org_opencv_samples_facedetect_DetectionBasedTracker_nativeDetect");

    try
    {
        vector<Rect> RectFaces;
        ((DetectorAgregator*)thiz)->tracker->process(*((Mat*)imageGray));
        ((DetectorAgregator*)thiz)->tracker->getObjects(RectFaces);
        *((Mat*)faces) = Mat(RectFaces, true);
    }
    catch(const cv::Exception& e)
    {
        LOGD("nativeCreateObject caught cv::Exception: %s", e.what());
        jclass je = jenv->FindClass("org/opencv/core/CvException");
        if(!je)
            je = jenv->FindClass("java/lang/Exception");
        jenv->ThrowNew(je, e.what());
    }
    catch (...)
    {
        LOGD("nativeDetect caught unknown exception");
        jclass je = jenv->FindClass("java/lang/Exception");
        jenv->ThrowNew(je, "Unknown exception in JNI code DetectionBasedTracker.nativeDetect()");
    }
    LOGD("Java_org_opencv_samples_facedetect_DetectionBasedTracker_nativeDetect END");
}

JNIEXPORT void JNICALL Java_com_test_RemapHelper_nativeRemap(JNIEnv*, jclass,jlong srcAddress,jlong map_x_address,jlong map_y_address,jint type)
{
    Mat& src  = *(Mat*)srcAddress;
    Mat& map_x  = *(Mat*)map_x_address;
    Mat& map_y  = *(Mat*)map_y_address;
    switch(type){
        case 11:
            for( int j = 0; j < src.rows; j++ ) {
                for (int i = 0; i < src.cols; i++) {
                    if(j>src.rows/2){
                        map_x.at<float>(j,i) = i ;
                        map_y.at<float>(j,i) = src.rows - j ;
                    }else{
                        map_x.at<float>(j,i) = i ;
                        map_y.at<float>(j,i) = j ;
                    }
                }
            }
            break;
        case 12:
            for( int j = 0; j < src.rows; j++ ) {
                for (int i = 0; i < src.cols; i++) {
                    if(j>src.rows/2){
                        map_x.at<float>(j,i) = i ;
                        map_y.at<float>(j,i) = j -src.rows/2;
                    }else{
                        map_x.at<float>(j,i) = i ;
                        map_y.at<float>(j,i) = src.rows/2-j ;
                    }
                }
            }
            break;
        case 21:
            for( int j = 0; j < src.rows; j++ ) {
                for (int i = 0; i < src.cols; i++) {
                    if(i>src.cols/2){
                        map_x.at<float>(j,i) =src.cols-i ;
                        map_y.at<float>(j,i) = j ;
                    }else{
                        map_x.at<float>(j,i) = i ;
                        map_y.at<float>(j,i) = j ;
                    }
                }
            }
            break;
        case 22:
            for( int j = 0; j < src.rows; j++ ) {
                for (int i = 0; i < src.cols; i++) {
                    if(i>src.cols/2){
                        map_x.at<float>(j,i) = i-src.cols/2;
                        map_y.at<float>(j,i) = j ;
                    }else{
                        map_x.at<float>(j,i) = src.cols/2-i ;
                        map_y.at<float>(j,i) = j ;
                    }
                }
            }
            break;
        case 31:
            for( int j = 0; j < src.rows; j++ ) {
                for (int i = 0; i < src.cols; i++) {
                    if(i>src.cols/2){
                        map_x.at<float>(j,i) = src.cols-i ;
                    }else{
                        map_x.at<float>(j,i) =i ;
                    }
                    if(j>src.rows/2){
                        map_y.at<float>(j,i) = src.rows - j ;
                    }else{
                        map_y.at<float>(j,i) = j ;
                    }
                }
            }
            break;
        case 32:
            for( int j = 0; j < src.rows; j++ ) {
                for (int i = 0; i < src.cols; i++) {
                    if(i>src.cols/2){
                        map_x.at<float>(j,i) = i-src.cols/2 ;
                    }else{
                        map_x.at<float>(j,i) =src.cols/2-i ;
                    }
                    if(j>src.rows/2){
                        map_y.at<float>(j,i) = src.rows - j ;
                    }else{
                        map_y.at<float>(j,i) = j ;
                    }
                }
            }
            break;
        case 33:
            for( int j = 0; j < src.rows; j++ ) {
                for (int i = 0; i < src.cols; i++) {
                    if(i>src.cols/2){
                        map_x.at<float>(j,i) = src.cols-i ;
                    }else{
                        map_x.at<float>(j,i) =i ;
                    }
                    if(j>src.rows/2){
                        map_y.at<float>(j,i) = j-src.rows/2 ;
                    }else{
                        map_y.at<float>(j,i) = src.rows/2-j ;
                    }
                }
            }
            break;
        case 34:
            for( int j = 0; j < src.rows; j++ ) {
                for (int i = 0; i < src.cols; i++) {
                    if(i>src.cols/2){
                        map_x.at<float>(j,i) = i-src.cols/2 ;
                    }else{
                        map_x.at<float>(j,i) =src.cols/2-i ;
                    }
                    if(j>src.rows/2){
                        map_y.at<float>(j,i) = j-src.rows/2 ;
                    }else{
                        map_y.at<float>(j,i) = src.rows/2-j ;
                    }
                }
            }
            break;
    }
}

JNIEXPORT void JNICALL Java_com_test_RemapHelper_nativeTranlatePicture(JNIEnv*, jclass,jlong srcAddress,jlong map_x_address,jlong map_y_address,jint offsetX,jint offsetY)
{
    Mat& src  = *(Mat*)srcAddress;
    Mat& map_x  = *(Mat*)map_x_address;
    Mat& map_y  = *(Mat*)map_y_address;
    for( int j = 0; j < src.rows; j++ ) {
        for (int i = 0; i < src.cols; i++) {
            map_x.at<float>(j,i) = i+offsetX ;
            map_y.at<float>(j,i) = j +offsetY ;
        }
    }
}
JNIEXPORT void JNICALL Java_com_test_RemapHelper_nativeRotate(JNIEnv*, jclass,jlong srcAddress,jlong dstAddress,jint angle) {
    Mat& src  = *(Mat*)srcAddress;
    Mat& dst  = *(Mat*)dstAddress;
    float radian = (float) (angle /180.0 * CV_PI);

    //填充图像
    int maxBorder =(int) (max(src.cols, src.rows)* 1.414 ); //即为sqrt(2)*max
    int dx = (maxBorder - src.cols)/2;
    int dy = (maxBorder - src.rows)/2;
    copyMakeBorder(src, dst, dy, dy, dx, dx, BORDER_CONSTANT);

    //旋转
    Point2f center( (float)(dst.cols/2) , (float) (dst.rows/2));
    Mat affine_matrix = getRotationMatrix2D( center, angle, 1.0 );//求得旋转矩阵
    warpAffine(dst, dst, affine_matrix, dst.size());

    //计算图像旋转之后包含图像的最大的矩形
    float sinVal = abs(sin(radian));
    float cosVal = abs(cos(radian));
    Size targetSize( (int)(src.cols * cosVal +src.rows * sinVal),
                     (int)(src.cols * sinVal + src.rows * cosVal) );

    //剪掉多余边框
    int x = (dst.cols - targetSize.width) / 2;
    int y = (dst.rows - targetSize.height) / 2;
    Rect rect(x, y, targetSize.width, targetSize.height);
    dst = Mat(dst,rect);
}
JNIEXPORT void JNICALL Java_com_test_RemapHelper_overlayImage(JNIEnv*, jclass,jlong srcAddress,jlong dstAddress, jint locaton_x,jint location_y)
{
    Mat& first  = *(Mat*)srcAddress;
    Mat& second  = *(Mat*)dstAddress;

    Mat* src  = &first;
    Mat* overlay = &second;

    for (int y = max(location_y, 0); y < src->rows; ++y)
    {
        int fY = y - location_y;

        if (fY >= overlay->rows)
            break;

        for (int x = max(locaton_x, 0); x < src->cols; ++x)
        {
            int fX = x - locaton_x;

            if (fX >= overlay->cols)
                break;

            double opacity = ((double)overlay->data[fY * overlay->step + fX * overlay->channels() + 3]) / 255;

            for (int c = 0; opacity > 0 && c < src->channels(); ++c)
            {
                unsigned char overlayPx = overlay->data[fY * overlay->step + fX * overlay->channels() + c];
                unsigned char srcPx = src->data[y * src->step + x * src->channels() + c];
                src->data[y * src->step + src->channels() * x + c] = srcPx * (1. - opacity) + overlayPx * opacity;
            }
        }
    }

}
JNIEXPORT void JNICALL Java_com_test_RemapHelper_changeHue(JNIEnv*, jclass,jlong srcAddress,jlong dstAddress,jint hue,jint saturation,jint brightness)
{
    Mat &src = *(Mat *) srcAddress;
    Mat &dst = *(Mat *) dstAddress;
    HSL hsl;
    int color = 0;
    hsl.channels[color].
    hue = hue;
    hsl.channels[color].
    saturation = saturation;
    hsl.channels[color].
    brightness = brightness;
    hsl.adjust(src, dst);
}
//strength 光照强度
JNIEXPORT void JNICALL Java_com_test_RemapHelper_yinyangse(JNIEnv*, jclass,jlong srcAddress,jlong dstAddress,jint centerX,jint centerY,jint radius ,jint strength)
{
//    Mat& src  = *(Mat*)srcAddress;
//    Mat& dst  = *(Mat*)dstAddress;
//    int rows = src.rows;
//    int cols = src.cols;
//    dst.create(src.size(), src.type());
////图像光照特效
//    for(int i=0;i<rows;i++){
//        for(int j=0;j<cols;j++){
//            //计算当前点到光照中心距离(平面坐标系中两点之间的距离)
//            int distance = pow((centerY-j), 2) + pow((centerX-i), 2);
//            if (distance < radius * radius){
//                //获取原始图像像素色值
//                float blue = src.at<Vec3b>(i, j)[0];
//                float green = src.at<Vec3b>(i, j)[1];
//                float red = src.at<Vec3b>(i, j)[2];
//                //按照距离大小计算增强的光照值
//                int result = (int) (strength * (1.0 - sqrt(distance) / radius));
//                blue = blue + result;
//                green = green + result;
//                red = red + result;
//                //判断边界 防止越界
//                if(blue<0){
//                    blue = 0;
//                }else if(blue>255){
//                    blue = 255;
//                }
//                if(green<0){
//                    green = 0;
//                }else if(green>255){
//                    green = 255;
//                }
//                if(red<0){
//                    red = 0;
//                }else if(red>255){
//                    red = 255;
//                }
//                dst.at<Vec3b>(i, j)[0]=blue;
//                dst.at<Vec3b>(i, j)[1]=green;
//                dst.at<Vec3b>(i, j)[2]=red;
//
//                if(strength<=0){//如果调暗
//
//                }else{//如果调亮
//
//                }
//                img_adjust[:, :, 2] *= (1/(lum**0.5))**0.7
//                img_adjust[:, :, 0] *= 1.2
//                elif lum < 1:
//                img_adjust[:, :, 2] *= (1/(lum**0.5))**0.7
//                img_adjust[:, :, 2][img_adjust[:, :, 2] > 1] = 1
//                img_adjust[:, :, 0] *= 0.5
//            }else{
//                dst.at<Vec3b>(i, j)[0]=src.at<Vec3b>(i, j)[0];
//                dst.at<Vec3b>(i, j)[1]=src.at<Vec3b>(i, j)[1];
//                dst.at<Vec3b>(i, j)[2]=src.at<Vec3b>(i, j)[2];
//            }
//        }
//    }
}
//白平衡 值在0-1之间
JNIEXPORT void JNICALL Java_com_test_RemapHelper_baipingheng(JNIEnv*, jclass,jlong srcAddress,jfloat weight)
{
    Mat &src = *(Mat *) srcAddress;
//    Mat &dst = *(Mat *) dstAddress;
    std::vector<cv::Mat> imageRGB;
    // R G B三通道分离
    split(src, imageRGB);
    //原图RGB分量的平均值
    double aR,aG,aB;
    aB=mean(imageRGB[0])[0];
    aG=mean(imageRGB[1])[0];
    aR=mean(imageRGB[2])[0];
    //需要调整的RGB分量的增益
    double kR,kG,kB;
    kB=(aR+aB+aG)/(3*aB);
    kG=(aR+aB+aG)/(3*aG);
    kR=(aR+aB+aG)/(3*aR);
    //白平衡前后图加权平均，并且输出不超过255
    addWeighted(imageRGB[0]*kB, weight, imageRGB[0], 1-weight, 0, imageRGB[0]);
    addWeighted(imageRGB[1]*kG, weight, imageRGB[1], 1-weight, 0, imageRGB[1]);
    addWeighted(imageRGB[2]*kR, weight, imageRGB[2], 1-weight, 0, imageRGB[2]);

    //RGB三通道合并
    merge(imageRGB, src);
}
JNIEXPORT void JNICALL Java_com_test_RemapHelper_MyYinYangSe(JNIEnv*, jclass,jlong srcAddress,jlong maskAddress,jlong imgShowAddress,jfloat lum,jint explor){
//    //构造大小和原图一样的空图用于之后制作合成图
//    cv::Mat imgShow=cv::Mat(src.size(),src.type(),Scalar::all(0));

    Mat &src = *(Mat *) srcAddress;
    Mat &mask = *(Mat *) maskAddress;
    Mat &imgShow = *(Mat *) imgShowAddress;

    //变为渐变掩码，值代表透明度
    cv::Mat maskAdjust = maskWithGradient(mask,explor);
    //原图透明度
    cv::Mat maskOri=1-maskAdjust;
    //调节选择区域的亮度
    cv::Mat imgAdjust=MyGammaCorrection(src, lum);
    imgAdjust.convertTo(imgAdjust, CV_32FC3,1.0/255);
    cvtColor(imgAdjust, imgAdjust, COLOR_RGB2HLS);
    cv::MatIterator_<cv::Vec3f> it, end,outBegin;
    for (int row = 0;  row< imgAdjust.rows; row++)
    {
        Vec3f* ptr = imgAdjust.ptr<Vec3f>(row);
        for (int col = 0; col < imgAdjust.cols; col++)
        {
            float s=ptr[col][2]*(pow(1.0/pow(lum, 0.5), 0.7));
            ptr[col][2]= s>1?1:s;
            if (lum>1) {
                float h=ptr[col][0]*1.2;
                ptr[col][0]=h>360?360:h;
            }else{
                ptr[col][0]*=0.5;
            }
        }
    }
    cvtColor(imgAdjust, imgAdjust, COLOR_HLS2RGB);
    imgAdjust.convertTo(imgAdjust, CV_32FC3,255);

    std::vector<cv::Mat> srcChannal;
    std::vector<cv::Mat> imgAdjustChannal;
    src.convertTo(src, CV_32FC3);
    cv::split(src, srcChannal);
    cv::Mat srcB = srcChannal[0];
    cv::Mat srcG = srcChannal[1];
    cv::Mat srcR = srcChannal[2];

    cv::split(imgAdjust, imgAdjustChannal);
    cv::Mat imgAdjustB = imgAdjustChannal[0];
    cv::Mat imgAdjustG = imgAdjustChannal[1];
    cv::Mat imgAdjustR = imgAdjustChannal[2];

    srcB=maskOri.mul(srcB)+maskAdjust.mul(imgAdjustB);
    srcG=maskOri.mul(srcG)+maskAdjust.mul(imgAdjustG);
    srcR=maskOri.mul(srcR)+maskAdjust.mul(imgAdjustR);
    merge(srcChannal, imgShow);
    imgShow.convertTo(imgShow, CV_8UC3);
}

cv::Mat maskWithGradient(cv::Mat mask, int explor){
    int bias = 5;
    mask = kFourChannelsBecomeThree(mask);
    // cv::Mat gray=cv::Mat(mask.size(),CV_8UC1,Scalar::all(0));
    //cv::cvtColor(mask, gray, COLOR_RGB2GRAY);
    // cv::threshold(gray, gray, 240, 255, THRESH_BINARY);
    cv::Mat kernel = getStructuringElement(cv::MORPH_RECT, cv::Size(3, 3));
    //对Mask膨胀处理，增加Mask面积
    cv::dilate(mask, mask, kernel,cv::Point(-1,-1),explor);
    cv::blur(mask, mask, cv::Size(bias+int(3*explor),bias+int(3*explor)));
    mask.convertTo(mask, CV_32FC3);
    mask=(mask/255)/2;
    return mask;
}

cv::Mat MyGammaCorrection(cv::Mat& src, float fGamma)
{
    src=kFourChannelsBecomeThree(src);
    CV_Assert(src.data);
    // accept only char type matrices
    CV_Assert(src.depth() != sizeof(uchar));
    // build look up table
    unsigned char lut[256];
    for( int i = 0; i < 256; i++ )
    {
        lut[i] = saturate_cast<uchar>(pow((float)(i/255.0), fGamma) * 255.0f);
    }

    cv::Mat  dst = src.clone();
    const int channels = dst.channels();
    switch(channels)
    {
        case 1:
        {
        MatIterator_<uchar> it, end;
        for( it = dst.begin<uchar>(), end = dst.end<uchar>(); it != end; it++ )
            *it = lut[(*it)];

            break;
        }
        case 3:
        {
        for (int row = 0; row < dst.rows; row++)
        {
            Vec3b* ptr = dst.ptr<Vec3b>(row);
            for (int col = 0; col < dst.cols; col++)
            {
                ptr[col][0] = lut[ptr[col][0]];
                ptr[col][1] = lut[ptr[col][1]];
                ptr[col][2] = lut[ptr[col][2]];
            }
        }
        break;
        }
    }
    return dst;
}
/* 4通道转成3通道 */
cv::Mat kFourChannelsBecomeThree(cv::Mat src){
    cv::Mat dst=cv::Mat::zeros(src.size(), src.type());
    if (src.channels() == 4) {
        cv::cvtColor(src, dst, cv::COLOR_RGBA2RGB);
        return dst;
    }
    return src;
}




