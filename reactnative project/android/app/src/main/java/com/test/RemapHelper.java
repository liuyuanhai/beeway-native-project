package com.test;

public class RemapHelper {

    //1.垂直双拼；2.水平双拼；3.四pin
    public static void setRemap(long srcAddress,long mapXAddress,long mapyAddress,int type){
        nativeRemap(srcAddress,mapXAddress,mapyAddress,type);
    }
    public static void tranlatePicture(long srcAddress,long mapXAddress,long mapYAddress,int offsetX,int offsetY){
        nativeTranlatePicture(srcAddress,mapXAddress,mapYAddress,offsetX,offsetY);
    }
    //
    public static void setRotate(long srcAddress,long dstAddress,int angle){
        nativeRotate(srcAddress,dstAddress,angle);
    }
    public static void setOverlayImage(long srcAddress,long overlyAddress,int x,int y){
        overlayImage(srcAddress,overlyAddress,x,y);
    }
//    int hue;          //色度调整值，     数据范围:  [-180, 180]
//    int saturation;   //饱和度调整值，数据范围:  [-100, 100]
//    int brightness;   //明度调整值，    数据范围:  [-100, 100]
    public static  void setChangeHue(long srcAddress,long dstAddress,int hue,int saturation,int brightness){
        changeHue(srcAddress,dstAddress,hue,saturation,brightness);
    }
    public static  void setYinYangSe(long srcAddress,long dstAddress,int centerX,int centerY,int radius ,int strength){
        yinyangse(srcAddress,dstAddress,centerX,centerY,radius,strength);
    }
    public static  void setBaipingheng(long srcAddress,float weight){
        baipingheng(srcAddress,weight);
    }
    public static void setMyYingYangSe(long srcAddress,long maskAddress,long imgShowAddress,float lum,int explor){
        MyYinYangSe(srcAddress,maskAddress,imgShowAddress,lum,explor);
    }
    private static native void nativeRemap(long srcAddress,long map_x,long map_y,int type);
    private static native void nativeTranlatePicture(long srcAddress,long map_x,long map_y,int offsetX,int offsetY);

    private static native void nativeRotate(long srcAddress,long dstAddress,int angle);
    private static native void overlayImage(long srcAddress,long dstAddress,int x,int y);

    private static native void changeHue(long srcAddress,long dstAddress,int hue,int saturation,int brightness);
    private static native void yinyangse(long srcAddress,long dstAddress,int centerX,int centerY,int radius ,int strength);
    private static native void baipingheng(long srcAddress,float weight);
    private static native void MyYinYangSe(long srcAddress,long maskAddress,long imgShowAddress,float lum,int explor);
    static {
        System.loadLibrary("detection_based_tracker");
    }
}
