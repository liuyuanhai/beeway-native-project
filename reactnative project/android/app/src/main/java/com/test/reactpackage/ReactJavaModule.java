package com.test.reactpackage;

import android.app.Activity;
import android.content.Intent;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;

public class ReactJavaModule extends ReactContextBaseJavaModule {
    public ReactJavaModule(ReactApplicationContext reactContext) {
        super(reactContext);
    }
    @NonNull
    @Override
    public String getName() {
        return "ReactJavaModule";
    }
    @ReactMethod
    public void startActivityFromJs(String name,String params){
        Activity currentActivity = getCurrentActivity();
        if(currentActivity!=null){
            try {
                Class toClass = Class.forName(name);
                Intent in = new Intent();
                in.putExtra("params",params);
                in.setClass(currentActivity,toClass);
                currentActivity.startActivity(in);
            } catch (ClassNotFoundException e) {
                Toast.makeText(currentActivity,"不能打开指定的页面",Toast.LENGTH_SHORT).show();
            }
        }
    }
}
