package com.lyh.mvp.yingyangse;

import static org.opencv.core.CvType.CV_8UC3;

import android.graphics.Bitmap;

import com.test.RemapHelper;

import org.opencv.android.Utils;
import org.opencv.core.Mat;

public class YingYangSeModule {
    private ModuleInterface moduleInterface;//回调
    public YingYangSeModule(ModuleInterface moduleInterface){
        this.moduleInterface = moduleInterface;
    }

    //设置阴阳色画笔粗细
    public Bitmap setPaintSize(Bitmap bitmap,Bitmap dst,int x,int y,int R,float lum,int explore ){

        Mat srcMat = new Mat();
        Utils.bitmapToMat(bitmap, srcMat);
        Mat imgShow = new Mat(srcMat.size(),srcMat.type());
        Mat mask=new Mat(srcMat.size(),CV_8UC3);
//        RemapHelper.setMyYingYangSe();
//        Bitmap bitmapDst = new Bitmap();
        return bitmap;
    }
    //


    public void getTestData(){
        //1.获取数据逻辑。
        //.......
        //2.将数据回调给parsener
        moduleInterface.getData("test");
    }
    public interface ModuleInterface{
        public void getData(String s );
    }
}
