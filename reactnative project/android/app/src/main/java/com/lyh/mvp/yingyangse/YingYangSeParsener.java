package com.lyh.mvp.yingyangse;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.util.Log;
import android.view.MotionEvent;
import android.widget.Toast;

import com.lyh.brean.MySelectPath;
import com.lyh.brean.MyPoint;
import com.lyh.util.common;
import com.test.R;

import org.opencv.core.Point;

import java.util.ArrayList;

public class YingYangSeParsener implements YingYangSeModule.ModuleInterface{

    private Context context;

    private YingYangSeView mvpView;
    private YingYangSeModule yingYangSeModule = new YingYangSeModule(this);

    private Paint paintForSize;//调节画笔粗细时，绘制圆形的画笔
    private Paint paintForSelectArea;//绘制涂抹区域的画笔

    private int paintMinimumSize;//画笔最小宽度
    private int paintMaximumSize;//画笔最大宽度
    private int paintSizeNow;//画笔当前大小//画笔宽度默认24dp 在24dp到44dp之间

    private int gradualMinimumSize;//边缘渐变最小宽度
    private int gradualMaximumSize;//边缘渐变最大宽度
    private int gradualWidthNow;//边缘渐变范围 边缘模糊范围 0到20dp

    private Bitmap bitmapYingYangSe;//阴阳色调整后的图片

    private int step=1;// 1选择区域 ，2调节亮度和范渐变范围
    private ArrayList<MySelectPath> paths=new ArrayList<>();//消除区域路径合集

    private int pathEndPosition=-1;//路径结束位置
    private MySelectPath currentPath;

    private float lastX;//手指按下时的坐标 选择替换区域
    private float lastY;//手指按下时的坐标 选择替换区域

    private boolean isShowXiaoChuBiWidthCircle;//

    public YingYangSeParsener(YingYangSeView xiaoChuBiView){
        this.mvpView = xiaoChuBiView;
    }
    //初始化消除笔
    public void init(Context context){
        this.context =context;
        //初始画画笔 1.调节画笔粗细时，绘制圆形的画笔
        paintForSize = new Paint();
        paintForSize.setAntiAlias(true);
        paintForSize.setStyle(Paint.Style.FILL);
        paintForSize.setColor(context.getResources().getColor(R.color.color30FF0000));
        //初始画画笔 2.绘制涂抹区域的画笔
        paintForSelectArea = new Paint();
        paintForSelectArea.setAntiAlias(true);
        paintForSelectArea.setStyle(Paint.Style.STROKE);
        paintForSelectArea.setColor(context.getResources().getColor(R.color.color30FF0000));
        paintForSelectArea.setStrokeCap(Paint.Cap.ROUND);
    }
    public void reset(){
        paintMinimumSize = common.Dp2Px(context,24);//画笔宽度默认24dp 在24dp到44dp之间
        paintMaximumSize = common.Dp2Px(context,44);//画笔宽度默认24dp 在24dp到44dp之间
        paintSizeNow = (paintMaximumSize+paintMinimumSize)/2;//画笔宽度默认24dp 在24dp到44dp之间
        gradualMinimumSize = 0;
        gradualMaximumSize = common.Dp2Px(context,20);
        gradualWidthNow = 0;

        paintForSize.setStrokeWidth(paintSizeNow);//默认画笔大小
        paintForSelectArea.setStrokeWidth(gradualWidthNow);//默认画笔大小
        step = 1;
        paths.clear();
        currentPath = null;
    }
    /**
     * 设置画笔粗细
     */
    public void setPaintSize(Bitmap bitmapInit,int paintSize){
        paintSizeNow=paintSize;
        paintForSize.setStrokeWidth(paintSize);//画笔大小
//        bitmapYingYangSe =yingYangSeModule.setPaintSize();
        mvpView.invalidateView();
    }
    /**
     * 设置画笔粗细结束
     */
    public void setPaintSizeEnd(){
        mvpView.paintSizeChangedEnd();
    }

    /**
     * 设置模糊渐变范围
     */
    public void setGradual(int width){
        gradualWidthNow = width;
        mvpView.setGradual(width);
//        setXiaochuBi();
    }
    /**
     * 设置模糊渐变范围结束
     */
    public void setGradualEnd(){
        mvpView.setGradualEnd();
    }
    /**
     * @param type 涂抹选区,1.上一步,2.下一步
     */
    public void xiaoChuBiSelectArea(int type,Bitmap bitmapInit){
        if(type==1){//上一步
            if(pathEndPosition==-1){
                return;
            }else{
                pathEndPosition = pathEndPosition-1;
                if(pathEndPosition==-1){
                    mvpView.xiaoChuBiSetSectlectAreaStep(false,true);
                }else{
                    mvpView.xiaoChuBiSetSectlectAreaStep(true,true);
                }
            }
        }else{//下一步
            if(pathEndPosition==paths.size()-1){
                return;
            }else{
                pathEndPosition = pathEndPosition+1;
                if(pathEndPosition==paths.size()-1){
                    mvpView.xiaoChuBiSetSectlectAreaStep(true,false);
                }else{
                    mvpView.xiaoChuBiSetSectlectAreaStep(true,true);
                }
            }
        }
    }

    //上一步
    public void setPre(){
        step =1;
        if(pathEndPosition==paths.size()-1) {
            if(bitmapYingYangSe!=null){
                bitmapYingYangSe.recycle();
                bitmapYingYangSe =null;
            }
            mvpView.xiaoChuBiSetSectlectAreaStep(true,false);
        }else {
            mvpView.xiaoChuBiSetSectlectAreaStep(true,true);
        }
    }
    //下一步
    public void setNext(){
        if(step==1){
            if(pathEndPosition==-1){
                mvpView.toastInfo("请先涂抹瑕疵区域");
            }else{
                mvpView.xiaoChuBiSetNext();
                step =2;
            }
        }else{
            mvpView.xiaoChuBiSetNext();
            reset();
        }
    }

    public void drawPaintSize(Canvas canvas, int width , int height){
        if(isShowXiaoChuBiWidthCircle){
            canvas.drawCircle(width/2,height/2,paintSizeNow/2,paintForSize);
        }
        if(step==1){
            drawXiaochuBiShadows(canvas);
        }
    }
    //绘制消除区域
    public void drawXiaochuBiShadows(Canvas canvas){
        if(paths.size()>0){
            Path path = new Path();
            for(int i=0;i<paths.size();i++){
                if(i>pathEndPosition){
                    break;
                }
                for(int j = 0;j<paths.get(i).getPathPoints().size();j++){
                    if(j==0) {
                        path.moveTo((float)paths.get(i).getPathPoints().get(j).getPointShow().x,(float)paths.get(i).getPathPoints().get(j).getPointShow().y);
                    }else {
                        path.lineTo((float)paths.get(i).getPathPoints().get(j).getPointShow().x,(float)paths.get(i).getPathPoints().get(j).getPointShow().y);
                    }
                }
            }
            canvas.drawPath(path,paintForSelectArea);
        }
    }
    //选择消除区域事件
    public boolean onXiaoChuBiSelectArea(MotionEvent event, int imageLeft, int imageTop){
        switch(event.getAction()){
            case MotionEvent.ACTION_DOWN:
//                currentPath = new MySelectPath(paintSizeNow,(int)(paintSizeNow/scale));
                currentPath = new MySelectPath(paintSizeNow,(int)(paintSizeNow/1));
                addXiaochuBiTouchPoints(event,currentPath,imageLeft,imageTop);
                break;
            case MotionEvent.ACTION_MOVE:
                addXiaochuBiTouchPoints(event,currentPath,imageLeft,imageTop);
                break;
            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_CANCEL:
                addXiaochuBiTouchPoints(event,currentPath,imageLeft,imageTop);
                if(pathEndPosition==-1){
                    pathEndPosition = 0;
                    paths.clear();
                    paths.add(currentPath);
                }else{
                    for(int i=paths.size()-1;i>pathEndPosition;i--){
                        paths.remove(i);
                    }
                    paths.add(currentPath);
                    pathEndPosition = pathEndPosition+1;
                }
                mvpView.xiaoChuBiSetSectlectAreaStep(true,false);
                break;
        }
        return true;
    }
    private void addXiaochuBiTouchPoints(MotionEvent event, MySelectPath path, int imageLeft, int imageTop){
        int x = (int)event.getX();
        int y = (int)event.getY();
        Point point = new Point();
        point.x = x;
        point.y = y;
        Point point2 = new Point();

//        point2.x = (x-imageLeft)/scale;
//        point2.y = (y-imageTop)/scale;
        point2.x = (x-imageLeft)/1;
        point2.y = (y-imageTop)/1;
        Log.e("------------------>","point2.x ="+point2.x);
        Log.e("------------------>","point2.y ="+point2.y);

        MyPoint xiaoChuBiPoint = new MyPoint();
        xiaoChuBiPoint.setPointShow(point);
        xiaoChuBiPoint.setPointXiaoChu(point2);
        path.getPathPoints().add(xiaoChuBiPoint);
    }
    public void setXiaochuBiSelectReplaceArea(Context context){
        if(step==1){
            if(pathEndPosition==-1){
                Toast.makeText(context,"请用手指涂抹选择区域",Toast.LENGTH_SHORT).show();
                return;
            }else{
                step = 2;
            }
        }
    }
    /**
     * 设置消除笔
     */
    public void setYingYangSe(){
        if(pathEndPosition!=-1){
//            if(bitmapXiaoChu!=null){
//                bitmapXiaoChu.recycle();
//                bitmapXiaoChu=null;
//            }
//            bitmapXiaoChu = Bitmap.createBitmap(bitmapInit.getWidth(),bitmapInit.getHeight(), Bitmap.Config.ARGB_8888);
//            OpenCVUtil.inpaintBlurAllPicture(bitmapInit,bitmapXiaoChu,paths,pathEndPosition,(int)(xiaochuX/scale),(int)(xiaochuY/scale),(int)(xiaoChuBiWidth/scale),(int)(xiaoChuBiBorderWidth/scale));
//            xiaoChuBiView.invalidateView();
        }
    }
    @Override
    public void getData(String s) {

    }
}
