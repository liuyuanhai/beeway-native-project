package com.lyh.util;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;

import androidx.core.content.FileProvider;

import java.io.File;

public class CameraPicturesUtil {
    public static  final int  CAMERA=1001;//相机
    public static  final int  CAMERACROP=1002;//相机裁剪
    public static  final int  ALBUM=1003;//相册
    public static  final int  ALBUMCROP=1004;//相册裁剪
    public static  final int  CROP =1005;//裁剪
    /**
     *  调用拍照
     *  @param  isCrop 是否裁剪
     */
    public static void captrure(Activity activity, boolean isCrop, String filePath){
        try {

            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            Uri imageUri=null;
            if(SystemUtil.getIs7up()){
                String provider = activity.getPackageName()+".provider";
                imageUri= FileProvider.getUriForFile(activity,provider,new File(filePath));
            }else{
                String filepath = "file://" + filePath;
                imageUri = Uri.parse(filepath);
            }
            intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
            if(!isCrop){
                activity.startActivityForResult(intent, CAMERA);
            }else{
                activity.startActivityForResult(intent, CAMERACROP);
            }
        } catch (ActivityNotFoundException e) {
            //没有拍照功能
        }
    }
    /**
     * 调用相册不裁剪
     * @param  isCrop 是否裁剪
     */

    public static void photos(Activity activity, boolean isCrop){
        Intent intent = new Intent();
        intent.setType("image/*");//可选择图片视频
        //修改为以下两句代码
        intent.setAction(Intent.ACTION_PICK);
        intent.setData(MediaStore.Images.Media.EXTERNAL_CONTENT_URI);//使用以上这种模式，并添加以上两句
        if(!isCrop){
            activity.startActivityForResult(intent,ALBUM);
        }else{
            activity.startActivityForResult(intent,ALBUMCROP);
        }
    }
    /**
     * 裁剪图片方法
     *
     * @param filePath
     * @param outputX
     * @param outputY
     * example
     * cropImageUri(filpath, 300, 300)
     */
    public static void cropImage(String filePath, String fileOutPutPath, int outputX, int outputY, Activity activity) {
        Uri uri;
        Uri uriOutPut;
        if(SystemUtil.getIs7up()){
            String provider = activity.getPackageName()+".provider";
            uri= FileProvider.getUriForFile(activity,provider,new File(filePath));
            uriOutPut = Uri.parse("file://"+fileOutPutPath);
        }else{
            uri = Uri.parse("file://"+filePath);
            uriOutPut = Uri.parse("file://"+fileOutPutPath);
        }
        Intent intent = new Intent("com.android.camera.action.CROP");
        if (SystemUtil.getIs7up()) {
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        }
        intent.setDataAndType(uri, "image/*");
        intent.putExtra("crop", "true");
        intent.putExtra("aspectX", 1);
        intent.putExtra("aspectY", 1);
        intent.putExtra("outputX", outputX);
        intent.putExtra("outputY", outputY);
        intent.putExtra("scale", true);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, uriOutPut);
        intent.putExtra("return-data", false);
        intent.putExtra("outputFormat", Bitmap.CompressFormat.JPEG.toString());
        intent.putExtra("noFaceDetection", true); // no face detection
        activity.startActivityForResult(intent, CROP);
    }
    public static void onActivityResult(Activity activity, OnCameraPictureSelectListener listener, int requestCode, int resultCode, Intent data) {
        if (resultCode == activity.RESULT_OK){
            switch (requestCode){
                case CAMERA:
                    //根据拍照路径可以直接使用图片
                    listener.carmeraUnCrop(data);
                    break;
                case CAMERACROP:
                    //根据拍照路径对图片进行裁剪
                    listener.carmeraCrop(data);
                    break;
                case ALBUM:
                    //获取相册所在文件真实路径 String path =   getPhotosPath.getPath(this, originalUri)
                    listener.photosUnCrop(data);
                    break;
                case ALBUMCROP:
                    //获取相册所在文件真实路径 String path =   getPhotosPath.getPath(this, originalUri) 对图片进行裁剪
                    listener.photosCrop(data);
                    break;
                case CROP:
                    //根据图片裁剪的路径 可以直接使用图片
                    listener.crop(data);
                    break;
            }
        }
    }
    public interface OnCameraPictureSelectListener{
        public void carmeraUnCrop(Intent data);
        public void carmeraCrop(Intent data);
        public void photosUnCrop(Intent data);
        public void photosCrop(Intent data);
        public void crop(Intent data);
    }
}
