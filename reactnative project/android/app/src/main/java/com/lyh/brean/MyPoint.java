package com.lyh.brean;

import org.opencv.core.Point;

public class MyPoint {
    private Point pointShow;//界面显示用坐标
    private Point pointXiaoChu;//消除操作使用坐标

    public Point getPointShow() {
        return pointShow;
    }

    public void setPointShow(Point pointShow) {
        this.pointShow = pointShow;
    }

    public Point getPointXiaoChu() {
        return pointXiaoChu;
    }

    public void setPointXiaoChu(Point pointXiaoChu) {
        this.pointXiaoChu = pointXiaoChu;
    }
}
