package com.lyh.activity;

import static org.opencv.core.CvType.CV_32FC1;
import static org.opencv.imgproc.Imgproc.COLOR_RGBA2BGRA;

import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.lyh.scale.ScreenUtil;
import com.lyh.util.FileUtil;
import com.lyh.util.ImageDealUtil;
import com.lyh.util.PermissionUtil;
import com.lyh.util.common;
import com.lyh.view.Tutorial3View;
import com.test.R;
import com.test.RemapHelper;
import com.yanzhenjie.album.Action;
import com.yanzhenjie.album.Album;
import com.yanzhenjie.album.AlbumFile;
import com.yanzhenjie.album.api.widget.Widget;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.CameraActivity;
import org.opencv.android.CameraBridgeViewBase;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.core.Mat;
import org.opencv.core.Size;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class PortraitCaptureActivity extends CameraActivity implements CameraBridgeViewBase.CvCameraViewListener2{
    private final String TAG = this.getClass().getName();

    private LinearLayout ll_album,ll_capture;

    private TextView tv_album,tv_capture;
    private View v_line_album,v_line_capture;

    //相机相关控件
    private Tutorial3View mOpenCvCameraView;
    private View v_capture_back;
    private ImageView iv_capture_back,iv_sipin,iv_shangxia,iv_capture,iv_zuoyou,iv_no_mirro;
    private LinearLayout ll_sipin,ll_shangxia,ll_zuoyou,ll_no_mirro;
    private TextView tv_sipin,tv_shangxia,tv_zuoyou,tv_no_mirro;

    private Mat map_x;
    private Mat map_y;
    /**
     * mirro_type;
     * 0 正常拍摄
     * 1 左右镜像
     * 2 上下镜像
     * 3 四拼镜像
     */
    private int mirro_type;
    /**
     * mirro_type = 1 左右镜像 mirro_type_child:0垂直镜像,1垂直镜像且垂直翻转；
     * mirro_type = 2 上下镜像 mirro_type_child:水平镜像,1水平镜像且水平翻转；
     * mirro_type = 3 四拼镜像 mirro_type_child:0四拼镜像 ,1四拼镜像 且水平翻转 ,2四拼镜像 且垂直翻转,3四拼镜像 且水平垂直翻转
     */
    private int mirro_type_child;

    private Size size;
    private int cols;
    private int rows;
    private int threadFinisishedNum = 0;
    private long time;
    private int takePictureStatus = -1;// -1不拍照； 0 执行拍照；1正在拍照；3 拍照结束
    private int calculatedStatus = 0;//0未开始计算 ；1 计算中 ；2 计算结束
    private int START_TYPE;//1 切割 ；2 镜像拍;3 对花
    private boolean isDestroy = false;
    private Handler handler;
    private BaseLoaderCallback mLoaderCallback = new BaseLoaderCallback(this) {
        @Override
        public void onManagerConnected(int status) {
            switch (status) {
                case LoaderCallbackInterface.SUCCESS:
                {
                    Log.i(TAG, "OpenCV loaded successfully");
                    mOpenCvCameraView.enableView();
                } break;
                default:
                {
                    super.onManagerConnected(status);
                } break;
            }
        }
    };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.beeway_portrait_captures);
        hideNavigatonButton();
        isDestroy = false;
        ScreenUtil.setLayoutNum(this);
        init();

    }
    private void init(){
        getAllViews();
        setParams();
        setListener();
        boolean isHaveSdcardPermission = PermissionUtil.isHavePermission(this, PermissionUtil.TYPE.STORAGE);
        if(!isHaveSdcardPermission){
            PermissionUtil.startRequestPermission(this, PermissionUtil.TYPE.STORAGE);
        }
    }
    private void getAllViews(){

        ll_album = findViewById(R.id.ll_album);
        ll_capture = findViewById(R.id.ll_capture);
        tv_album = findViewById(R.id.tv_album);
        tv_capture = findViewById(R.id.tv_capture);
        v_line_album = findViewById(R.id.v_line_album);
        v_line_capture = findViewById(R.id.v_line_capture);

        //相机相关控件
        mOpenCvCameraView = (Tutorial3View) findViewById(R.id.cameraView);
        v_capture_back = findViewById(R.id.v_capture_back);
        iv_capture_back = findViewById(R.id.iv_capture_back);
        iv_sipin = findViewById(R.id.iv_sipin);
        iv_shangxia = findViewById(R.id.iv_shangxia);
        iv_capture = findViewById(R.id.iv_capture);
        iv_zuoyou = findViewById(R.id.iv_zuoyou);
        iv_no_mirro = findViewById(R.id.iv_no_mirro);
        ll_sipin = findViewById(R.id.ll_sipin);
        ll_shangxia = findViewById(R.id.ll_shangxia);
        ll_zuoyou = findViewById(R.id.ll_zuoyou);
        ll_no_mirro = findViewById(R.id.ll_no_mirro);
        tv_sipin = findViewById(R.id.tv_sipin);
        tv_shangxia = findViewById(R.id.tv_shangxia);
        tv_zuoyou = findViewById(R.id.tv_zuoyou);
        tv_no_mirro = findViewById(R.id.tv_no_mirro);
    }
    private void setParams(){
        mOpenCvCameraView.setVisibility(View.VISIBLE);
        mOpenCvCameraView.isPhone = true;

    }
    private void setListener(){
        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()){
                    case R.id.v_capture_back:
                        common.finishActivity(PortraitCaptureActivity.this);
                        break;
                    case R.id.ll_album:
                        selectAlbum();
                        break;
                    case R.id.ll_capture:
                        break;
                    case R.id.ll_sipin:
                        selectMirroType(3);
                        break;
                    case R.id.ll_shangxia:
                        selectMirroType(2);
                        break;
                    case R.id.iv_capture:
                        setCantakePicture();
                        break;
                    case R.id.ll_zuoyou:
                        selectMirroType(1);
                        break;
                    case R.id.ll_no_mirro:
                        selectMirroType(0);
                        break;
                }
            }
        };
        v_capture_back.setOnClickListener(listener);
        ll_album.setOnClickListener(listener);
        ll_sipin.setOnClickListener(listener);
        ll_shangxia.setOnClickListener(listener);
        iv_capture.setOnClickListener(listener);
        ll_zuoyou.setOnClickListener(listener);
        ll_no_mirro.setOnClickListener(listener);
        mOpenCvCameraView.setCvCameraViewListener(this);
    }
    public boolean onKeyDown(int keyCode, KeyEvent event) {
//        if (keyCode == KeyEvent.KEYCODE_BACK
//                && event.getRepeatCount() == 0) {
//            return true;
//        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void onCameraViewStarted(int width, int height) {

    }

    @Override
    public void onCameraViewStopped() {

    }

    @Override
    public Mat onCameraFrame(CameraBridgeViewBase.CvCameraViewFrame inputFrame) {
        Mat mat = inputFrame.rgba();
        try{
            doRemap(mat);
        }catch (Exception e){

        }
        if(takePictureStatus==0){
            takePictureStatus = 1;
            String fileName = System.currentTimeMillis()+".jpg";
            String path = null;
            path = FileUtil.createFilePathCache(this,"pictures")+fileName;
            Mat temp = new Mat();
            Imgproc.cvtColor(mat, temp, COLOR_RGBA2BGRA);
            Mat tempRotate = new Mat();
            RemapHelper.setRotate(temp.nativeObj,tempRotate.nativeObj,270);
            Imgcodecs.imwrite(path,tempRotate);
            temp.release();
            temp = null;
            tempRotate.release();
            tempRotate=null;
//            if(START_TYPE==1){
//                Intent in = new Intent();
//                in.putExtra("filePath",path);
//                in.putExtra("type", type);
//                in.putExtra("fileDir",filePath);
//                in.setClass(CaptureActivity.this, CutPictureActivity.class);
//                startActivity(in);
//            }else if(START_TYPE==2){
//                Intent in = new Intent();
//                in.putExtra("filePath",path);
//                if(type==0){
//                    in.putExtra("type", 0);
//                }else{
//                    in.putExtra("type", 1);
//                }
//                in.setClass(CaptureActivity.this, MirroPictureActivity.class);
//                startActivity(in);
//            }else if(START_TYPE==3){
//                Intent in = new Intent();
//                in.putExtra("filePath",path);
//                in.putExtra("type", type);
//                in.setClass(CaptureActivity.this, DuiHuaActivity.class);
//                startActivity(in);
//            }
            finish();
        }
        return mat;
    }
    private void doRemap(Mat mat){
        switch(mirro_type){
            case 0://正常拍摄
                break;
            case 1:
                if(map_x==null){
                    map_x =new Mat(mat.size(), CV_32FC1);
                    map_y =new Mat(mat.size(), CV_32FC1);
                    if(mirro_type_child==0){
                        RemapHelper.setRemap(mat.getNativeObjAddr(),map_x.getNativeObjAddr(),map_y.getNativeObjAddr(),12);
                    }
                    if(mirro_type_child==1){
                        RemapHelper.setRemap(mat.getNativeObjAddr(),map_x.getNativeObjAddr(),map_y.getNativeObjAddr(),11);
                    }
                }
                Imgproc.remap(mat,mat, map_x, map_y, Imgproc.INTER_LINEAR);
                break;
            case 2:
                if(map_x==null){
                    map_x =new Mat(mat.size(), CV_32FC1);
                    map_y =new Mat(mat.size(), CV_32FC1);
                    if(mirro_type_child==0){
                        RemapHelper.setRemap(mat.getNativeObjAddr(),map_x.getNativeObjAddr(),map_y.getNativeObjAddr(),21);
                    }
                    if(mirro_type_child==1){
                        RemapHelper.setRemap(mat.getNativeObjAddr(),map_x.getNativeObjAddr(),map_y.getNativeObjAddr(),22);
                    }
                }
                Imgproc.remap(mat,mat, map_x, map_y, Imgproc.INTER_LINEAR);
                break;
            case 3:
                if(map_x==null){
                    map_x =new Mat(mat.size(), CV_32FC1);
                    map_y =new Mat(mat.size(), CV_32FC1);
                    if(mirro_type_child==0){
                        RemapHelper.setRemap(mat.getNativeObjAddr(),map_x.getNativeObjAddr(),map_y.getNativeObjAddr(),33);

                    }
                    if(mirro_type_child==1){
                        RemapHelper.setRemap(mat.getNativeObjAddr(),map_x.getNativeObjAddr(),map_y.getNativeObjAddr(),31);
                    }
                    if(mirro_type_child==2){
                        RemapHelper.setRemap(mat.getNativeObjAddr(),map_x.getNativeObjAddr(),map_y.getNativeObjAddr(),34);
                    }
                    if(mirro_type_child==3){
                        RemapHelper.setRemap(mat.getNativeObjAddr(),map_x.getNativeObjAddr(),map_y.getNativeObjAddr(),32);
                    }
                }
                Imgproc.remap(mat,mat, map_x, map_y, Imgproc.INTER_LINEAR);
                break;
        }
    }
    private void selectMirroType(int mirro_type){
        if(this.mirro_type==mirro_type){
            switch (this.mirro_type){
                case 0://无镜像，返回，无处理
                    break;
                case 1:
                    switch (mirro_type_child){
                        case 0:
                            mirro_type_child=1;
                            break;
                        case 1:
                            mirro_type_child=0;
                            break;
                    }
                    break;
                case 2:
                    switch (mirro_type_child){
                        case 0:
                            mirro_type_child=1;
                            break;
                        case 1:
                            mirro_type_child=0;
                            break;
                    }
                    break;
                case 3:
                    switch (mirro_type_child){
                        case 0:
                            mirro_type_child=1;
                            break;
                        case 1:
                            mirro_type_child=2;
                            break;
                        case 2:
                            mirro_type_child=3;
                            break;
                        case 3:
                            mirro_type_child=0;
                            break;
                    }
                    break;
            }
            this.mirro_type = mirro_type;
        }else{
            this.mirro_type = mirro_type;
            mirro_type_child = 0;
        }
        if(map_x!=null){
            map_x.release();
            map_y.release();
            map_x = null;
            map_y = null;
        }
        showMirroButton();
    }
    private void setCantakePicture(){
        if(takePictureStatus!=1){
            takePictureStatus = 0;
        }
    }
    private void selectAlbum(){
        Album.image(this)
                .multipleChoice()
                .camera(true)
                .columnCount(3)
                .selectCount(1)
                .checkedList(new ArrayList<>())
                .setTakePictureType(START_TYPE)
                .setPhone(true)
                .widget(
                        Widget.newDarkBuilder(this)
                                .title("选择图片")
                                .build()
                )
                .onResult(new Action<ArrayList<AlbumFile>>() {
                    @Override
                    public void onAction( ArrayList<AlbumFile> result) {
                        if(result!=null&&result.size()>0){
//                            Intent in = new Intent();
//                            in.putExtra("album",result.get(0));
//                            in.putExtra("isChooseAlbum",true);//是否是相册中的文件
//                            if(START_TYPE==1){
//                                in.putExtra("fileDir",filePath);
//                                in.setClass(CaptureActivity.this, CutPictureActivity.class );
//                            }else if(START_TYPE==2){
//                                in.putExtra("type",0);
//                                in.setClass(CaptureActivity.this, MirroPictureActivity.class );
//                            }else if(START_TYPE==3){
//                                in.setClass(CaptureActivity.this, DuiHuaActivity.class );
//                            }
//                            startActivity(in);
//                            finish();
                        }else{
                        }
                    }
                })
                .onCancel(new Action<String>() {
                    @Override
                    public void onAction( String result) {

                    }
                })
                .start();
    }
    private void showMirroButton(){
        switch(mirro_type){
            case 0:
                tv_no_mirro.setTextColor(getResources().getColor(R.color.color39BDC8));
                tv_zuoyou.setTextColor(getResources().getColor(R.color.colorFFFFFF));
                tv_shangxia.setTextColor(getResources().getColor(R.color.colorFFFFFF));
                tv_sipin.setTextColor(getResources().getColor(R.color.colorFFFFFF));
                iv_no_mirro.setImageBitmap(ImageDealUtil.readBitMap(this,R.mipmap.beeway_icon_no_mirro_selected,false));
                iv_zuoyou.setImageBitmap(ImageDealUtil.readBitMap(this,R.mipmap.beeway_icon_zuoyou_mirro_unselected,false));
                iv_shangxia.setImageBitmap(ImageDealUtil.readBitMap(this,R.mipmap.beeway_icon_shangxia_mirro_unselected,false));
                iv_sipin.setImageBitmap(ImageDealUtil.readBitMap(this,R.mipmap.beeway_icon_sipin_mirro_unselected,false));
                break;
            case 1:
                tv_no_mirro.setTextColor(getResources().getColor(R.color.colorFFFFFF));
                tv_zuoyou.setTextColor(getResources().getColor(R.color.color39BDC8));
                tv_shangxia.setTextColor(getResources().getColor(R.color.colorFFFFFF));
                tv_sipin.setTextColor(getResources().getColor(R.color.colorFFFFFF));
                iv_no_mirro.setImageBitmap(ImageDealUtil.readBitMap(this,R.mipmap.beeway_icon_no_mirro_unselected,false));
                iv_shangxia.setImageBitmap(ImageDealUtil.readBitMap(this,R.mipmap.beeway_icon_shangxia_mirro_unselected,false));
                iv_sipin.setImageBitmap(ImageDealUtil.readBitMap(this,R.mipmap.beeway_icon_sipin_mirro_unselected,false));
                switch (mirro_type_child){
                    case 0:
                        iv_zuoyou.setImageBitmap(ImageDealUtil.readBitMap(this,R.mipmap.beeway_icon_zuoyou_selected_1,false));
                        break;
                    case 1:
                        iv_zuoyou.setImageBitmap(ImageDealUtil.readBitMap(this,R.mipmap.beeway_icon_zuoyou_selected_2,false));
                        break;
                }
                break;
            case 2:
                tv_no_mirro.setTextColor(getResources().getColor(R.color.colorFFFFFF));
                tv_zuoyou.setTextColor(getResources().getColor(R.color.colorFFFFFF));
                tv_shangxia.setTextColor(getResources().getColor(R.color.color39BDC8));
                tv_sipin.setTextColor(getResources().getColor(R.color.colorFFFFFF));
                iv_no_mirro.setImageBitmap(ImageDealUtil.readBitMap(this,R.mipmap.beeway_icon_no_mirro_unselected,false));
                iv_zuoyou.setImageBitmap(ImageDealUtil.readBitMap(this,R.mipmap.beeway_icon_zuoyou_mirro_unselected,false));
                iv_sipin.setImageBitmap(ImageDealUtil.readBitMap(this,R.mipmap.beeway_icon_sipin_mirro_unselected,false));
                switch (mirro_type_child){
                    case 0:
                        iv_shangxia.setImageBitmap(ImageDealUtil.readBitMap(this,R.mipmap.beeway_icon_shangxia_mirro_selected_1,false));
                        break;
                    case 1:
                        iv_shangxia.setImageBitmap(ImageDealUtil.readBitMap(this,R.mipmap.beeway_icon_shangxia_mirro_selected_2,false));
                        break;
                }
                break;
            case 3:
                tv_no_mirro.setTextColor(getResources().getColor(R.color.colorFFFFFF));
                tv_zuoyou.setTextColor(getResources().getColor(R.color.colorFFFFFF));
                tv_shangxia.setTextColor(getResources().getColor(R.color.colorFFFFFF));
                tv_sipin.setTextColor(getResources().getColor(R.color.color39BDC8));
                iv_no_mirro.setImageBitmap(ImageDealUtil.readBitMap(this,R.mipmap.beeway_icon_no_mirro_unselected,false));
                iv_zuoyou.setImageBitmap(ImageDealUtil.readBitMap(this,R.mipmap.beeway_icon_zuoyou_mirro_unselected,false));
                iv_shangxia.setImageBitmap(ImageDealUtil.readBitMap(this,R.mipmap.beeway_icon_shangxia_mirro_unselected,false));
                switch (mirro_type_child){
                    case 0:
                        iv_sipin.setImageBitmap(ImageDealUtil.readBitMap(this,R.mipmap.beeway_icon_sipin_mirro_select_1,false));
                        break;
                    case 1:
                        iv_sipin.setImageBitmap(ImageDealUtil.readBitMap(this,R.mipmap.beeway_icon_sipin_mirro_select_2,false));
                        break;
                    case 2:
                        iv_sipin.setImageBitmap(ImageDealUtil.readBitMap(this,R.mipmap.beeway_icon_sipin_mirro_select_3,false));
                        break;
                    case 3:
                        iv_sipin.setImageBitmap(ImageDealUtil.readBitMap(this,R.mipmap.beeway_icon_sipin_mirro_select_4,false));
                        break;
                }
                break;
        }

    }
    @Override
    public void onPause()
    {
        super.onPause();
        if (mOpenCvCameraView != null)
            mOpenCvCameraView.disableView();
    }
    @Override
    public void onResume()
    {
        super.onResume();
        if (!OpenCVLoader.initDebug()) {
            Log.d(TAG, "Internal OpenCV library not found. Using OpenCV Manager for initialization");
            OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_3_0_0, this, mLoaderCallback);
        } else {
            Log.d(TAG, "OpenCV library found inside package. Using it!");
            mLoaderCallback.onManagerConnected(LoaderCallbackInterface.SUCCESS);
        }
    }
    @Override
    protected List<? extends CameraBridgeViewBase> getCameraViewList() {
        return Collections.singletonList(mOpenCvCameraView);
    }
    public void onDestroy() {
        super.onDestroy();
        isDestroy = true;
        if (mOpenCvCameraView != null)
            mOpenCvCameraView.disableView();
    }
    private void hideSystemUI() {
        if (Build.VERSION.SDK_INT > 11 && Build.VERSION.SDK_INT < 19) {
            View v = this.getWindow().getDecorView();
            v.setSystemUiVisibility(View.GONE);
        } else if (Build.VERSION.SDK_INT >= 19) {
            //for new api versions.
            View decorView = getWindow().getDecorView();
            int uiOptions = View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION // hide nav bar
                    | View.SYSTEM_UI_FLAG_FULLSCREEN // hide status bar
                    | View.SYSTEM_UI_FLAG_IMMERSIVE;
            decorView.setSystemUiVisibility(uiOptions);
        }
    }
    private void hideNavigatonButton(){
        if(handler == null){
            handler = new Handler();
        }
        if(!isDestroy){
            hideSystemUI();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    hideNavigatonButton();
                }
            }, 5000);
        }
    }
}
