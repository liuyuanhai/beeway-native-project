package com.lyh.mvp.xiaochubi;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.view.MotionEvent;
import android.widget.Toast;

import com.lyh.util.OpenCVUtil;
import com.lyh.util.common;
import com.lyh.view.ImageCutView;
import com.test.R;

import org.opencv.core.Point;

import java.util.ArrayList;

/**
 * 负责数据逻辑
 */
public class XiaoChuModule {
    private ModuleInterface moduleInterface;//回调
    public XiaoChuModule(ModuleInterface moduleInterface){
        this.moduleInterface = moduleInterface;
    }
    public void getData(){
        //1.获取数据逻辑。
        //.......
        //2.将数据回调给parsener
        moduleInterface.getData("test");
    }




    public interface ModuleInterface{
        public void getData(String s );
    }
}
