package com.lyh.mvp.yingyangse;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Paint;

import com.lyh.brean.MySelectPath;
import com.test.R;

import java.util.ArrayList;

public interface YingYangSeView {
    public void setPaintSizeChanged(int size);//设置画笔粗细
    public void paintSizeChangedEnd();//设置画笔粗细结束
    public void setGradual(int width);//设置模糊渐变范围
    public void setGradualEnd();//设置模糊渐变范围结束

    public void xiaoChubiSetPre();//消除笔上一步
    public void xiaoChuBiSetNext();//消除笔下一步
    public void invalidateView();
    public void toastInfo(String content);//提示
    public void xiaoChuBiSetSectlectAreaStep(boolean isCanPre,boolean isCanNext);
}
