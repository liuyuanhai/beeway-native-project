package com.lyh.activity;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.lyh.mvp.xiaochubi.XiaoChuParsener;
import com.lyh.mvp.xiaochubi.XiaoChuView;
import com.lyh.mvp.yingyangse.YingYangSeParsener;
import com.lyh.mvp.yingyangse.YingYangSeView;
import com.lyh.util.FileUtil;
import com.lyh.util.ImageDealUtil;
import com.lyh.util.OpenCVUtil;
import com.lyh.util.ShapeUtil;
import com.lyh.util.common;
import com.lyh.view.ImageCutView;
import com.test.R;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

public class EditPictureActivity extends Activity {

    private View v_return;
    private ImageCutView cutView;
    private LinearLayout ll_jiaozheng,ll_tiaose,ll_xiaochubi,ll_yinyangse,ll_biaochi;
    private ImageView iv_jiaozheng,iv_tiaose,iv_xiaochubi,iv_yinyangse,iv_biaochi;
    private TextView tv_jiaozheng,tv_tiaose,tv_xiaochubi,tv_yinyangse,tv_biaochi;
    private RelativeLayout re_rotate,re_crop;
    private ImageView iv_rotate,iv_crop;
    private TextView tv_save;

    private LinearLayout ll_text_hint;
    private TextView tv_hint;

    private RelativeLayout re_other_views_show_2;
    private LinearLayout ll_jiaozheng_options;
    private LinearLayout ll_tiaose_options;
    private LinearLayout ll_xiaochubi_options;
    private RelativeLayout ll_yinyangse_options;
    private LinearLayout ll_crop_options;

    private Handler handler;
    private boolean isDestroy;
    private String filePath;//文件路径
    private String fileDir;//文件保存路径

    private XiaoChuParsener xiaoChuBiParsener;
    private XiaoChuView xiaoChuView;
    private YingYangSeParsener yingYangSeParsener;
    private YingYangSeView yingYangSeView;

    private BaseLoaderCallback mLoaderCallback = new BaseLoaderCallback(this) {
        @Override
        public void onManagerConnected(int status) {
            switch (status) {
                case LoaderCallbackInterface.SUCCESS:
                {

                } break;
                default:
                {
                    super.onManagerConnected(status);
                } break;
            }
        }
    };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.beeway_edit);
        fileDir = getExternalCacheDir().getAbsolutePath();
//        filePath = fileDir+File.separator+"aa.jpg";
                filePath = fileDir+File.separator+"test.jpg";
//        filePath = fileDir+File.separator+"test2.jpg";
//        filePath = fileDir+File.separator+"test_inpaint.jpg";
//        filePath = fileDir+File.separator+"test.jpg";
        isDestroy = false;
        hideNavigatonButton();
        init();
    }

    private void init() {
        getAllViews();
        setParams();
        initHandler();
        setListeners();

        initParsener();


    }
    private void initParsener(){
      initXiaoChuBiParsener();
      initYingYangSeParsener();
    }
    private void initXiaoChuBiParsener(){
        xiaoChuView = new XiaoChuView() {
            @Override
            public void xiaoChubiSetPre() {
                xiaoChubiShowFirstStep();
            }

            @Override
            public void xiaoChuBiSetNext() {
                xiaoChuBiShowSecondStep();
            }

            @Override
            public void invalidateView() {
                cutView.invalidate();
            }

            @Override
            public void toastInfo(String content) {
                Toast.makeText(EditPictureActivity.this,content,Toast.LENGTH_SHORT).show();
            }

            @Override
            public void xiaoChuBiSetSectlectAreaStep(boolean isCanPre, boolean isCanNext) {
                ImageView iv_pre = ll_xiaochubi_options.findViewById(R.id.iv_pre);
                ImageView iv_next = ll_xiaochubi_options.findViewById(R.id.iv_next);
                if(isCanPre){
                    iv_pre.setImageBitmap(ImageDealUtil.readBitMap(EditPictureActivity.this,R.mipmap.beeway_icon_pre_enable,false));
                    iv_pre.setEnabled(true);
                }else{
                    iv_pre.setImageBitmap(ImageDealUtil.readBitMap(EditPictureActivity.this,R.mipmap.beeway_icon_pre_unable,false));
                    iv_pre.setEnabled(false);
                }
                if(isCanNext){
                    iv_next.setImageBitmap(ImageDealUtil.readBitMap(EditPictureActivity.this,R.mipmap.beeway_icon_next_enable,false));
                    iv_next.setEnabled(true);
                }else{
                    iv_next.setImageBitmap(ImageDealUtil.readBitMap(EditPictureActivity.this,R.mipmap.beeway_icon_next_unable,false));
                    iv_next.setEnabled(false);
                }
            }
        };
        xiaoChuBiParsener = new XiaoChuParsener(xiaoChuView);
        xiaoChuBiParsener.initXiaoChuBi(this);
        cutView.setXiaoChuBiParsener(xiaoChuBiParsener);
    }
    private void initYingYangSeParsener(){
        yingYangSeView = new YingYangSeView() {

            @Override
            public void setPaintSizeChanged(int size) {

            }

            @Override
            public void paintSizeChangedEnd() {

            }

            @Override
            public void setGradual(int width) {

            }

            @Override
            public void setGradualEnd() {

            }

            @Override
            public void xiaoChubiSetPre() {

            }

            @Override
            public void xiaoChuBiSetNext() {

            }

            @Override
            public void invalidateView() {

            }

            @Override
            public void toastInfo(String content) {

            }

            @Override
            public void xiaoChuBiSetSectlectAreaStep(boolean isCanPre, boolean isCanNext) {

            }
        };
        yingYangSeParsener = new YingYangSeParsener(yingYangSeView);
    }

    private void getAllViews() {
        v_return = findViewById(R.id.v_return);
        cutView = findViewById(R.id.cutView);
        ll_jiaozheng = findViewById(R.id.ll_jiaozheng);
        ll_tiaose = findViewById(R.id.ll_tiaose);
        ll_xiaochubi = findViewById(R.id.ll_xiaochubi);
        ll_yinyangse = findViewById(R.id.ll_yinyangse);
        ll_biaochi = findViewById(R.id.ll_biaochi);
        iv_jiaozheng = findViewById(R.id.iv_jiaozheng);
        iv_tiaose = findViewById(R.id.iv_tiaose);
        iv_xiaochubi = findViewById(R.id.iv_xiaochubi);
        iv_yinyangse = findViewById(R.id.iv_yinyangse);
        iv_biaochi = findViewById(R.id.iv_biaochi);
        tv_jiaozheng = findViewById(R.id.tv_jiaozheng);
        tv_tiaose = findViewById(R.id.tv_tiaose);
        tv_xiaochubi = findViewById(R.id.tv_xiaochubi);
        tv_yinyangse = findViewById(R.id.tv_yinyangse);
        tv_save = findViewById(R.id.tv_save);
        re_rotate = findViewById(R.id.re_rotate);
        tv_biaochi = findViewById(R.id.tv_biaochi);
        re_crop = findViewById(R.id.re_crop);
        iv_rotate = findViewById(R.id.iv_rotate);
        iv_crop = findViewById(R.id.iv_crop);

        ll_text_hint = findViewById(R.id.ll_text_hint);
        tv_hint = findViewById(R.id.tv_hint);

        re_other_views_show_2  = findViewById(R.id.re_other_views_show_2);
    }
    private void setParams(){

        Bitmap bitmap;
        bitmap = getBitmapFromAssetsFile("cizhuantest.jpg");
//        bitmap = OpenCVUtil.getBitmap(true,this,filePath,null, ConstantData.maxDecodeImageWidth);
        if(bitmap==null) {
            Log.e("-------------->","bitmap==null");
        }else{
            Log.e("-------------->","bitmap!      =null");
        }
        cutView.setBitmap(bitmap);
        ll_text_hint.setBackground(ShapeUtil.getShape(false,0,0,true, common.Dp2Px(this,3),true,getResources().getColor(R.color.color24000000)));
    }
    private void initHandler(){
        handler = new Handler(){
            public void handleMessage(Message  msg){
                super.handleMessage(msg);
                switch(msg.what){
                    case 1:
                        break;
                }
            }

        };
    }

    private void setListeners() {
        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(v.getId()== R.id.v_return){
                    finish();
                }
//                if(v.getId()== R.id.v_rotate){
//                    cutView.rotate();
//                }
//                if(v.getId()== R.id.tv_recapture){
//                    Intent in = new Intent();
//                    in.setClass(MirroPictureActivity.this,CaptureActivity.class);
//                    in.putExtra("start_type",2);
//                    startActivity(in);
//                    finish();
//                }
//                if(v.getId()== R.id.tv_share){
//                    String path = FileUtil.createFilePathCache(MirroPictureActivity.this,"pictures")+System.currentTimeMillis()+".jpg";
//                    cutView.saveBitmap(path);
//                    Intent in = new Intent();
//                    in.setClass( MirroPictureActivity.this, ShareActivity.class);
//                    in.putExtra("filePath",path);
//                    startActivityForResult(in,SHARE_REQUEST_CODE);
//                }
//                if(v.getId()== R.id.tv_save_album){
//                    String path = FileUtil.createFilePathCache(MirroPictureActivity.this,"pictures")+System.currentTimeMillis()+".jpg";
//                    cutView.saveBitmap(path);
//                    if(Build.VERSION.SDK_INT > Build.VERSION_CODES.P){
//                        FileUtil.copyPrivateToAlbum(MirroPictureActivity.this,path,""+System.currentTimeMillis());
//                    }else{
//                        String publicPath = FileUtil.getSystemAlbumPath()+System.currentTimeMillis()+".jpg";
//                        FileUtil.copyfile(new File(path), new File(publicPath), true);
//                        Intent intent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
//                        Uri uri = Uri.fromFile(new File(publicPath));
//                        intent.setData(uri);
//                        sendBroadcast(intent);
//                    }
//                    Toast.makeText(MirroPictureActivity.this, "保存成功",Toast.LENGTH_LONG ).show();
//                }
//                if(v.getId()== R.id.tv_preview){
//                    cutView.preViewClick();
//                }

            }
        };
        v_return.setOnClickListener(listener);
//        v_rotate.setOnClickListener(listener);
//        tv_share.setOnClickListener(listener);
//        tv_save_album.setOnClickListener(listener);
//        tv_recapture.setOnClickListener(listener);
//        tv_preview.setOnClickListener(listener);

        View.OnClickListener viewListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch(v.getId()){
                    case R.id.ll_jiaozheng:
                        selectOption(1);
                        showHintText(false,null);
                        showJiaoZhengOptions();
                        cutView.setCURRENT_OPTION_TYPE(1);
                        break;
                    case R.id.ll_tiaose:
                        selectOption(2);
                        showHintText(false,null);
                        cutView.setCURRENT_OPTION_TYPE(2);
                        showTiaoSeOptions();
                        break;
                    case R.id.ll_xiaochubi:
                        selectOption(3);
                        showHintText(true,getResources().getString(R.string.xiaochubi_hint));
                        cutView.setCURRENT_OPTION_TYPE(3);
                        showXiaoChuBiOptions();
                        break;
                    case R.id.ll_yinyangse:
                        selectOption(4);
                        cutView.setCURRENT_OPTION_TYPE(4);
                        showYinYangSeOptions();
                        break;
                    case R.id.ll_biaochi:
                        selectOption(5);
                        showHintText(false,null);
                        cutView.setCURRENT_OPTION_TYPE(5);
                        closeOptionsView();
                        break;
                    case R.id.re_rotate:
                        selectOption(6);
                        showHintText(false,null);
                        closeOptionsView();
                        cutView.setCURRENT_OPTION_TYPE(6);
                        cutView.rotate();
                        break;
                    case R.id.re_crop:
                        showHintText(false,null);
                        selectOption(7);
                        showCropOptions();
                        cutView.setCURRENT_OPTION_TYPE(7);
                        break;
                    case R.id.tv_save:
                        String biaoChiNoGraduationPath = FileUtil.createFilePathCache(EditPictureActivity.this,"temp")+"biaochiOriginal.png";
                        String biaoChiHaveGraduationPath = FileUtil.createFilePathCache(EditPictureActivity.this,"temp")+"biaochi.png";
                        String biaoChiHaveGraduationPath1 = FileUtil.createFilePathCache(EditPictureActivity.this,"temp")+"biaochi01.png";
                        String biaoChiHaveGraduationPath2 = FileUtil.createFilePathCache(EditPictureActivity.this,"temp")+"biaochi02.png";
                        String biaoChiHaveGraduationPath3 = FileUtil.createFilePathCache(EditPictureActivity.this,"temp")+"biaochi03.png";
                        OpenCVUtil.saveBiaoChiPictureNoGraduation(2,EditPictureActivity.this,null,2500,2500,400f/1000f,biaoChiNoGraduationPath);
                        OpenCVUtil.saveBiaoChiPictureHaveGraduation(2,EditPictureActivity.this,null,200,200,400f/1000f,biaoChiHaveGraduationPath);
                        OpenCVUtil.saveBiaoChiPictureHaveGraduation(2,EditPictureActivity.this,null,600,600,400f/1000f,biaoChiHaveGraduationPath1);
                        OpenCVUtil.saveBiaoChiPictureHaveGraduation(2,EditPictureActivity.this,null,2000,2000,400f/1000f,biaoChiHaveGraduationPath2);
                        OpenCVUtil.saveBiaoChiPictureHaveGraduation(2,EditPictureActivity.this,null,6000,6000,400f/1000f,biaoChiHaveGraduationPath3);
                        //保存标尺图片
                        break;
                }
            }
        };
        ll_jiaozheng.setOnClickListener(viewListener);
        ll_tiaose.setOnClickListener(viewListener);
        ll_xiaochubi.setOnClickListener(viewListener);
        ll_yinyangse.setOnClickListener(viewListener);
        ll_biaochi.setOnClickListener(viewListener);
        re_rotate.setOnClickListener(viewListener);
        re_crop.setOnClickListener(viewListener);
        tv_save.setOnClickListener(viewListener);

    }
    private void hideSystemUI() {
        if (Build.VERSION.SDK_INT > 11 && Build.VERSION.SDK_INT < 19) {
            View v = this.getWindow().getDecorView();
            v.setSystemUiVisibility(View.GONE);
        } else if (Build.VERSION.SDK_INT >= 19) {
            //for new api versions.
            View decorView = getWindow().getDecorView();
            int uiOptions = View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION // hide nav bar
                    | View.SYSTEM_UI_FLAG_FULLSCREEN // hide status bar
                    | View.SYSTEM_UI_FLAG_IMMERSIVE;
            decorView.setSystemUiVisibility(uiOptions);
        }
    }
    private void hideNavigatonButton(){
        if(handler == null){
            handler = new Handler();
        }
        if(!isDestroy){
            hideSystemUI();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    hideNavigatonButton();
                }
            }, 5000);
        }
    }
    public void onDestroy(){
        super.onDestroy();
        isDestroy = true;

    }
    @Override
    public void onResume()
    {
        super.onResume();
        if (!OpenCVLoader.initDebug()) {
            Log.d("tag", "Internal OpenCV library not found. Using OpenCV Manager for initialization");
            OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_3_0_0, this, mLoaderCallback);
        } else {
            Log.d("tag", "OpenCV library found inside package. Using it!");
            mLoaderCallback.onManagerConnected(LoaderCallbackInterface.SUCCESS);
        }
    }

    /**
     * 0初始状态 ,1.矫正 ；2 调色 ；3 消除笔；4 阴阳色 ；5 添加标尺;6旋转 ；7 裁剪
     * @param optionType
     */
    private void selectOption(int optionType){
        isSelectedJiaoZheng(false);
        isSelectedTiaoSe(false);
        isSelectedXiaoChuBi(false);
        isSelectedYinYangSe(false);
        isSelectedAddBiaoChi(false);
        isSelectedRotate(false);
        isSelectedCrop(false);
        switch (optionType){
            case 1:
                isSelectedJiaoZheng(true);
                break;
            case 2:
                isSelectedTiaoSe(true);
                break;
            case 3:
                isSelectedXiaoChuBi(true);
                break;
            case 4:
                isSelectedYinYangSe(true);
                break;
            case 5:
                isSelectedAddBiaoChi(true);
                break;
            case 6:
                isSelectedRotate(true);
                break;
            case 7:
                isSelectedCrop(true);

                break;
        }
    }
    private void isSelectedJiaoZheng(boolean isSelected){
        if(isSelected){
            iv_jiaozheng.setImageBitmap(ImageDealUtil.readBitMap(this,R.mipmap.beeway_icon_jiaozheng_selected,false));
            tv_jiaozheng.setTextColor(getResources().getColor(R.color.color4CB2BD));
        }else{
            iv_jiaozheng.setImageBitmap(ImageDealUtil.readBitMap(this,R.mipmap.beeway_icon_jiaozheng_unselected,false));
            tv_jiaozheng.setTextColor(getResources().getColor(R.color.colorFFFFFF));
        }
    }
    private void isSelectedTiaoSe(boolean isSelected){
        if(isSelected){
            iv_tiaose.setImageBitmap(ImageDealUtil.readBitMap(this,R.mipmap.beeway_icon_tiaose_selected,false));
            tv_tiaose.setTextColor(getResources().getColor(R.color.color4CB2BD));
        }else{
            iv_tiaose.setImageBitmap(ImageDealUtil.readBitMap(this,R.mipmap.beeway_icon_tiaose_unselected,false));
            tv_tiaose.setTextColor(getResources().getColor(R.color.colorFFFFFF));
        }
    }
    private void isSelectedXiaoChuBi(boolean isSelected){
        if(isSelected){
            iv_xiaochubi.setImageBitmap(ImageDealUtil.readBitMap(this,R.mipmap.beeway_icon_xiaochubi_selected,false));
            tv_xiaochubi.setTextColor(getResources().getColor(R.color.color4CB2BD));
        }else{
            iv_xiaochubi.setImageBitmap(ImageDealUtil.readBitMap(this,R.mipmap.beeway_icon_xiaochubi_unselected,false));
            tv_xiaochubi.setTextColor(getResources().getColor(R.color.colorFFFFFF));
        }
    }
    private void isSelectedYinYangSe(boolean isSelected){
        if(isSelected){
            iv_yinyangse.setImageBitmap(ImageDealUtil.readBitMap(this,R.mipmap.beeway_icon_yinyangse_selected,false));
            tv_yinyangse.setTextColor(getResources().getColor(R.color.color4CB2BD));
        }else{
            iv_yinyangse.setImageBitmap(ImageDealUtil.readBitMap(this,R.mipmap.beeway_icon_yinyangse_unselected,false));
            tv_yinyangse.setTextColor(getResources().getColor(R.color.colorFFFFFF));
        }
    }
    private void isSelectedAddBiaoChi(boolean isSelected){
        if(isSelected){
            iv_biaochi.setImageBitmap(ImageDealUtil.readBitMap(this,R.mipmap.beeway_icon_biaochi_selected,false));
            tv_biaochi.setTextColor(getResources().getColor(R.color.color4CB2BD));
        }else{
            iv_biaochi.setImageBitmap(ImageDealUtil.readBitMap(this,R.mipmap.beeway_icon_biaochi_unselected,false));
            tv_biaochi.setTextColor(getResources().getColor(R.color.colorFFFFFF));
        }
    }
    private void isSelectedRotate(boolean isSelected){
        if(isSelected){
            iv_rotate.setImageBitmap(ImageDealUtil.readBitMap(this,R.mipmap.beeway_icon_rotate_selected,false));
        }else{
            iv_rotate.setImageBitmap(ImageDealUtil.readBitMap(this,R.mipmap.beeway_icon_rotate_unselected,false));
        }
    }
    private void isSelectedCrop(boolean isSelected){
        if(isSelected){
            iv_crop.setImageBitmap(ImageDealUtil.readBitMap(this,R.mipmap.beeway_icon_crop_selected,false));
        }else{
            iv_crop.setImageBitmap(ImageDealUtil.readBitMap(this,R.mipmap.beeway_icon_crop_unselected,false));
        }
    }
    private void showHintText(boolean isShow,String text){
        if(isShow){
            tv_hint.setText(text);
            ll_text_hint.setVisibility(View.VISIBLE);
        }else{
            ll_text_hint.setVisibility(View.GONE);
        }
    }
    private void showJiaoZhengOptions(){
        closeOptionsView();
        if(ll_jiaozheng_options!=null){
            re_other_views_show_2.addView(ll_jiaozheng_options);
        }else{
            ll_jiaozheng_options = (LinearLayout) LayoutInflater.from(this).inflate(R.layout.beeway_edit_view_jiaozheng_options,null);
            ImageView iv_close = ll_jiaozheng_options.findViewById(R.id.iv_close);
            ImageView iv_ok = ll_jiaozheng_options.findViewById(R.id.iv_ok);
            View.OnClickListener listener = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    switch(v.getId()){
                        case R.id.iv_close:
                            cutView.setCURRENT_OPTION_TYPE(0);
                            closeOptionsView();
                            break;
                        case R.id.iv_ok:
                            cutView.setJiaoZheng();
                            selectOption(0);
                            closeOptionsView();
                            break;
                    }
                }
            };
            iv_close.setOnClickListener(listener);
            iv_ok.setOnClickListener(listener);
            re_other_views_show_2.addView(ll_jiaozheng_options);

        }
    }
    //显示调色操作项
    private void showTiaoSeOptions() {
        closeOptionsView();
        if (ll_tiaose_options != null) {
            re_other_views_show_2.addView(ll_tiaose_options);
            View v_progress_light = ll_tiaose_options.findViewById(R.id.v_progress_light);
            View v_block_light = ll_tiaose_options.findViewById(R.id.v_block_light);
            RelativeLayout.LayoutParams p_progress = (RelativeLayout.LayoutParams) v_progress_light.getLayoutParams();
            RelativeLayout.LayoutParams p = (RelativeLayout.LayoutParams) v_block_light.getLayoutParams();
            p.leftMargin = p_progress.leftMargin + v_progress_light.getWidth() / 2 - common.Dp2Px(EditPictureActivity.this, 3);
            v_block_light.setLayoutParams(p);

            View v_block_colorvalue = ll_tiaose_options.findViewById(R.id.v_block_colorvalue);
            RelativeLayout.LayoutParams p2 = (RelativeLayout.LayoutParams) v_block_colorvalue.getLayoutParams();
            p2.leftMargin = common.Dp2Px(EditPictureActivity.this, 3);
            v_block_colorvalue.setLayoutParams(p2);
        } else {
            Log.e("------------>", "showTiaoSeOptions first");

            ll_tiaose_options = (LinearLayout) LayoutInflater.from(EditPictureActivity.this).inflate(R.layout.beeway_edit_view_tiaose_options, null);
            re_other_views_show_2.addView(ll_tiaose_options);
            View v_progress_light = ll_tiaose_options.findViewById(R.id.v_progress_light);
            View v_block_light = ll_tiaose_options.findViewById(R.id.v_block_light);
            v_progress_light.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
                @Override
                public boolean onPreDraw() {
                    int progressWith = v_progress_light.getWidth(); // 获取宽度
                    int progressHeight = v_progress_light.getHeight(); // 获取高度
                    Log.e("------------>", "progressWith = " + progressWith);

                    RelativeLayout.LayoutParams p_progress = (RelativeLayout.LayoutParams) v_progress_light.getLayoutParams();
                    Log.e("------------>", "progressWith = " + p_progress.width);
                    Log.e("------------>", "p_progress.leftMargin = " + p_progress.leftMargin);

                    RelativeLayout.LayoutParams p = (RelativeLayout.LayoutParams) v_block_light.getLayoutParams();
                    p.leftMargin = p_progress.leftMargin + progressWith / 2 - common.Dp2Px(EditPictureActivity.this, 3);
                    v_block_light.setLayoutParams(p);
                    v_block_light.requestLayout();
                    v_progress_light.getViewTreeObserver().removeOnPreDrawListener(this);
                    return true;
                }
            });
            v_block_light.setOnTouchListener(new View.OnTouchListener() {
                private float lastX;

                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    switch (event.getAction()) {
                        case MotionEvent.ACTION_DOWN:
                            lastX = event.getRawX();
                            break;
                        case MotionEvent.ACTION_MOVE:
                            RelativeLayout.LayoutParams p_block = (RelativeLayout.LayoutParams) v_block_light.getLayoutParams();
                            int leftMargin = p_block.leftMargin;
                            int moveX = (int) (event.getRawX() - lastX);
                            lastX = event.getRawX();
                            if (moveX <= 0) {//左移
                                if (leftMargin + moveX <= 0) {
                                    p_block.leftMargin = 0;
                                } else {
                                    p_block.leftMargin = leftMargin + moveX;
                                }
                            } else {//右移
                                RelativeLayout.LayoutParams p_progress = (RelativeLayout.LayoutParams) v_progress_light.getLayoutParams();
                                if (leftMargin + moveX >= v_progress_light.getWidth()) {
                                    p_block.leftMargin = v_progress_light.getWidth();
                                } else {
                                    p_block.leftMargin = leftMargin + moveX;
                                }
                            }
                            int light = (v_progress_light.getWidth() / 2 - p_block.leftMargin) * 100 / (v_progress_light.getWidth() / 2);
                            cutView.setBaipinghengAndLight(2, light,0);
                            v_block_light.setLayoutParams(p_block);
                            break;
                        case MotionEvent.ACTION_UP:
                            break;
                    }
                    return true;
                }
            });

            View v_progress_colorvalue = ll_tiaose_options.findViewById(R.id.v_progress_colorvalue);
            View v_block_colorvalue = ll_tiaose_options.findViewById(R.id.v_block_colorvalue);
//      v_progress_colorvalue.setBackground(ShapeUtil.getColorsValueShape(getContext()));
            v_progress_colorvalue.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
                @Override
                public boolean onPreDraw() {
                    int progressWith = v_progress_light.getWidth();
                    int progressHeight = v_progress_light.getHeight();
                    RelativeLayout.LayoutParams p_progress = (RelativeLayout.LayoutParams) v_progress_colorvalue.getLayoutParams();
                    RelativeLayout.LayoutParams p = (RelativeLayout.LayoutParams) v_block_colorvalue.getLayoutParams();
                    p.leftMargin = progressWith + common.Dp2Px(EditPictureActivity.this, 3);
                    v_block_colorvalue.setLayoutParams(p);
                    v_block_colorvalue.requestLayout();
                    v_progress_colorvalue.getViewTreeObserver().removeOnPreDrawListener(this);

                    return true;
                }
            });
            v_block_colorvalue.setOnTouchListener(new View.OnTouchListener() {
                private float lastX;

                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    switch (event.getAction()) {
                        case MotionEvent.ACTION_DOWN:
                            lastX = event.getRawX();
                            break;
                        case MotionEvent.ACTION_MOVE:
                            RelativeLayout.LayoutParams p_block = (RelativeLayout.LayoutParams) v_block_colorvalue.getLayoutParams();
                            int leftMargin = p_block.leftMargin;
                            int moveX = (int) (event.getRawX() - lastX);
                            lastX = event.getRawX();
                            if (moveX <= 0) {//左移
                                if (leftMargin + moveX <= 0) {
                                    p_block.leftMargin = 0;
                                } else {
                                    p_block.leftMargin = leftMargin + moveX;
                                }
                            } else {//右移
                                RelativeLayout.LayoutParams p_progress = (RelativeLayout.LayoutParams) v_progress_colorvalue.getLayoutParams();
                                if (leftMargin + moveX >= v_progress_colorvalue.getWidth()) {
                                    p_block.leftMargin = v_progress_colorvalue.getWidth();
                                } else {
                                    p_block.leftMargin = leftMargin + moveX;
                                }
                            }
                            float baipingheng = (float)(v_progress_colorvalue.getWidth()+common.Dp2Px(EditPictureActivity.this,3) - p_block.leftMargin)/ v_progress_colorvalue.getWidth();
                            v_block_colorvalue.setLayoutParams(p_block);
                            cutView.setBaipinghengAndLight(1,0,1- baipingheng);
                            break;
                        case MotionEvent.ACTION_UP:
                            break;
                    }
                    return true;
                }
            });
        }
    }
    //显示消除笔操作项
    private void showXiaoChuBiOptions(){
        closeOptionsView();
        xiaoChuBiParsener.resetData();
        xiaoChuBiParsener.setData(cutView.getBitmapInit(),cutView.getScale());
        if(ll_xiaochubi_options!=null){
            re_other_views_show_2.addView(ll_xiaochubi_options);
            xiaoChubiShowFirstStep();
        }else{
            ll_xiaochubi_options = (LinearLayout) LayoutInflater.from(this).inflate(R.layout.beeway_edit_view_xiaochubi_opitions,null);
            re_other_views_show_2.addView(ll_xiaochubi_options);
            ImageView iv_pre = ll_xiaochubi_options.findViewById(R.id.iv_pre);
            ImageView iv_next = ll_xiaochubi_options.findViewById(R.id.iv_next);
            View v_progress = ll_xiaochubi_options.findViewById(R.id.v_progress);
            View v_block = ll_xiaochubi_options.findViewById(R.id.v_block);
            TextView tv_pre = ll_xiaochubi_options.findViewById(R.id.tv_pre);
            TextView tv_next = ll_xiaochubi_options.findViewById(R.id.tv_next);
            v_progress.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    int progressWith = v_progress.getMeasuredWidth();
                    RelativeLayout.LayoutParams p_progress = (RelativeLayout.LayoutParams)v_progress.getLayoutParams();
                    RelativeLayout.LayoutParams p = (RelativeLayout.LayoutParams)v_block.getLayoutParams();
                    p.leftMargin = p_progress.leftMargin+progressWith/2-common.Dp2Px(EditPictureActivity.this,6);
                    v_block.setLayoutParams(p);
                    getWindow().getDecorView().getViewTreeObserver().removeOnGlobalLayoutListener(this);
                }
            });
            v_block.setOnTouchListener(new View.OnTouchListener() {
                private float lastX;
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    switch (event.getAction()){
                        case MotionEvent.ACTION_DOWN:
                            lastX = event.getRawX();
                            break;
                        case MotionEvent.ACTION_MOVE:
                            RelativeLayout.LayoutParams p_block = (RelativeLayout.LayoutParams)v_block.getLayoutParams();
                            int leftMargin = p_block.leftMargin;
                            int moveX = (int)(event.getRawX()-lastX);
                            lastX = event.getRawX();
                            if(moveX<=0){//左移
                                if(leftMargin+moveX<=0){
                                    p_block.leftMargin = 0;
                                }else{
                                    p_block.leftMargin = leftMargin+moveX;
                                }
                            }else {//右移
                                RelativeLayout.LayoutParams p_progress = (RelativeLayout.LayoutParams) v_progress.getLayoutParams();
                                if (leftMargin + moveX >= v_progress.getWidth()) {
                                    p_block.leftMargin = v_progress.getWidth();
                                } else {
                                    p_block.leftMargin = leftMargin + moveX;
                                }
                            }
                            float a = (float)(v_progress.getWidth()-(p_block.leftMargin+common.Dp2Px(EditPictureActivity.this,3)))/v_progress.getWidth();

                            v_block.setLayoutParams(p_block);
                            if(xiaoChuBiParsener.getXiaoChuBiStep()==1){
                                int xiaoChuBiR = common.Dp2Px(EditPictureActivity.this,24)
                                        +(int)(common.Dp2Px(EditPictureActivity.this,20)*a);
                                xiaoChuBiParsener.xiaoChuBiSetPaintWidth(xiaoChuBiR,true);
                            }else{
                                int xiaoChuBiBorder =(int)(common.Dp2Px(EditPictureActivity.this,20)*a);
                                xiaoChuBiParsener.setXiaoChuBiReplReplaceScope(xiaoChuBiBorder,true);
                            }
                            break;
                        case MotionEvent.ACTION_UP:
                        case MotionEvent.ACTION_CANCEL:
                            if(xiaoChuBiParsener.getXiaoChuBiStep()==1){
                                xiaoChuBiParsener.xiaoChuBiSetPaintWidth(-1,false);
                            }else{
                                xiaoChuBiParsener.setXiaoChuBiReplReplaceScope(-1,false);
                            }
                            break;
                    }
                    return true;
                }
            });
            View.OnClickListener listener =new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    switch(v.getId()){
                        case R.id.iv_pre:
                            cutView.xiaoChuBiSelectArea(1);
                            break;
                        case R.id.iv_next:
                            cutView.xiaoChuBiSelectArea(2);
                            break;
                        case R.id.tv_finish:
                            xiaoChuBiParsener.setNext();
                            break;
                        case R.id.tv_pre:
                            xiaoChuView.xiaoChubiSetPre();
                            showHintText(true,"手指涂抹瑕疵区域");
                            xiaoChuBiParsener.setPre();
                            break;
                        case R.id.tv_next:
                            showHintText(true,"滑动手指,选择替换区域");
                            xiaoChuBiParsener.setNext();
                            break;
                    }

                }
            };
            iv_pre.setOnClickListener(listener);
            iv_next.setOnClickListener(listener);
            tv_pre.setOnClickListener(listener);
            tv_next.setOnClickListener(listener);
        }
    }
    public void setXiaoChuBiSatus(boolean isCanPre,boolean isCanNext){
        ImageView iv_pre = ll_xiaochubi_options.findViewById(R.id.iv_pre);
        ImageView iv_next = ll_xiaochubi_options.findViewById(R.id.ll_xiaochubi);
        View v_progress = ll_xiaochubi_options.findViewById(R.id.v_progress);
        View v_block = ll_xiaochubi_options.findViewById(R.id.v_block);
        if(isCanPre){
            iv_pre.setImageBitmap(ImageDealUtil.readBitMap(EditPictureActivity.this,R.mipmap.beeway_icon_pre_enable,false));
        }else{
            iv_pre.setImageBitmap(ImageDealUtil.readBitMap(EditPictureActivity.this,R.mipmap.beeway_icon_pre_unable,false));
        }
        if(isCanNext){
            iv_next.setImageBitmap(ImageDealUtil.readBitMap(EditPictureActivity.this,R.mipmap.beeway_icon_next_enable,false));
        }else{
            iv_next.setImageBitmap(ImageDealUtil.readBitMap(EditPictureActivity.this,R.mipmap.beeway_icon_next_unable,false));
        }
    }
    //显示阴阳色操作项
    private void showYinYangSeOptions(){
        closeOptionsView();
        if(ll_yinyangse_options!=null){
            re_other_views_show_2.addView(ll_yinyangse_options);
            View v_progress = ll_yinyangse_options.findViewById(R.id.v_progress);
            View v_block = ll_yinyangse_options.findViewById(R.id.v_block);
            LinearLayout ll_yinyangese_step_1 = ll_yinyangse_options.findViewById(R.id.ll_yinyangese_step_1);
            LinearLayout ll_yinyangse_step_2 = ll_yinyangse_options.findViewById(R.id.ll_yinyangse_step_2);
            ll_yinyangese_step_1.setVisibility(View.VISIBLE);
            ll_yinyangse_step_2.setVisibility(View.INVISIBLE);
            RelativeLayout.LayoutParams p_progress = (RelativeLayout.LayoutParams)v_progress.getLayoutParams();
            RelativeLayout.LayoutParams p = (RelativeLayout.LayoutParams)v_block.getLayoutParams();
            p.leftMargin = p_progress.leftMargin+v_progress.getWidth()/2-common.Dp2Px(EditPictureActivity.this,6);
            v_block.setLayoutParams(p);
        }else{
            ll_yinyangse_options = (RelativeLayout) LayoutInflater.from(this).inflate(R.layout.beeway_edit_view_yingyangse_options,null);
            ll_yinyangse_options.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT,common.Dp2Px(EditPictureActivity.this,120)));
            re_other_views_show_2.addView(ll_yinyangse_options);
            LinearLayout ll_yinyangese_step_1 = ll_yinyangse_options.findViewById(R.id.ll_yinyangese_step_1);
            LinearLayout ll_yinyangse_step_2 = ll_yinyangse_options.findViewById(R.id.ll_yinyangse_step_2);
            ll_yinyangese_step_1.setVisibility(View.VISIBLE);
            ll_yinyangse_step_2.setVisibility(View.INVISIBLE);
            //step1
            ImageView iv_pre = ll_yinyangse_options.findViewById(R.id.iv_pre);
            ImageView iv_next = ll_yinyangse_options.findViewById(R.id.iv_next);
            View v_progress = ll_yinyangse_options.findViewById(R.id.v_progress);
            View v_block = ll_yinyangse_options.findViewById(R.id.v_block);
            TextView tv_pre = ll_yinyangse_options.findViewById(R.id.tv_pre);
            TextView tv_next = ll_yinyangse_options.findViewById(R.id.tv_next);
            v_progress.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    int progressWith = v_progress.getMeasuredWidth();
                    RelativeLayout.LayoutParams p_progress = (RelativeLayout.LayoutParams)v_progress.getLayoutParams();
                    RelativeLayout.LayoutParams p = (RelativeLayout.LayoutParams)v_block.getLayoutParams();
                    p.leftMargin = p_progress.leftMargin+progressWith/2-common.Dp2Px(EditPictureActivity.this,6);
                    v_block.setLayoutParams(p);
                    getWindow().getDecorView().getViewTreeObserver().removeOnGlobalLayoutListener(this);
                }
            });
            v_block.setOnTouchListener(new View.OnTouchListener() {
                private float lastX;
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    switch (event.getAction()){
                        case MotionEvent.ACTION_DOWN:
                            lastX = event.getRawX();
                            break;
                        case MotionEvent.ACTION_MOVE:
//                            RelativeLayout.LayoutParams p_block = (RelativeLayout.LayoutParams)v_block.getLayoutParams();
//                            int leftMargin = p_block.leftMargin;
//                            int moveX = (int)(event.getRawX()-lastX);
//                            lastX = event.getRawX();
//                            if(moveX<=0){//左移
//                                if(leftMargin+moveX<=0){
//                                    p_block.leftMargin = 0;
//                                }else{
//                                    p_block.leftMargin = leftMargin+moveX;
//                                }
//                            }else {//右移
//                                RelativeLayout.LayoutParams p_progress = (RelativeLayout.LayoutParams) v_progress.getLayoutParams();
//                                if (leftMargin + moveX >= v_progress.getWidth()) {
//                                    p_block.leftMargin = v_progress.getWidth();
//                                } else {
//                                    p_block.leftMargin = leftMargin + moveX;
//                                }
//                            }
//                            float a = (float)(v_progress.getWidth()-(p_block.leftMargin+common.Dp2Px(EditPictureActivity.this,3)))/v_progress.getWidth();
//
//                            v_block.setLayoutParams(p_block);
//                            if(yingYangSeParsener.getXiaoChuBiStep()==1){
//                                int xiaoChuBiR = common.Dp2Px(EditPictureActivity.this,24)
//                                        +(int)(common.Dp2Px(EditPictureActivity.this,20)*a);
//                                yingYangSeParsener.xiaoChuBiSetPaintWidth(xiaoChuBiR,true);
//                            }else{
//                                int xiaoChuBiBorder =(int)(common.Dp2Px(EditPictureActivity.this,20)*a);
//                                yingYangSeParsener.setXiaoChuBiReplReplaceScope(xiaoChuBiBorder,true);
//                            }
//                            break;
//                        case MotionEvent.ACTION_UP:
//                        case MotionEvent.ACTION_CANCEL:
//                            if(yingYangSeParsener.getXiaoChuBiStep()==1){
//                                yingYangSeParsener.xiaoChuBiSetPaintWidth(-1,false);
//                            }else{
//                                yingYangSeParsener.setXiaoChuBiReplReplaceScope(-1,false);
//                            }
//                            break;
                    }
                    return true;
                }
            });
//            View.OnClickListener listener =new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    switch(v.getId()){
//                        case R.id.iv_pre:
//                            cutView.xiaoChuBiSelectArea(1);
//                            break;
//                        case R.id.iv_next:
//                            cutView.xiaoChuBiSelectArea(2);
//                            break;
//                        case R.id.tv_finish:
//                            xiaoChuBiParsener.setNext();
//                            break;
//                        case R.id.tv_pre:
//                            xiaoChuView.xiaoChubiSetPre();
//                            showHintText(true,"手指涂抹瑕疵区域");
//                            xiaoChuBiParsener.setPre();
//                            break;
//                        case R.id.tv_next:
//                            showHintText(true,"滑动手指,选择替换区域");
//                            xiaoChuBiParsener.setNext();
//                            break;
//                    }
//
//                }
//            };
//            iv_pre.setOnClickListener(listener);
//            iv_next.setOnClickListener(listener);
//            tv_pre.setOnClickListener(listener);
//            tv_next.setOnClickListener(listener);
        }


    }
    private void showCropOptions(){
        closeOptionsView();
        if(ll_crop_options!=null){
            re_other_views_show_2.addView(ll_crop_options);
        }else{
            ll_crop_options = (LinearLayout) LayoutInflater.from(this).inflate(R.layout.beeway_edit_view_crop_options,null);
            ImageView iv_close = ll_crop_options.findViewById(R.id.iv_close);
            ImageView iv_ok = ll_crop_options.findViewById(R.id.iv_ok);
            View.OnClickListener listener = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    switch(v.getId()){
                        case R.id.iv_close:
                            cutView.setCURRENT_OPTION_TYPE(0);
                            closeOptionsView();
                            break;
                        case R.id.iv_ok:
                            cutView.setJiaoZheng();
                            selectOption(0);
                            closeOptionsView();
                            break;
                    }
                }
            };
            iv_close.setOnClickListener(listener);
            iv_ok.setOnClickListener(listener);
            re_other_views_show_2.addView(ll_crop_options);

        }

    }

    private void closeOptionsView(){
        re_other_views_show_2.removeAllViews();
    }
    private Bitmap getBitmapFromAssetsFile(String filename){
        Bitmap bitmap;
        try {
            InputStream ims = this.getAssets().open(filename);
            // 读入图片并将其强转为 BitmapDrawable类型
            BitmapDrawable bd = (BitmapDrawable) Drawable.createFromStream(ims, null);
            bitmap = bd.getBitmap();
            ims.close();
            return bitmap;
        }
        catch(IOException ex) {
            return null;
        }
    }


    public void xiaoChubiShowFirstStep() {
        TextView tv_xiaochu_hint = ll_xiaochubi_options.findViewById(R.id.tv_xiaochu_hint);
        TextView tv_pre = ll_xiaochubi_options.findViewById(R.id.tv_pre);
        TextView tv_next = ll_xiaochubi_options.findViewById(R.id.tv_next);
        TextView tv_scorll = ll_xiaochubi_options.findViewById(R.id.tv_scorll);
        tv_pre.setVisibility(View.INVISIBLE);
        tv_scorll.setText("粗细");
        tv_xiaochu_hint.setText("手指涂抹瑕疵区域");
        tv_next.setText("下一步");
    }

    public void xiaoChuBiShowSecondStep() {
        if(xiaoChuBiParsener.getXiaoChuBiStep()==1){
            TextView tv_xiaochu_hint = ll_xiaochubi_options.findViewById(R.id.tv_xiaochu_hint);
            TextView tv_pre = ll_xiaochubi_options.findViewById(R.id.tv_pre);
            TextView tv_next = ll_xiaochubi_options.findViewById(R.id.tv_next);
            TextView tv_scorll = ll_xiaochubi_options.findViewById(R.id.tv_scorll);
            xiaoChuView.xiaoChuBiSetSectlectAreaStep(false,false);
            tv_pre.setVisibility(View.VISIBLE);
            tv_scorll.setText("模糊范围");
            tv_xiaochu_hint.setText("滑动手指，选择替换区域");
            tv_next.setText("完成");
        }else{
            closeOptionsView();
            cutView.saveXiaoChuBi();
            selectOption(0);
        }

    }

}
