package com.lyh.util;

import static android.graphics.Bitmap.createBitmap;

import static org.opencv.core.CvType.CV_8UC1;
import static org.opencv.core.CvType.CV_8UC3;
import static org.opencv.imgproc.Imgproc.COLOR_RGB2RGBA;
import static org.opencv.imgproc.Imgproc.COLOR_RGBA2BGRA;
import static org.opencv.imgproc.Imgproc.FILLED;
import static org.opencv.imgproc.Imgproc.FONT_HERSHEY_SIMPLEX;
import static org.opencv.imgproc.Imgproc.circle;
import static org.opencv.imgproc.Imgproc.cvtColor;
import static org.opencv.imgproc.Imgproc.getStructuringElement;
import static org.opencv.imgproc.Imgproc.getTextSize;
import static org.opencv.imgproc.Imgproc.line;
import static org.opencv.imgproc.Imgproc.putText;
import static org.opencv.imgproc.Imgproc.rectangle;
import static org.opencv.imgproc.Imgproc.resize;
import static org.opencv.imgproc.Imgproc.warpAffine;
import static org.opencv.photo.Photo.illuminationChange;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.util.Log;

import com.lyh.MediaLoader;
import com.lyh.brean.MySelectPath;
import com.test.R;
import com.test.RemapHelper;
import com.yanzhenjie.album.Album;
import com.yanzhenjie.album.AlbumConfig;

import org.opencv.android.Utils;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;
import org.opencv.photo.Photo;

import java.io.File;
import java.util.ArrayList;
import java.util.Locale;

public class OpenCVUtil {
    /**
     * @param isCapture 是否是拍照
     * @param context
     * @param filePath
     * @param Qpath
     * @param maxSize
     * @return
     */
    public static Bitmap getBitmap(boolean isCapture, Context context, String filePath, String Qpath, int maxSize) {
        Bitmap bitmap = null;
        if (isCapture) {
            bitmap = ImageDealUtil.decodeFile(new File(filePath), maxSize);
        } else {
            int pictureRotateDegree;
            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.P) {
                bitmap = ImageDealUtil.getBitmapFromUri(context, Uri.parse(Qpath), maxSize);
                int count = bitmap.getByteCount() / (bitmap.getWidth() * bitmap.getHeight());
                pictureRotateDegree = SystemUtil.readAndroidQPictureDegree(context, Qpath);
                if (count > 4) {
                    String tempFilePath = FileUtil.createFilePathCache(context, "pictures") + System.currentTimeMillis() + ".jpg";
                    FileUtil.saveBitmapToFile(tempFilePath, bitmap);
                    bitmap.recycle();
                    bitmap = null;
                    Mat mat = Imgcodecs.imread(tempFilePath);
                    bitmap = Bitmap.createBitmap(mat.cols(), mat.rows(), Bitmap.Config.ARGB_8888);
                    Mat temp = new Mat();
                    cvtColor(mat, temp, COLOR_RGBA2BGRA);
                    Utils.matToBitmap(temp, bitmap);
                    mat.release();
                    temp.release();
                }
            } else {
                bitmap = ImageDealUtil.decodeFile(new File(filePath), maxSize);
                int count = bitmap.getByteCount() / (bitmap.getWidth() * bitmap.getHeight());
                pictureRotateDegree = SystemUtil.readPictureDegree(filePath);
                if (count > 4) {//图片每个像素大于4个字节表示图片位数大于8
                    String tempFilePath = FileUtil.createFilePathCache(context, "pictures") + System.currentTimeMillis() + ".jpg";
                    FileUtil.saveBitmapToFile(tempFilePath, bitmap);
                    bitmap.recycle();
                    bitmap = null;
                    Mat mat = Imgcodecs.imread(tempFilePath);
                    bitmap = Bitmap.createBitmap(mat.cols(), mat.rows(), Bitmap.Config.ARGB_8888);
                    Mat temp = new Mat();
                    cvtColor(mat, temp, COLOR_RGBA2BGRA);
                    Utils.matToBitmap(temp, bitmap);
                    mat.release();
                    temp.release();
                }
            }
            if (pictureRotateDegree != 0) {
                Mat mat = new Mat();
                Utils.bitmapToMat(bitmap, mat);
                bitmap.recycle();
                Mat temp = new Mat();
                RemapHelper.setRotate(mat.nativeObj, temp.nativeObj, 360 - pictureRotateDegree);
                bitmap = createBitmap(temp.width(), temp.height(), Bitmap.Config.ARGB_8888);
                Utils.matToBitmap(temp, bitmap);
                mat.release();
                temp.release();
            }
        }
        return bitmap;
    }

    /**
     * @param context
     * @param filePath
     * @param Qpath
     * @param maxSize
     * @return
     */
    public static String reSaveFile(Context context, String filePath, String Qpath, int maxSize) {
        Bitmap bitmap = null;
        int pictureRotateDegree;
        int count;
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.P) {
            bitmap = ImageDealUtil.getBitmapFromUri(context, Uri.parse(Qpath), maxSize);
            count = bitmap.getByteCount() / (bitmap.getWidth() * bitmap.getHeight());
            pictureRotateDegree = SystemUtil.readAndroidQPictureDegree(context, Qpath);
        } else {
            bitmap = ImageDealUtil.decodeFile(new File(filePath), maxSize);
            count = bitmap.getByteCount() / (bitmap.getWidth() * bitmap.getHeight());
            pictureRotateDegree = SystemUtil.readPictureDegree(filePath);
        }
        if (count > 4) {
            String tempFilePath = FileUtil.createFilePathCache(context, "pictures") + System.currentTimeMillis() + ".jpg";
            FileUtil.saveBitmapToFile(tempFilePath, bitmap);
            bitmap.recycle();
            bitmap = null;
            Mat mat = Imgcodecs.imread(tempFilePath);
            bitmap = Bitmap.createBitmap(mat.cols(), mat.rows(), Bitmap.Config.ARGB_8888);
            Mat temp = new Mat();
            cvtColor(mat, temp, COLOR_RGBA2BGRA);
            Utils.matToBitmap(temp, bitmap);
            mat.release();
            temp.release();
            if (pictureRotateDegree != 0) {
                mat = new Mat();
                Utils.bitmapToMat(bitmap, mat);
                bitmap.recycle();
                temp = new Mat();
                RemapHelper.setRotate(mat.nativeObj, temp.nativeObj, 360 - pictureRotateDegree);
                bitmap = createBitmap(temp.width(), temp.height(), Bitmap.Config.ARGB_8888);
                Utils.matToBitmap(temp, bitmap);
                mat.release();
                temp.release();
                String tempFilePath2 = FileUtil.createFilePathCache(context, "pictures") + System.currentTimeMillis() + ".jpg";
                FileUtil.saveBitmapToFile(tempFilePath2, bitmap);
                bitmap.recycle();
                return tempFilePath2;
            } else {
                bitmap.recycle();
                return tempFilePath;
            }
        }
        return null;
    }

    /**
     * 亮度值0-255
     *
     * @param light 亮度变化大小
     */
    public static void chengePictureLight(Bitmap bitmap, Bitmap destBitmap, int light) {
//        int width = bitmap.getWidth();
//        int height = bitmap.getHeight();
//        Mat srcMat = new Mat(width, height, CvType.CV_8UC1);
//        Utils.bitmapToMat(bitmap, srcMat);
//        Mat destMat = new Mat();
//        Imgproc.cvtColor(srcMat, destMat, Imgproc.COLOR_BGRA2RGB, 4);
//        Utils.matToBitmap(destMat, bitmap);

        Mat srcMat = new Mat();
        Utils.bitmapToMat(bitmap, srcMat);
        /**
         * m  目标矩阵。如果m的大小与原矩阵不一样，或者数据类型与参数不匹配，那么在函数convertTo内部会先给m重新分配空间。
         * rtype 指定从原矩阵进行转换后的数据类型，即目标矩阵m的数据类型。当然，矩阵m的通道数应该与原矩阵一样的。如果rtype是负数，那么m矩阵的数据类型应该与原矩阵一样。
         * alpha 缩放因子。默认值是1。即把原矩阵中的每一个元素都乘以alpha。可以控制图片对比度 取值范围：0-1
         * beta 增量。默认值是0。即把原矩阵中的每一个元素都乘以alpha，再加上beta。可以控制图片亮度 取值 -255到255；
         */
        srcMat.convertTo(srcMat, -1, 1, light);
        Utils.matToBitmap(srcMat, destBitmap);
        srcMat.release();
        srcMat = null;
    }



    /**
     * 均值滤波
     */
    public static void blur(Bitmap bitmap, Bitmap destBitmap, int ksize) {
        Mat srcMat = new Mat();
        Utils.bitmapToMat(bitmap, srcMat);
        Mat destMat = new Mat();
        Imgproc.blur(srcMat, destMat, new Size(9, 9), new Point(-1, -1), Core.BORDER_DEFAULT);
        Utils.matToBitmap(srcMat, destBitmap);
        srcMat.release();
        srcMat = null;
        destMat.release();
        destMat = null;
    }

    /**
     * 高斯滤波 图片去噪
     */
    public static void gaussianBlur(Bitmap bitmap, Bitmap destBitmap, int ksize) {
        Mat srcMat = new Mat();
        Utils.bitmapToMat(bitmap, srcMat);
        Mat destMat = new Mat();
        Imgproc.GaussianBlur(srcMat, destMat, new Size(9, 9), 0, 0, Core.BORDER_DEFAULT);
        Utils.matToBitmap(srcMat, destBitmap);
        srcMat.release();
        srcMat = null;
        destMat.release();
        destMat = null;
    }

    /**
     * 中值滤波 图片去噪
     */
    public static void medianBlur(Bitmap bitmap, Bitmap destBitmap, int ksize) {
        Mat srcMat = new Mat();
        Utils.bitmapToMat(bitmap, srcMat);
        Mat destMat = new Mat();
        Imgproc.medianBlur(srcMat, destMat, 7);
        Utils.matToBitmap(srcMat, destBitmap);
        srcMat.release();
        srcMat = null;
        destMat.release();
        destMat = null;
    }
    /**
     * 图片修复
     */
    /**
     * 图片修复，去除噪点，水印等 整张图修复
     */
    public static void inpaintBlurAllPicture1(Bitmap bitmap, Bitmap destBitmap,int currentPosition) {

        Mat srcMat = new Mat();
        Mat matRoi =new Mat();//圆形区域
        Mat originalMat = new Mat();
        Mat gray = new Mat();
        Mat mask = new Mat();
        Mat destMat = new Mat();
        Mat temp = new Mat();
        Mat kernel;
        Utils.bitmapToMat(bitmap, srcMat);

        //1.转换颜色格式
        cvtColor(srcMat, originalMat, Imgproc.COLOR_RGBA2RGB);
        //2.得到灰度图
        cvtColor(originalMat, gray, Imgproc.COLOR_RGB2GRAY);//得到灰度图
        //3.通过阈值处理生成Mask
//        Imgproc.threshold(gray, mask, 210, 255, Imgproc.THRESH_BINARY);
        Imgproc.threshold(matRoi, mask, 0, 255, Imgproc.THRESH_OTSU);//自适应算法
        kernel = Imgproc.getStructuringElement(Imgproc.MORPH_ELLIPSE, new Size(3, 3));//函数会返回指定形状和尺寸的结构元素。
        Imgproc.dilate(mask, mask, kernel);//dilate()膨胀函数//erode()腐蚀函数
        Photo.inpaint(originalMat,mask,destMat,3,Photo.INPAINT_TELEA);
        cvtColor(destMat,temp, COLOR_RGB2RGBA);
        Utils.matToBitmap(temp, destBitmap);
        releaseMat(srcMat);
        releaseMat(originalMat);
        releaseMat(mask);
        releaseMat(destMat);
        releaseMat(temp);
        releaseMat(kernel);
    }

    /**
     * 图片修复
     */
    /**
     * 图片修复，去除噪点，水印等 整张图修复
     */
    public static void inpaintBlurAllPicture2(Bitmap bitmap, Bitmap destBitmap, ArrayList<MySelectPath> xiaoChuCircleLists, int currentPosition, int xiaochuWidth) {
        int offsetX=0;
        int offsetY=-100;
        Mat srcMat = new Mat();
        Mat originalMat = new Mat();
        Mat gray = new Mat();
        Mat mask = new Mat();
        Mat destMat = new Mat();
        Mat temp = new Mat();
        Mat kernel=null;
        Utils.bitmapToMat(bitmap, srcMat);
        //替换区域
        Mat matRoiMask=new Mat(srcMat.size(),CV_8UC1);
        rectangle(matRoiMask,new Point(0,0),new Point(srcMat.width(),srcMat.height()),new Scalar(0),FILLED);
        for(int i=0;i<xiaoChuCircleLists.size();i++){
            Point last_point = null;
            MySelectPath path = xiaoChuCircleLists.get(i);
            if(path.getPathPoints().size()==1){
                circle(matRoiMask,new Point(path.getPathPoints().get(0).getPointXiaoChu().x,path.getPathPoints().get(0).getPointXiaoChu().y),xiaochuWidth,new Scalar(255));
            }else{
                for(int j=0;j<path.getPathPoints().size();j++){
                    if(j ==0){
                        last_point = new Point(path.getPathPoints().get(j).getPointXiaoChu().x+offsetX,path.getPathPoints().get(j).getPointXiaoChu().y+offsetY);
                    }else{
                        Point current_point =new Point(path.getPathPoints().get(j).getPointXiaoChu().x+offsetX,path.getPathPoints().get(j).getPointXiaoChu().y+offsetY);
                        line(matRoiMask,last_point,current_point,new Scalar(255),90);
                        last_point = current_point;
                    }
                }
            }
        }




        //处理掩码渐变
        int bias = 0;
        int explore=2;
        Mat area = new Mat();
        Mat area2 = new Mat();
        srcMat.copyTo(area,matRoiMask);
////        //1.转换颜色格式
//        Imgproc.cvtColor(area, originalMat, Imgproc.COLOR_RGBA2RGB);
////        //得到灰度图
//        Imgproc.cvtColor(area, gray, Imgproc.COLOR_RGB2GRAY);//得到灰度图
//        Imgproc.threshold(gray, gray, 240, 255, Imgproc.THRESH_BINARY);
//        kernel = Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(3, 3));//函数会返回指定形状和尺寸的结构元素。
//        Imgproc.dilate(area, area, kernel,new Point(-1d,-1d),explore);//dilate()膨胀函数//erode()腐蚀函数
//        Imgproc.blur(area,area,new Size(bias+3*explore,bias+3*explore));
        Imgproc.GaussianBlur(area, area2, new Size(9, 9), 0, 0, Core.BORDER_DEFAULT);


//        area.convertTo(area,CV_32FC3);

//        matRoiMask = (matRoiMask/255)/2;
//        Mat area = new Mat();
//        srcMat.copyTo(area,matRoiMask);

        srcMat.copyTo(destMat);
        RemapHelper.setOverlayImage(destMat.nativeObj,area2.nativeObj,-offsetX,-offsetY);
        cvtColor(destMat,temp, COLOR_RGB2RGBA);
        Utils.matToBitmap(temp, destBitmap);
        releaseMat(srcMat);
        releaseMat(originalMat);
        releaseMat(mask);
        releaseMat(destMat);
        releaseMat(temp);
        releaseMat(kernel);

    }

    /**
     * 图片修复
     */
    /**
     * 图片修复，去除噪点，水印等 整张图修复
     */
    public static void inpaintBlurAllPicture(Bitmap bitmap, Bitmap destBitmap, ArrayList<MySelectPath> paths, int current, int offsetX, int offsetY, int xiaoChuWidth, int explore) {
        Mat srcMat = new Mat();
        Mat mask;
        Mat destMat = new Mat();
        Mat temp = new Mat();
        Utils.bitmapToMat(bitmap, srcMat);
        mask=new Mat(srcMat.size(),CV_8UC1);
        rectangle(mask,new Point(0,0),new Point(srcMat.width(),srcMat.height()),new Scalar(0),FILLED);
        Log.e("------------------>","xiaoChuWidth = "+xiaoChuWidth);
        Log.e("------------------>","explore = "+explore);

        for(int i=0;i<paths.size();i++){
            if(i>current){
                break;
            }
            Point last_point = null;
            for(int j=0;j<paths.get(i).getPathPoints().size();j++){
                int R = xiaoChuWidth-explore;
                if(j ==0){
                    last_point = new Point(paths.get(i).getPathPoints().get(j).getPointXiaoChu().x+offsetX,paths.get(i).getPathPoints().get(j).getPointXiaoChu().y+offsetY);
                }else{
                    Point current_point =new Point(paths.get(i).getPathPoints().get(j).getPointXiaoChu().x+offsetX,paths.get(i).getPathPoints().get(j).getPointXiaoChu().y+offsetY);
                    line(mask,last_point,current_point,new Scalar(255),R);
                    last_point = current_point;
                }
            }
        }
        srcMat.copyTo(destMat);
        if(explore>0){
            Imgproc.blur(mask, mask,new Size(explore,explore),new Point(-1,-1),Core.BORDER_DEFAULT);
        }
        Mat area = new Mat();
        srcMat.copyTo(area,mask);
        RemapHelper.setOverlayImage(destMat.nativeObj,area.nativeObj,-offsetX,-offsetY);
        cvtColor(destMat,temp, COLOR_RGB2RGBA);
        Utils.matToBitmap(temp, destBitmap);
        releaseMat(srcMat);
        releaseMat(mask);
        releaseMat(destMat);
        releaseMat(temp);

    }
    /**
     * 图片修复  整图修复
     * @param ksize  必须是大于等于1 的奇数
     */
    public static void inpaintBlurWay1(Bitmap bitmap,Bitmap destBitmap,int ksize){
        Mat desc = new Mat(bitmap.getHeight(),bitmap.getWidth(), CV_8UC3);
        //转化为mat对象
        Utils.bitmapToMat(bitmap, desc,true);
        //转化为3通道图像
        Mat src = new Mat();
        cvtColor(desc,src,Imgproc.COLOR_RGBA2RGB);
        //灰度图像
        Mat srcGray = new Mat();
        cvtColor(src, srcGray, Imgproc.COLOR_RGB2GRAY);
        //中值滤波去燥
        Imgproc.medianBlur(srcGray,srcGray,3);
        //获取污点的二值化图像
        Mat srcThresh = new Mat();
        Imgproc.threshold(srcGray,srcThresh,242,255,Imgproc.THRESH_BINARY);

        //修复图像
        Mat inpaintResult = new Mat();
        Photo.inpaint(src,srcThresh,inpaintResult,ksize,Photo.INPAINT_TELEA);
        Utils.matToBitmap(inpaintResult, destBitmap);
    }

    /**
     * 图片修复，去除噪点，水印等
     */
    public static void inpaintBlur(Bitmap bitmap, Bitmap destBitmap) {
        Mat srcMat = new Mat();
        Mat originalMat = new Mat();
        Mat matRoi;//修复的区域
        Mat gray = new Mat();
        Mat mask = new Mat();
        Mat destMat = new Mat();
        Mat temp = new Mat();
        Mat kernel;
        Utils.bitmapToMat(bitmap, srcMat);
        cvtColor(srcMat, originalMat, Imgproc.COLOR_BGR2RGB);

        //得到操作的区域
        Rect roi = new Rect(new Point(0, 0), new Point(200, 200));
        matRoi = originalMat.submat(roi);
        //2.得到灰度图
        cvtColor(matRoi, gray, Imgproc.COLOR_RGB2GRAY);//得到灰度图
        //3.通过阈值处理生成Mask
        mask = new Mat(matRoi.size(), CV_8UC1, new Scalar(0));

        Imgproc.threshold(gray, mask, 254.0, 255.0, Imgproc.THRESH_BINARY);
        kernel = Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(3, 3));//函数会返回指定形状和尺寸的结构元素。
        Imgproc.dilate(mask, mask, kernel);//dilate()膨胀函数//erode()腐蚀函数
        Photo.inpaint(srcMat,mask,destMat,5.0,Photo.INPAINT_TELEA);
        cvtColor(destMat,temp,Imgproc.COLOR_RGBA2BGRA);
        Log.e("-------->","srcMat.width()="+srcMat.width()+";srcMat.height()="+srcMat.height());
        Log.e("-------->","temp.width()="+temp.width()+"；temp.height()="+temp.height());
        Utils.matToBitmap(temp, destBitmap);
        Log.e("-------->","destBitmap=null?"+(destBitmap==null?true:false));

        releaseMat(srcMat);
        releaseMat(originalMat);
        releaseMat(mask);
        releaseMat(destMat);
        releaseMat(temp);
        releaseMat(kernel);
    }
    /**
     * 局部调整亮度
     * light -1 到 1
     */
    public static void regulateParterLightless(Bitmap bitmap,Bitmap destBitmap,int x,int y,int R,float light){
        Mat srcMat = new Mat();
        Utils.bitmapToMat(bitmap, srcMat);
        cvtColor(srcMat,srcMat,Imgproc.COLOR_BGRA2RGB);
        Mat mask=new Mat(srcMat.size(),CV_8UC1);
        circle(mask,new Point(x,y),R,new Scalar(255),-1);
        Mat temp = new Mat();
        illuminationChange(srcMat, mask, temp, 1, light);
        cvtColor(temp,temp,Imgproc.COLOR_RGB2BGRA);
        Utils.matToBitmap(temp, destBitmap);
        releaseMat(srcMat);
        releaseMat(mask);
        releaseMat(temp);
    }
    public static void setMyYingYangSe(Bitmap bitmap, Bitmap destBitmap, ArrayList<MySelectPath> paths, float light, int current, int xiaoChuWidth, int explore){
        Mat srcMat = new Mat();
        Utils.bitmapToMat(bitmap, srcMat);
        Mat imgShow = new Mat(srcMat.size(),srcMat.type());
        Mat mask=new Mat(srcMat.size(),CV_8UC3);
        rectangle(mask,new Point(0,0),new Point(srcMat.width(),srcMat.height()),new Scalar(0,0,0),FILLED);
        for(int i=0;i<paths.size();i++){
            if(i>current){
                break;
            }
            Point last_point = null;
            for(int j=0;j<paths.get(i).getPathPoints().size();j++){
                int R = xiaoChuWidth-explore;
                if(j ==0){
                    last_point = new Point(paths.get(i).getPathPoints().get(j).getPointXiaoChu().x,paths.get(i).getPathPoints().get(j).getPointXiaoChu().y);
                }else{
                    Point current_point =new Point(paths.get(i).getPathPoints().get(j).getPointXiaoChu().x,paths.get(i).getPathPoints().get(j).getPointXiaoChu().y);
                    line(mask,last_point,current_point,new Scalar(255,255,255),R);
                    last_point = current_point;
                }
            }
        }
        RemapHelper.setMyYingYangSe(srcMat.nativeObj,mask.nativeObj,imgShow.nativeObj,light,explore);
    }
    public static void releaseMat(Mat mat){
        if(mat!=null){
            mat.release();
            mat = null;
        }
    }
    public static void initAlbum(Context context){
        Album.initialize(AlbumConfig.newBuilder(context)
                .setAlbumLoader(new MediaLoader())
                .setLocale(Locale.getDefault())
                .build()
        );
    }

    /**
     *
     * @param context
     * @param bitmap
     * @param scale 标尺比例   400/1000   左像素，右毫米   400像素表示1000mm
     */
    private static void saveBiaoChiPicture(Context context,Bitmap bitmap,float scale){
        bitmap = ImageDealUtil.readBitMap(context, R.mipmap.whiteprint,false);
        int imageWidth = bitmap.getWidth();
        int imageHeight = bitmap.getHeight();
    }
    /**
     * @param type=1,图片不需要拉伸 ;type=2 图片需要拉伸
     * @param context
     * @param bitmap
     * @param scale 标尺比例   400/1000   左像素，右毫米   400像素表示1000mm
     */
    public static void saveBiaoChiPictureNoGraduation(int type,Context context,Bitmap bitmap,int biaoChiWidth,int biaoChiHeight,float scale,String filePath){
        bitmap = ImageDealUtil.readBitMap(context, R.mipmap.whiteprint,false);
        Mat srcMat = new Mat();
        Mat dstMat=null;
        Mat temp=null;
        Utils.bitmapToMat(bitmap, srcMat);
        if(type==1){
            Imgcodecs.imwrite(filePath,srcMat);
        }else{
            dstMat = new Mat((int)(scale*biaoChiWidth),(int)(scale*biaoChiHeight),srcMat.type());
            resize(srcMat,dstMat,dstMat.size());
            //保存标尺原图
            temp = new Mat();
            cvtColor(dstMat, temp, COLOR_RGBA2BGRA);
            Imgcodecs.imwrite(filePath,temp);
        }
        OpenCVUtil.releaseMat(srcMat);
        OpenCVUtil.releaseMat(dstMat);
        OpenCVUtil.releaseMat(temp);
    }
    /**
     * @param type=1,图片不需要拉伸 ;type=2 图片需要拉伸
     * @param context
     * @param bitmap
     * @param scale 标尺比例   400/1000   左像素，右毫米   400像素表示1000mm
     */
    public static void saveBiaoChiPictureHaveGraduation(int type,Context context,Bitmap bitmap,int biaoChiWidth,int biaoChiHeight,float scale,String filePath){
        bitmap = ImageDealUtil.readBitMap(context, R.mipmap.whiteprint,false);
        Mat srcMat = new Mat();
        Mat dstMat=null;
        Mat temp=null;
        Utils.bitmapToMat(bitmap, srcMat);
        if(type==1){
            //保存标尺原图
            temp = new Mat();
            cvtColor(srcMat, temp, COLOR_RGBA2BGRA);
            drawGraduation(temp,biaoChiWidth,biaoChiHeight,filePath);
        }else{
            dstMat = new Mat((int)(scale*biaoChiWidth),(int)(scale*biaoChiHeight),srcMat.type());
            resize(srcMat,dstMat,dstMat.size());
            //保存标尺原图
            temp = new Mat();
            cvtColor(dstMat, temp, COLOR_RGBA2BGRA);
            drawGraduation(temp,biaoChiWidth,biaoChiHeight,filePath);
        }
        OpenCVUtil.releaseMat(srcMat);
        OpenCVUtil.releaseMat(dstMat);
        OpenCVUtil.releaseMat(temp);
    }
    public static void drawGraduation(Mat srcMat,int biaoChiWidth,int biaoChiHeight,String filePath){
        Mat temp=null;
        int width = srcMat.width();//图片宽度
        int height = srcMat.height();//图片高度
        int biaochiWidth = 40;
        int widthAfterAddBiaoChi = width+biaochiWidth;
        int heightAfterAddBiaoChi = height +biaochiWidth;

        Mat dstMat = new Mat(widthAfterAddBiaoChi,heightAfterAddBiaoChi,srcMat.type());
        rectangle(dstMat,new Rect(0,0,widthAfterAddBiaoChi,heightAfterAddBiaoChi),new Scalar(64,64,64,255), FILLED);
        rectangle(dstMat,new Rect(1,1,widthAfterAddBiaoChi-2,heightAfterAddBiaoChi-2),new Scalar(255,255,255,255), FILLED);
        RemapHelper.setOverlayImage(dstMat.nativeObj,srcMat.nativeObj,biaochiWidth,0);
        int graduationMinWidth = 2;
        int graduationMiddleWidth = 4;
        int graduationMaxWidth = 6;

        int graduationMinSpace = 4;//1厘米
        int graduationMiddleSpace = 40;//1分米
        int graduationMaxSpace = 400;//1米

        double textSize = 0.3d;
        //绘制垂直方向刻度
        int graduationVerticalMinNumber = dstMat.height()/(2*graduationMinSpace);//小刻度数量
        int graduationVerticalMiddleNumber = dstMat.height()/graduationMiddleSpace;//中刻度数量
        int graduationVerticalMaxNumber = dstMat.height()/graduationMaxSpace;//大刻度数量
        //绘制最小刻度  2厘米
        for(int i=0;i<graduationVerticalMinNumber;i++){
            line(dstMat,new Point(biaochiWidth-graduationMinWidth,srcMat.height()-(2*graduationMinSpace)*i),new Point(biaochiWidth,srcMat.height()-(2*graduationMinSpace)*i),new Scalar(64,64,64,255));
        }
        //绘制中刻度  1分米
        for(int i=0;i<graduationVerticalMiddleNumber;i++){
            line(dstMat,new Point(biaochiWidth-graduationMiddleWidth,srcMat.height()-graduationMiddleSpace*i),new Point(biaochiWidth,srcMat.height()-graduationMiddleSpace*i),new Scalar(64,64,64,255));
        }
        //绘制大刻度  1米
        for(int i=0;i<graduationVerticalMaxNumber;i++){
            line(dstMat,new Point(biaochiWidth-graduationMaxWidth,srcMat.height()-graduationMaxSpace*i),new Point(biaochiWidth,srcMat.height()-graduationMaxSpace*i),new Scalar(13,13,220,255));
        }
        //绘制水平方向刻度
        int graduationHorizontalMinNumber = dstMat.width()/(2*graduationMinSpace);//小刻度数量
        int graduationHorizontalMiddleNumber = dstMat.width()/graduationMiddleSpace;//中刻度数量
        int graduationHorizontalMaxNumber = dstMat.width()/graduationMaxSpace;//大刻度数量
        //绘制最小刻度  2厘米
        for(int i=0;i<graduationHorizontalMinNumber;i++){
            line(dstMat,new Point(biaochiWidth+graduationMinSpace*2*i,srcMat.height()),new Point(biaochiWidth+graduationMinSpace*2*i,srcMat.height()+graduationMinWidth),new Scalar(64,64,64,255));
        }
        //绘制中刻度  1分米
        for(int i=0;i<graduationHorizontalMiddleNumber;i++){
            line(dstMat,new Point(biaochiWidth+graduationMiddleSpace*i,srcMat.height()),new Point(biaochiWidth+graduationMiddleSpace*i,srcMat.height()+graduationMiddleWidth),new Scalar(64,64,64,255));
        }
        //绘制大刻度  1米
        for(int i=0;i<graduationHorizontalMaxNumber;i++){
            line(dstMat,new Point(biaochiWidth+graduationMaxSpace*i,srcMat.height()),new Point(biaochiWidth+graduationMaxSpace*i,srcMat.height()+graduationMaxWidth),new Scalar(13,13,220,255));
        }

        //绘制垂直方向文本
        for(int i=0;i<=graduationVerticalMiddleNumber;i++){
            if(i%10!=0){
                Size size = getTextSize(i*100+"",FONT_HERSHEY_SIMPLEX,textSize,1,null);
                putText(dstMat,i*100+"",new Point(biaochiWidth-6-size.width,srcMat.height()-i*graduationMiddleSpace+size.height/2),FONT_HERSHEY_SIMPLEX,textSize,new Scalar(64,64,64,255));
            }
        }
        for(int i=0;i<=graduationVerticalMaxNumber;i++){
            Size size = getTextSize(i*1000+"",FONT_HERSHEY_SIMPLEX,textSize,1,null);
            putText(dstMat,i*1000+"",new Point(biaochiWidth-6-size.width,srcMat.height()-i*graduationMaxSpace+size.height/2),FONT_HERSHEY_SIMPLEX,textSize,new Scalar(13,13,220,255));
        }

        //绘制水平方向文本
        for(int i=0;i<=graduationVerticalMiddleNumber;i++){
            if(i%10!=0){
                Size size = getTextSize(i*100+"",FONT_HERSHEY_SIMPLEX,textSize,1,null);
                putText(dstMat,i*100+"",new Point(biaochiWidth+i*graduationMiddleSpace-size.width/2,srcMat.height()+8+size.height),FONT_HERSHEY_SIMPLEX,textSize,new Scalar(64,64,64,255));
            }
        }
        Log.e("-------------->","maxHorizontalNumber = "+graduationVerticalMaxNumber);
        for(int i=0;i<=graduationVerticalMaxNumber;i++){
            Size size = getTextSize(i*1000+"",FONT_HERSHEY_SIMPLEX,textSize,1,null);
            putText(dstMat,i*1000+"",new Point(biaochiWidth+i*graduationMaxSpace-size.width/2,srcMat.height()+8+size.height),FONT_HERSHEY_SIMPLEX,textSize,new Scalar(13,13,220,255));
        }
        //绘制单位 宽高
        Size size = getTextSize(+biaoChiWidth+"mm x "+biaoChiHeight+"mm",FONT_HERSHEY_SIMPLEX,textSize,1,null);
        putText(dstMat,+biaoChiWidth+"mm x "+biaoChiHeight+"mm",new Point(biaochiWidth,srcMat.height()+6+size.height+size.height+10),FONT_HERSHEY_SIMPLEX,textSize,new Scalar(13,13,220,255));
//        temp = new Mat();
//        Imgproc.cvtColor(dstMat, temp, COLOR_RGBA2BGRA);
        Imgcodecs.imwrite(filePath,dstMat);
        OpenCVUtil.releaseMat(srcMat);
        OpenCVUtil.releaseMat(dstMat);
        OpenCVUtil.releaseMat(temp);
    }
    /**
     * @param context
     * @param bitmap
     * @param scale 标尺比例   400/1000   左像素，右毫米   400像素表示1000mm
     */
    public static void getBiaochiBitmap(Context context,Bitmap bitmap,Bitmap bitmapBiaochi,int biaoChiWidth,int biaoChiHeight,float scale){
        Mat srcMat = new Mat();
        Mat dstMat=null;
        Mat temp=null;
        Utils.bitmapToMat(bitmap, srcMat);
        dstMat = new Mat((int)(scale*biaoChiWidth),(int)(scale*biaoChiHeight),srcMat.type());
        resize(srcMat,dstMat,dstMat.size());
        //保存标尺原图
        temp = new Mat();
        cvtColor(dstMat, temp, COLOR_RGBA2BGRA);
        Utils.matToBitmap(temp,bitmapBiaochi);
        OpenCVUtil.releaseMat(srcMat);
        OpenCVUtil.releaseMat(dstMat);
        OpenCVUtil.releaseMat(temp);
    }
}
