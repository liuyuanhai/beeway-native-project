package com.lyh.mvp.xiaochubi;

import android.graphics.Bitmap;

public interface XiaoChuView {
    public void xiaoChubiSetPre();//消除笔上一步
    public void xiaoChuBiSetNext();//消除笔下一步
    public void invalidateView();
    public void toastInfo(String content);//提示
    public void xiaoChuBiSetSectlectAreaStep(boolean isCanPre,boolean isCanNext);
}
