package com.lyh.mvp.xiaochubi;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.util.Log;
import android.view.MotionEvent;
import android.widget.Toast;

import com.lyh.brean.MySelectPath;
import com.lyh.brean.MyPoint;
import com.lyh.util.OpenCVUtil;
import com.lyh.util.common;
import com.test.R;

import org.opencv.core.Point;

import java.util.ArrayList;

public class XiaoChuParsener implements XiaoChuModule.ModuleInterface{
    private Context context;
    private Bitmap bitmapInit;

    private XiaoChuView xiaoChuBiView;
    private XiaoChuModule xiaoChuBiModule = new XiaoChuModule(this);

    private Paint paintForXiaoChuWidth;//绘制消除比粗细的画笔
    private Paint paintForXiaoChuArea;//绘制消除区域的画笔
    private Paint paintForXiaoChuReplaceArea;//绘制替换区域的画笔
    /*-------------------------*/
    private int xiaoChuBiWidth;//消除笔粗细 //画笔宽度默认24dp 在24dp到44dp之间
    private int xiaoChuBiBorderWidth;//消除区域边缘模糊范围 0到20dp
    /*-------------------------*/
    private Bitmap bitmapXiaoChu;//消除后的图片
    /*-------------------------*/
    private int xiaoChuBiStep=1;// 1选择区域 ，2拖动区域
    /*-------------------------*/
    private ArrayList<MySelectPath> paths=new ArrayList<>();//消除区域路径合集
    private int pathEndPosition=-1;//路径结束位置
    /*-------------------------*/
    private MySelectPath currentPath;
    /*-------------------------*/
    private float lastX;//手指按下时的坐标 选择替换区域
    private float lastY;//手指按下时的坐标 选择替换区域
    private float xiaochuX;//替换区域相对于原区域的偏移量
    private float xiaochuY;//替换区域相对于原区域的偏移量
    private boolean isShowXiaoChuBiWidthCircle;//
    private float scale;//图像显示尺寸与图像原始像素的比例

    public Bitmap getBitmapXiaoChu() {
        return bitmapXiaoChu;
    }

    public void setBitmapXiaoChu(Bitmap bitmapXiaoChu) {
        this.bitmapXiaoChu = bitmapXiaoChu;
    }

    public int getXiaoChuBiStep() {
        return xiaoChuBiStep;
    }

    public void setXiaoChuBiStep(int xiaoChuBiStep) {
        this.xiaoChuBiStep = xiaoChuBiStep;
    }

    public XiaoChuParsener(XiaoChuView xiaoChuBiView){
        this.xiaoChuBiView = xiaoChuBiView;
    }
    //初始化消除笔
    public void initXiaoChuBi(Context context){
        this.context =context;
        //初始画画笔 1.消除笔粗细的画笔
        paintForXiaoChuWidth = new Paint();
        paintForXiaoChuWidth.setAntiAlias(true);
        paintForXiaoChuWidth.setStyle(Paint.Style.FILL);
        paintForXiaoChuWidth.setColor(context.getResources().getColor(R.color.color30FF0000));
        //初始画画笔 2.消除区域画笔
        paintForXiaoChuArea = new Paint();
        paintForXiaoChuArea.setAntiAlias(true);
        paintForXiaoChuArea.setStyle(Paint.Style.STROKE);
        paintForXiaoChuArea.setColor(context.getResources().getColor(R.color.color30FF0000));
        paintForXiaoChuArea.setStrokeCap(Paint.Cap.ROUND);

        //初始画画笔 2.绘制替换区域的画笔
        paintForXiaoChuReplaceArea = new Paint();
        paintForXiaoChuReplaceArea.setAntiAlias(true);
        paintForXiaoChuReplaceArea.setStyle(Paint.Style.STROKE);
        paintForXiaoChuReplaceArea.setColor(context.getResources().getColor(R.color.color30FF0000));
        paintForXiaoChuReplaceArea.setStrokeCap(Paint.Cap.ROUND);
    }
    //重置消除笔信息
    public void resetData(){
        xiaoChuBiWidth = common.Dp2Px(context,34);//画笔宽度默认24dp 在24dp到44dp之间
        xiaoChuBiBorderWidth = 0;
        paintForXiaoChuWidth.setStrokeWidth(xiaoChuBiWidth);//默认画笔大小
        paintForXiaoChuArea.setStrokeWidth(xiaoChuBiWidth);//默认画笔大小
        paintForXiaoChuReplaceArea.setStrokeWidth(xiaoChuBiWidth);//默认画笔大小
        xiaoChuBiStep = 1;
        xiaochuX=0;
        xiaochuY=0;
        paths.clear();
        currentPath = null;
    }
    public void setData(Bitmap bitmapInit,float scale) {
        this.bitmapInit=bitmapInit;
        this.scale = scale;
    }
    /**
     * 设置消除笔粗细
     */
    public void xiaoChuBiSetPaintWidth(int width, boolean isMove){
        if(isMove){
            xiaoChuBiWidth = width;
            paintForXiaoChuArea.setStrokeWidth(xiaoChuBiWidth);//画笔大小
            paintForXiaoChuReplaceArea.setStrokeWidth(xiaoChuBiWidth);
            isShowXiaoChuBiWidthCircle = true;
        }else{
            isShowXiaoChuBiWidthCircle = false;
        }
        xiaoChuBiView.invalidateView();
    }
    /**
     * 设置消除笔，消除范围
     */
    public void setXiaoChuBiReplReplaceScope(int width,boolean isMove){
        if(isMove){
            xiaoChuBiBorderWidth = width;
            setXiaochuBi();
        }
    }

    /**
     * @param type 涂抹选区,1.上一步,2.下一步
     */
    public void xiaoChuBiSelectArea(int type,Bitmap bitmapInit){
        if(type==1){//上一步
            if(pathEndPosition==-1){
                return;
            }else{
                pathEndPosition = pathEndPosition-1;
                if(pathEndPosition==-1){
                    xiaoChuBiView.xiaoChuBiSetSectlectAreaStep(false,true);
                }else{
                    xiaoChuBiView.xiaoChuBiSetSectlectAreaStep(true,true);
                }
                xiaoChuBiView.invalidateView();
            }
        }else{//下一步
            if(pathEndPosition==paths.size()-1){
                return;
            }else{
                pathEndPosition = pathEndPosition+1;
                if(pathEndPosition==paths.size()-1){
                    xiaoChuBiView.xiaoChuBiSetSectlectAreaStep(true,false);
                }else{
                    xiaoChuBiView.xiaoChuBiSetSectlectAreaStep(true,true);
                }
                xiaoChuBiView.invalidateView();
            }
        }
    }

    //上一步
    public void setPre(){
        xiaoChuBiStep =1;
        if(pathEndPosition==paths.size()-1) {
            xiaochuX=0;
            xiaochuY=0;
            if(bitmapXiaoChu!=null){
                bitmapXiaoChu.recycle();
                bitmapXiaoChu =null;
            }
            xiaoChuBiView.xiaoChuBiSetSectlectAreaStep(true,false);
            xiaoChuBiView.invalidateView();
        }else {
            xiaoChuBiView.xiaoChuBiSetSectlectAreaStep(true,true);
            xiaoChuBiView.invalidateView();
        }
    }
    //下一步
    public void setNext(){
        if(xiaoChuBiStep==1){
            if(pathEndPosition==-1){
                xiaoChuBiView.toastInfo("请先涂抹瑕疵区域");
            }else{
                xiaoChuBiView.xiaoChuBiSetNext();
                xiaoChuBiStep =2;
            }
        }else{
            xiaoChuBiView.xiaoChuBiSetNext();
            resetData();

        }
    }

    public void xiaoChubiDraw(Canvas canvas,int width ,int height){
        if(isShowXiaoChuBiWidthCircle){
            canvas.drawCircle(width/2,height/2,xiaoChuBiWidth/2,paintForXiaoChuWidth);
        }
        if(xiaoChuBiStep==1){
            drawXiaochuBiShadows(canvas);
        }else{
            drawXiaochuBiShadowsOffset(canvas);//替换区域
        }
    }
    //绘制消除区域
    public void drawXiaochuBiShadows(Canvas canvas){
        if(paths.size()>0){
            Path path = new Path();
            for(int i=0;i<paths.size();i++){
                if(i>pathEndPosition){
                    break;
                }
                for(int j = 0;j<paths.get(i).getPathPoints().size();j++){
                    if(j==0) {
                        path.moveTo((float)paths.get(i).getPathPoints().get(j).getPointShow().x,(float)paths.get(i).getPathPoints().get(j).getPointShow().y);
                    }else {
                        path.lineTo((float)paths.get(i).getPathPoints().get(j).getPointShow().x,(float)paths.get(i).getPathPoints().get(j).getPointShow().y);
                    }
                }
            }
            canvas.drawPath(path,paintForXiaoChuArea);
        }
    }
    //绘制替换区域
    public void drawXiaochuBiShadowsOffset(Canvas canvas){
        if(paths.size()>0){
            Path path = new Path();
            for(int i=0;i<paths.size();i++){
                if(i>pathEndPosition){
                    break;
                }
                for(int j = 0;j<paths.get(i).getPathPoints().size();j++){
                    if(j==0) {
                        path.moveTo((float)paths.get(i).getPathPoints().get(j).getPointShow().x+xiaochuX,(float)paths.get(i).getPathPoints().get(j).getPointShow().y+xiaochuY);
                    }else {
                        path.lineTo((float)paths.get(i).getPathPoints().get(j).getPointShow().x+xiaochuX,(float)paths.get(i).getPathPoints().get(j).getPointShow().y+xiaochuY);
                    }
                }
            }
            canvas.drawPath(path,paintForXiaoChuReplaceArea);
        }
    }
    //选择消除区域事件
    public boolean onXiaoChuBiSelectArea(MotionEvent event,int imageLeft,int imageTop){
        switch(event.getAction()){
            case MotionEvent.ACTION_DOWN:
                currentPath = new MySelectPath(xiaoChuBiWidth,(int)(xiaoChuBiWidth/scale));
                addXiaochuBiTouchPoints(event,currentPath,imageLeft,imageTop);
                break;
            case MotionEvent.ACTION_MOVE:
                addXiaochuBiTouchPoints(event,currentPath,imageLeft,imageTop);
                break;
            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_CANCEL:
                addXiaochuBiTouchPoints(event,currentPath,imageLeft,imageTop);
                if(pathEndPosition==-1){
                    pathEndPosition = 0;
                    paths.clear();
                    paths.add(currentPath);
                }else{
                    for(int i=paths.size()-1;i>pathEndPosition;i--){
                        paths.remove(i);
                    }
                    paths.add(currentPath);
                    pathEndPosition = pathEndPosition+1;
                }
                xiaoChuBiView.xiaoChuBiSetSectlectAreaStep(true,false);
                xiaoChuBiView.invalidateView();
                break;
        }
        xiaoChuBiView.invalidateView();
        return true;
    }
    //拖动选择替换选区
    public boolean onXiaoChuBiSelectReplaceArea(Bitmap bitmapInit,float scale,MotionEvent event){
        switch(event.getAction()){
            case MotionEvent.ACTION_DOWN:
                lastX = event.getRawX();
                lastY = event.getRawY();
                break;
            case MotionEvent.ACTION_MOVE:
            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_CANCEL:
                xiaochuX = xiaochuX+event.getRawX()-lastX;
                xiaochuY= xiaochuY+event.getRawY()-lastY;
                lastX = event.getRawX();
                lastY = event.getRawY();
                setXiaochuBi();
                break;
        }
        return true;
    }
    private void addXiaochuBiTouchPoints(MotionEvent event, MySelectPath path, int imageLeft, int imageTop){
        int x = (int)event.getX();
        int y = (int)event.getY();
        Point point = new Point();
        point.x = x;
        point.y = y;
        Point point2 = new Point();

        point2.x = (x-imageLeft)/scale;
        point2.y = (y-imageTop)/scale;
        Log.e("------------------>","point2.x ="+point2.x);
        Log.e("------------------>","point2.y ="+point2.y);

        MyPoint xiaoChuBiPoint = new MyPoint();
        xiaoChuBiPoint.setPointShow(point);
        xiaoChuBiPoint.setPointXiaoChu(point2);
        path.getPathPoints().add(xiaoChuBiPoint);
    }
    public void setXiaochuBiSelectReplaceArea(Context context){
        if(xiaoChuBiStep==1){
            if(pathEndPosition==-1){
                Toast.makeText(context,"请用手指涂抹瑕疵区域",Toast.LENGTH_SHORT).show();
                return;
            }else{
                xiaoChuBiStep = 2;
                xiaochuX = 0;
                xiaochuY = 0;
                Toast.makeText(context,"请用拖动选择替换区域",Toast.LENGTH_SHORT).show();
            }
        }
    }
    /**
     * 设置消除笔
     */
    public void setXiaochuBi(){
        if(pathEndPosition!=-1){
            if(bitmapXiaoChu!=null){
                bitmapXiaoChu.recycle();
                bitmapXiaoChu=null;
            }
            bitmapXiaoChu = Bitmap.createBitmap(bitmapInit.getWidth(),bitmapInit.getHeight(), Bitmap.Config.ARGB_8888);
            OpenCVUtil.inpaintBlurAllPicture(bitmapInit,bitmapXiaoChu,paths,pathEndPosition,(int)(xiaochuX/scale),(int)(xiaochuY/scale),(int)(xiaoChuBiWidth/scale),(int)(xiaoChuBiBorderWidth/scale));
            xiaoChuBiView.invalidateView();
        }
    }

    @Override
    public void getData(String s) {

    }
}
