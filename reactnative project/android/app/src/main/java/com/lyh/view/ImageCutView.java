package com.lyh.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PointF;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.View;

import com.lyh.mvp.xiaochubi.XiaoChuParsener;
import com.lyh.mvp.yingyangse.YingYangSeParsener;
import com.lyh.util.OpenCVUtil;
import com.lyh.util.common;
import com.test.R;
import com.test.RemapHelper;

import org.opencv.android.Utils;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;
import org.opencv.utils.Converters;

import java.util.ArrayList;

public class ImageCutView extends View{

    private Context context;
    private Bitmap bitmap;//bitmap 原图
    private Bitmap bitmapInit;//初始图片
    private Bitmap bitmapJiaoZheng;//矫正后的图片
    private Bitmap bitmapTiaoSe;//调色后的图片
    private Bitmap bitmapYinYangSeTemp;//阴阳色临时图片
    private Bitmap bitmapYinYangSe;//阴阳色调整后的图片；
    private Bitmap bitmapBiaochi;//添加标尺后的图片
    private Bitmap bitmapXuanZhuan;//旋转后的图片
    private Bitmap bitmapCaiJian;//裁剪后的图片

    private float bigImagescaleSize;//放大部分缩放区间的尺寸
    private float bigImageScale = 4;//放大部分缩放倍数

    private int width;
    private int height;
    private int bitmapWidth;//图片实际宽度
    private int bitmapHeight;//图片实际高度
    private int imageWidth;//图片显示宽度
    private int imageHeight;//图片显示高度
    private int imageBigWidth;
    private int imageBigHeight;
    private Paint paintForImage;
    private Paint paintForLine;
    private Paint paintForBig;
    private Paint paintForYinYangSe;//阴阳色画笔
    private Paint paintForBiaoChi;//标尺刻度尺绘制

    private int topPadding=100;//图片显示区上边距
    private int bottomPadding=100;//图片显示区下边距
    private int leftPadding = 100;//图片显示区左边距
    private int rightPadding = 100;//图片显示区右边距


    private int imageLeftPadding;//图片左边距
    private int imageTopPadding;//图片上边距

    private  float[] points = new float[8];
    private PointF[] circles = new PointF[8];//矫正圆
    private int position;//点击的拖动点位置

    private float initX;//按下时x坐标
    private float initY;//按下时y坐标

    private float x;//移动时候的坐标
    private float y;//移动时候的坐标
    private boolean isOnTouch;//是否响应触摸事件
    private int circleR;//触摸圆点半径
    private float scale;//图片显示尺寸与原图尺寸的比例
    private int CURRENT_OPTION_TYPE;//当前操作类型 0 未操作，1.矫正 ；2 调色 ；3 消除笔；4 阴阳色 ；5 添加标尺;6旋转 ；7 裁剪
    private int current_jiaozheng_type;// 0 未操作 ；1 操作中 ；2 已矫正

    private int tiaoSeHue;//调色 色相角度
    private int tiaoSeLight;//调色 图片亮度、
    private float tiaoSeBaipingheng;//调节白平衡

    private float lightPart=1;//局部调亮亮度

    private boolean isDown;//手指是否按下

    private float onTouchX;
    private float onTouchY;

    private final int[] sizes= new int[]{500,600,800,1000,1200};

    private int DEFAULT_MAX_WITH = 2600;//默认最大边长

    private int yinYangSeLocationX=-1;
    private int yinYangSeLocationY=-1;
    private int yinYangSeR;//半径
    private boolean isYinYangSeDragedPosition;

    private int xiaoChuBiSetLocationX;
    private int xiaoChuBiSetLocaitonY;
    private float xiaochuX;
    private float xiaochuY;

    private int xiaoChuBureWidth=3;//边缘模糊范围

    private int xiaoChuBiR;//消除笔半径
    private int default_biaochi_length = 1200;//
    private int biaoChiWidth;
    private int biaoChiHeight;
    private int biaochiBackgroudnWidth;//标尺背景宽度
    private int biaochiKeduShort;//短刻度
    private int biaochiKeduMiddle;//中等刻度
    private int biaochiKeduLong;//长刻度
    private Handler handler;

    private XiaoChuParsener xiaoChuBiParsener;
    private YingYangSeParsener yingYangSeParsener;
    public ImageCutView(Context context) {
        super(context);
    }

    public ImageCutView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context =context;
        init();
    }

    public ImageCutView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }
    private void init(){
        initHandler();
        paintForImage = new Paint();
        paintForImage.setAntiAlias(true);

        paintForLine = new Paint();
        paintForLine.setAntiAlias(true);
        paintForLine.setStrokeWidth(common.Dp2Px(getContext(), 2));
        paintForLine.setColor(Color.parseColor("#ffffff"));

        paintForBig = new Paint();
        paintForBig.setAntiAlias(true);
        paintForBig.setStrokeWidth(common.Dp2Px(getContext(), 2));
        paintForBig.setStyle(Paint.Style.STROKE);
        paintForBig.setColor(Color.parseColor("#60000000"));
        circleR = common.Dp2Px(context,5 );
        bigImagescaleSize = 4*circleR;
        imageBigHeight =imageBigWidth = (int)(bigImagescaleSize*bigImageScale);

        xiaoChuBiR = common.Dp2Px(getContext(),15);
        paintForYinYangSe = new Paint();
        paintForYinYangSe.setAntiAlias(true);
        paintForYinYangSe.setStrokeWidth(common.Dp2Px(getContext(), 4));
        paintForYinYangSe.setStyle(Paint.Style.STROKE);
        paintForYinYangSe.setTextSize(sp2px(20));
        paintForYinYangSe.setColor(getContext().getResources().getColor(R.color.color4CB2BD));

        yinYangSeR = common.Dp2Px(getContext(),20);
        paintForBiaoChi = new Paint();
        paintForBiaoChi.setAntiAlias(true);
        paintForBiaoChi.setStyle(Paint.Style.FILL);
        paintForBiaoChi.setColor(Color.parseColor("#333333"));
        paintForBiaoChi.setTextSize(sp2px(4));
        biaochiBackgroudnWidth = common.Dp2Px(getContext(),16);
        biaochiKeduShort = common.Dp2Px(getContext(),2);
        biaochiKeduMiddle = common.Dp2Px(getContext(),3);
        biaochiKeduLong = common.Dp2Px(getContext(),4);
    }
    private void initHandler(){
        handler = new Handler(){
            @Override
            public void handleMessage(Message msg){
                super.handleMessage(msg);
                switch(msg.what){
                }
            };
        };
    }
    public void onDraw(Canvas canvas){
        super.onDraw(canvas);
        //图片显示区域宽高
        if(bitmap==null){
            return;
        }
        switch (CURRENT_OPTION_TYPE){
            case 0://无选择项
                //绘制图片
                drawBitmap(canvas);
                break;
            case 1://矫正
                //绘制图片
                drawBitmap(canvas);
                //绘制线段
                drawLines(canvas);
                //绘制圆
                drawCircles(canvas);
                if(isDown&&current_jiaozheng_type==1){
                    drawScalePart(canvas);
                }
                break;
            case 2://调色
                //绘制图片
                drawBitmap(canvas);
                break;
            case 3://消除笔
                //绘制图片
                drawBitmap(canvas);
                xiaoChuBiParsener.xiaoChubiDraw(canvas,width,height);
                break;
            case 4://阴阳色
                //绘制图片
                drawBitmap(canvas);
                //绘制阴阳色区域
                drawCircleYinYangArea(canvas);
                //绘制阴阳色放大部分
                drawYinYangScalePart(canvas);
                break;
            case 5://标尺
                //绘制图片
                drawBitmap(canvas);
                drawBiaoChiKeDu(canvas);
                break;
            case 6://旋转
                //绘制图片
                drawBitmap(canvas);
                break;
            case 7://裁剪
                //绘制图片
                drawBitmap(canvas);
                //绘制线段
                drawLines(canvas);
                //绘制圆
                drawCirclesCrop(canvas);
                if(isDown&&current_jiaozheng_type==1){
                    drawScalePart(canvas);
                }
                break;
        }

    }

    //圆顺序：从左到右，从上到下
    private void initCirclesPosition(){
        for(int i=0;i<circles.length;i++){
            circles[i] = new PointF();
        }
        circles[0].x = points[0];
        circles[0].y = points[1];

        circles[1].x = (points[0]+points[2])/2;
        circles[1].y = (points[1]+points[3])/2;

        circles[2].x = points[2];
        circles[2].y = points[3];

        circles[3].x = (points[0]+points[4])/2;
        circles[3].y = (points[1]+points[5])/2;

        circles[4].x = (points[2]+points[6])/2;
        circles[4].y = (points[3]+points[7])/2;

        circles[5].x = points[4];
        circles[5].y = points[5];

        circles[6].x = (points[4]+points[6])/2;
        circles[6].y = (points[5]+points[7])/2;

        circles[7].x = points[6];
        circles[7].y = points[7];
    }
    public void drawBitmap(Canvas canvas){
//        Log.e("---------->","drawBitmap CURRENT_OPTION_TYPE="+CURRENT_OPTION_TYPE);
        switch (CURRENT_OPTION_TYPE){
            case 0:
                canvas.drawBitmapMesh(bitmapInit, 1, 1, points, 0, null, 0, paintForImage);
//                //测试阴阳色
//                Mat srcMat = new Mat();
//                Mat destMat =new Mat();
//                Utils.bitmapToMat(bitmapInit,srcMat);
//                if(bitmapInit==null){
//                    Log.e("--------------->","1 bitmapInit= null");
//                }
//                if(bitmapInit!=null){
//                    Log.e("--------------->","1 bitmapInit！= null");
//                }
//                RemapHelper.setYinYangSe(srcMat.nativeObj,destMat.nativeObj,srcMat.width()/2,srcMat.height()/2,400,-50);
//                if(destMat==null){
//                    Log.e("--------------->","destMat= null");
//                }else{
//                    Log.e("--------------->","destMat!= null");
//                }
//                bitmapYinYangSe= Bitmap.createBitmap(bitmapInit.getWidth(), bitmapInit.getHeight(), Bitmap.Config.ARGB_8888);
//                Utils.matToBitmap(destMat,bitmapYinYangSe);
//                Log.e("--------------->","bitmapInit.width = "+bitmapInit.getWidth());
//                Log.e("--------------->","bitmapYinYangSe.width = "+bitmapYinYangSe.getWidth());
//                canvas.drawBitmapMesh(bitmapYinYangSe, 1, 1, points, 0, null, 0, paintForImage);

//
                break;
            case 1://矫正
                if(bitmapJiaoZheng!=null){
                    canvas.drawBitmapMesh(bitmapJiaoZheng, 1, 1, points, 0, null, 0, paintForImage);
                }else{
                    canvas.drawBitmapMesh(bitmapInit, 1, 1, points, 0, null, 0, paintForImage);
                }
                break;
            case 2:
                if(bitmapTiaoSe!=null){
                    canvas.drawBitmapMesh(bitmapTiaoSe, 1, 1, points, 0, null, 0, paintForImage);
                }else{
                    canvas.drawBitmapMesh(bitmapInit, 1, 1, points, 0, null, 0, paintForImage);
                }
                break;
            case 3:
                if(xiaoChuBiParsener.getXiaoChuBiStep()==2){
                    if(xiaoChuBiParsener.getBitmapXiaoChu()==null){
                        canvas.drawBitmapMesh(bitmapInit, 1, 1, points, 0, null, 0, paintForImage);
                    }else{
                        Log.e("----------------->","绘制消除后的内容");
                        canvas.drawBitmapMesh(xiaoChuBiParsener.getBitmapXiaoChu(), 1, 1, points, 0, null, 0, paintForImage);
                    }
                }else{
                    canvas.drawBitmapMesh(bitmapInit, 1, 1, points, 0, null, 0, paintForImage);
                }
                break;
            case 4:
                if(bitmapYinYangSe!=null){
                    canvas.drawBitmapMesh(bitmapYinYangSe, 1, 1, points, 0, null, 0, paintForImage);
                }else{
                    canvas.drawBitmapMesh(bitmapInit, 1, 1, points, 0, null, 0, paintForImage);
                }
                break;
            case 5:
                canvas.drawBitmapMesh(bitmapInit, 1, 1, points, 0, null, 0, paintForImage);
                break;
            case 6:
                canvas.drawBitmapMesh(bitmapXuanZhuan, 1, 1, points, 0, null, 0, paintForImage);

                break;
            case 7:
                if(bitmapCaiJian!=null){
                    canvas.drawBitmapMesh(bitmapCaiJian, 1, 1, points, 0, null, 0, paintForImage);
                }else{
                    canvas.drawBitmapMesh(bitmapInit, 1, 1, points, 0, null, 0, paintForImage);
                }
                break;
        }

    }
    public void drawLines(Canvas canvas){
        canvas.drawLine(circles[0].x,circles[0].y ,circles[2].x,circles[2].y, paintForLine);
        canvas.drawLine(circles[2].x,circles[2].y ,circles[7].x,circles[7].y, paintForLine);
        canvas.drawLine(circles[7].x,circles[7].y ,circles[5].x,circles[5].y, paintForLine);
        canvas.drawLine(circles[5].x,circles[5].y ,circles[0].x,circles[0].y, paintForLine);
    }
    //绘制矫正圆
    public void drawCircles(Canvas canvas){
        if(bitmap!=null){
            for(int i=0;i<circles.length;i++){
                canvas.drawCircle(circles[i].x,circles[i].y , circleR,paintForLine );
            }
        }
    }
    //绘制裁剪圆
    public void drawCirclesCrop(Canvas canvas){
        if(bitmap!=null){
            for(int i=0;i<circles.length;i++){
                if(i==1||i==3||i==4||i==6){
                    canvas.drawCircle(circles[i].x,circles[i].y , circleR,paintForLine );
                }
            }
        }
    }
    public void drawCircleYinYangArea(Canvas canvas){
        paintForYinYangSe.setStyle(Paint.Style.FILL);
        paintForYinYangSe.setColor(getContext().getResources().getColor(R.color.colorFFFFFF));
        canvas.drawCircle(yinYangSeLocationX,yinYangSeLocationY,yinYangSeR,paintForYinYangSe);
        //绘制圆环
        paintForYinYangSe.setStyle(Paint.Style.STROKE);
        paintForYinYangSe.setStrokeWidth(common.Dp2Px(getContext(),3));
        paintForYinYangSe.setColor(getContext().getResources().getColor(R.color.color4CB2BD));
          canvas.drawArc(yinYangSeLocationX-yinYangSeR,yinYangSeLocationY-yinYangSeR,yinYangSeLocationX+yinYangSeR,yinYangSeLocationY+yinYangSeR,90,180,false,paintForYinYangSe);
        paintForYinYangSe.setColor(getContext().getResources().getColor(R.color.colorA3A3A3));
        canvas.drawArc(yinYangSeLocationX-yinYangSeR,yinYangSeLocationY-yinYangSeR,yinYangSeLocationX+yinYangSeR,yinYangSeLocationY+yinYangSeR,-90,180,false,paintForYinYangSe);
        //绘制文字
        paintForYinYangSe.setStyle(Paint.Style.FILL);
        paintForYinYangSe.setTypeface(Typeface.DEFAULT_BOLD);
        paintForYinYangSe.setColor(getContext().getResources().getColor(R.color.color4CB2BD));
        String text = "亮";
        Rect rect = new Rect();
        paintForYinYangSe.getTextBounds(text, 0, text.length(), rect);
        int width = rect.width();//文本的宽度
        int height = rect.height();//文本的高度
        canvas.drawText("亮",yinYangSeLocationX-width/2,yinYangSeLocationY+height/2,paintForYinYangSe);

    }
    @Override
    public boolean onTouchEvent(MotionEvent event){
        super.onTouchEvent(event);
        if(CURRENT_OPTION_TYPE==1){
           return onJiaoZhengOntouchEvent(event);
        }
        if(CURRENT_OPTION_TYPE==3){
            if(xiaoChuBiParsener.getXiaoChuBiStep()==1){
                xiaoChuBiParsener.onXiaoChuBiSelectArea(event,imageLeftPadding,imageTopPadding);
            }else{
                xiaoChuBiParsener.onXiaoChuBiSelectReplaceArea(bitmapInit,scale,event);
            }
        }
        if(CURRENT_OPTION_TYPE==4){
            return onYinYangSeTouchEvent(event);
        }
        if(CURRENT_OPTION_TYPE==7){
            return onJiaoZhengOntouchEvent(event);
        }
        return true;
    }
    private boolean onJiaoZhengOntouchEvent(MotionEvent event){
        switch(event.getAction()){
            case MotionEvent.ACTION_DOWN:
                isDown = true;
                x = initX = event.getX();
                y = initY = event.getY();
                onTouchX = x;
                onTouchY = y;
                isOnTouch = false;
                position = getOnTouchCirclePosition();
                if(position!=-1){
                    isOnTouch = true;
                    current_jiaozheng_type=1;
                }
                break;
            case MotionEvent.ACTION_MOVE:
                float xNow = event.getX();
                float yNow = event.getY();
                float offsetX = xNow-x;
                float offsetY = yNow-y;
                if(isOnTouch){
                    switch(position){
                        case 0:
                            move0(offsetX,offsetY);
                            break;
                        case 1:
                            move1(offsetX,offsetY);
                            break;
                        case 2:
                            move2(circles[2],offsetX,offsetY);
                            break;
                        case 3:
                            move3(offsetX,offsetY);
                            break;
                        case 4:
                            move4(offsetX,offsetY);
                            break;
                        case 5:
                            move5(circles[5],offsetX,offsetY);
                            break;
                        case 6:
                            move6(offsetX,offsetY);
                            break;
                        case 7:
                            move7(circles[7],offsetX,offsetY);
                            break;
                    }
                    postInvalidate();
                }
                x = xNow;
                y = yNow;
                if(position!=-1){
                    onTouchX = circles[position].x;
                    onTouchY = circles[position].y;
                }
//                if(et_width!=null&&et_heihgt!=null){
//                    setWidthAndHeight();
//                }
                break;
            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_CANCEL:
                isDown = false;
                invalidate();
                break;
        }
        return true;
    }
    private boolean onYinYangSeTouchEvent(MotionEvent event){
        int x = (int)event.getX();
        int y = (int)event.getY();
        switch(event.getAction()){
            case MotionEvent.ACTION_DOWN:
                x = (int)event.getX();
                y = (int)event.getY();
                if(x<imageLeftPadding){
                    yinYangSeLocationX = imageLeftPadding;
                }else if(x>imageLeftPadding+imageWidth){
                    yinYangSeLocationX = imageLeftPadding+imageWidth;
                }else{
                    yinYangSeLocationX = x;
                }
                if(y<imageTopPadding){
                    yinYangSeLocationY = imageTopPadding;
                }else if(y>imageTopPadding+imageHeight){
                    yinYangSeLocationY = imageTopPadding+imageHeight;
                }else{
                    yinYangSeLocationY = y;
                }

                invalidate();
                break;
            case MotionEvent.ACTION_MOVE:
                x = (int)event.getX();
                y = (int)event.getY();
                if(x<imageLeftPadding){
                    yinYangSeLocationX = imageLeftPadding;
                }else if(x>imageLeftPadding+imageWidth){
                    yinYangSeLocationX = imageLeftPadding+imageWidth;
                }else{
                    yinYangSeLocationX = x;
                }
                if(y<imageTopPadding){
                    yinYangSeLocationY = imageTopPadding;
                }else if(y>imageTopPadding+imageHeight){
                    yinYangSeLocationY = imageTopPadding+imageHeight;
                }else{
                    yinYangSeLocationY = y;
                }

                invalidate();
                break;
            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_CANCEL:

                break;
        }
        return true;
    }
    //获取被按下的圆的位置
    private int getOnTouchCirclePosition(){
        int position = -1;
        float distance = 0;//
        if(CURRENT_OPTION_TYPE==1){
            for(int i=0;i<circles.length;i++){
                float offsetX = circles[i].x-initX;
                float offsetY = circles[i].y-initY;
                float distanceCurrent = (float)Math.sqrt(offsetX*offsetX+offsetY*offsetY);
                if(distanceCurrent<= common.Dp2Px(context, 20)){
                    if(position==-1){
                        distance = distanceCurrent;
                        position = i;
                    }else{
                        if(distanceCurrent<=distance){
                            position = i;
                            distance = distanceCurrent;
                        }
                    }
                }

            }
        }
        if(CURRENT_OPTION_TYPE==7){
            for(int i=0;i<circles.length;i++){
                if(i==1||i==3||i==4||i==6){
                    float offsetX = circles[i].x-initX;
                    float offsetY = circles[i].y-initY;
                    float distanceCurrent = (float)Math.sqrt(offsetX*offsetX+offsetY*offsetY);
                    if(distanceCurrent<= common.Dp2Px(context, 20)){
                        if(position==-1){
                            distance = distanceCurrent;
                            position = i;
                        }else{
                            if(distanceCurrent<=distance){
                                position = i;
                                distance = distanceCurrent;
                            }
                        }
                    }
                }
            }
        }

        return position;
    }
    private int getTwoPointsDistance(PointF a,PointF b){
        return (int)Math.sqrt((b.x-a.x)*(b.x-a.x)+(b.y-a.y)*(b.y-a.y));

    }
    //移动点的位置0
    private void move0(float offsetX, float offsetY){
        Line line_2_5 = new Line(circles[2].x,circles[2].y ,circles[5].x ,circles[5].y);
        Line line_2_7 = new Line(circles[2].x,circles[2].y, circles[7].x ,circles[7].y);
        Line line_5_7 = new Line(circles[5].x ,circles[5].y,circles[7].x ,circles[7].y);

        float endX;
        float endY;
        endX = circles[0].x+offsetX;
        endY = circles[0].y+offsetY;
        //第一步 判断是否离开图片显示区
        boolean isInDisplayArea = false;
        if(endX>=points[0]&&endX<=points[2]&&endY>=points[1]&&endY<=points[5]){
            isInDisplayArea = true;
        }
        //第二步 判断相邻点是否越界
        boolean isInPoint = false;
        if(endX<=circles[2].x-4*circleR&&endY<=circles[5].y-4*circleR){
            isInPoint = true;
        }
        //第三步 判断是否越过对角线和是否越过对角边
        boolean isInLine = false;
        if(line_2_5.calculate(endX, endY)>=4*circleR&&line_2_7.calculate(endX, endY)>4*circleR&&line_5_7.calculate(endX, endY)>4*circleR){
            isInLine = true;
        }

        if(isInDisplayArea&&isInPoint&&isInLine){
            circles[0].x = endX;
            circles[0].y = endY;
            circles[1].x = (circles[0].x+circles[2].x)/2;
            circles[1].y = (circles[0].y+circles[2].y)/2;
            circles[3].x = (circles[0].x+circles[5].x)/2;
            circles[3].y = (circles[0].y+circles[5].y)/2;
        }
    }
    //移动点的位置1
    private void move1(float offsetX, float offsetY){
        if(offsetY>=0){//向下拖动
            PointF a= new PointF();
            a.x = circles[0].x;
            a.y = circles[0].y+offsetY;

            PointF b =new PointF();
            b.x = circles[2].x;
            b.y = circles[2].y+offsetY;

            int distance_0_5 = getTwoPointsDistance(a,circles[5]);
            int distance_2_7 = getTwoPointsDistance(b,circles[7]);
            Line line_5_7 = new Line(circles[5].x,circles[5].y,circles[7].x,circles[7].y);
            Line line_2_5 = new Line(circles[2].x,circles[2].y+offsetY,circles[5].x,circles[5].y);
            Line line_0_7 = new Line(circles[0].x,circles[0].y+offsetY,circles[7].x,circles[7].y);

            if(distance_0_5>=4*circleR&&distance_2_7>=4*circleR
                    &&line_5_7.calculate(a.x,a.y)>=4*circleR
                    &&line_5_7.calculate(b.x,b.y)>=4*circleR
                    &&line_2_5.calculate(a.x,a.y)>=4*circleR
                    &&line_0_7.calculate(b.x,b.y)>=4*circleR){
                circles[0].y= circles[0].y+offsetY;
                circles[1].y=circles[1].y+offsetY;
                circles[2].y = circles[2].y+offsetY;
            }
        }else{//向上拖动
            if(circles[0].y+offsetY<points[1]||circles[2].y+offsetY<points[1]){
                if(circles[0].y<=circles[2].y){
                    offsetY = points[1]-circles[0].y;
                }else{
                    offsetY = points[1]-circles[2].y;
                }
            }
            circles[0].y= circles[0].y+offsetY;
            circles[1].y= circles[1].y+offsetY;
            circles[2].y= circles[2].y+offsetY;
        }
        circles[3].y = (circles[0].y+circles[5].y)/2;
        circles[4].y = (circles[2].y+circles[7].y)/2;

    }
    //移动点的位置2
    private void move2(PointF circle, float offsetX, float offsetY){
        Line line_0_7 = new Line(circles[0].x,circles[0].y ,circles[7].x ,circles[7].y);
        Line line_0_5 = new Line(circles[0].x,circles[0].y, circles[5].x ,circles[5].y);
        Line line_5_7 = new Line(circles[5].x ,circles[5].y,circles[7].x ,circles[7].y);
        float endX;
        float endY;
        endX = circle.x+offsetX;
        endY = circle.y+offsetY;
        //第一步 判断是否离开图片显示区
        boolean isInDisplayArea = false;
        if(endX>=points[0]&&endX<=points[2]&&endY>=points[1]&&endY<=points[5]){
            isInDisplayArea = true;
        }
        //第二步 判断相邻点是否越界
        boolean isInPoint = false;
        if(endX>circles[0].x+4*circleR&&endY<circles[7].y-4*circleR){
            isInPoint = true;
        }
        //第三部 判断是否越过对角线
        boolean isInLine = false;
       if(line_0_7.calculate(endX, endY)>=4*circleR&&line_0_5.calculate(endX, endY)>=4*circleR&&line_5_7.calculate(endX, endY)>=4*circleR){
            isInLine = true;
        }
        if(isInDisplayArea&&isInPoint&&isInLine){
            circle.x = endX;
            circle.y = endY;
            circles[1].x = (circles[0].x+circles[2].x)/2;
            circles[1].y = (circles[0].y+circles[2].y)/2;
            circles[4].x = (circles[2].x+circles[7].x)/2;
            circles[4].y = (circles[2].y+circles[7].y)/2;
        }
    }
    //移动点的位置3
    private void move3(float offsetX, float offsetY){
        if(offsetX>=0){//向右拖动
            PointF a= new PointF();
            a.x = circles[0].x+offsetX;
            a.y = circles[0].y;

            PointF b =new PointF();
            b.x = circles[5].x+offsetX;
            b.y = circles[5].y;

            int distance_0_2 = getTwoPointsDistance(a,circles[2]);
            int distance_5_7 = getTwoPointsDistance(b,circles[7]);
            Line line_2_7 = new Line(circles[2].x,circles[2].y,circles[7].x,circles[7].y);
            Line line_2_5 = new Line(circles[2].x,circles[2].y+offsetY,circles[5].x,circles[5].y);
            Line line_0_7 = new Line(circles[0].x,circles[0].y+offsetY,circles[7].x,circles[7].y);

            if(distance_0_2>=4*circleR
                    &&distance_5_7>=4*circleR
                    &&line_2_7.calculate(a.x,a.y)>=4*circleR
                    &&line_2_7.calculate(b.x,b.y)>=4*circleR
                    &&line_2_5.calculate(circles[0].x+offsetX,circles[0].y)>=4*circleR
                    &&line_0_7.calculate(circles[5].x+offsetX,circles[5].y)>=4*circleR
                    ){
                circles[0].x= circles[0].x+offsetX;
                circles[3].x=circles[3].x+offsetX;
                circles[5].x = circles[5].x+offsetX;
            }
        }else{//向左拖动
            if(circles[0].x+offsetX<points[0]||circles[5].x+offsetX<points[0]){
                if(circles[0].x<=circles[5].x){
                    offsetX = points[0]-circles[0].x;
                }else{
                    offsetX = points[0]-circles[5].x;
                }
            }
            circles[0].x= circles[0].x+offsetX;
            circles[3].x=circles[3].x+offsetX;
            circles[5].x = circles[5].x+offsetX;
        }
        circles[1].x = (circles[0].x+circles[2].x)/2;
        circles[6].x = (circles[5].x+circles[7].x)/2;

    }
    //移动点的位置4
    private void move4(float offsetX, float offsetY){
        if(offsetX>=0){//向右拖动
            if(circles[2].x+offsetX>points[2]||circles[7].x+offsetX>points[2]){
                if(circles[2].x>=circles[7].x){
                    offsetX = points[2]-circles[2].x;
                }else{
                    offsetX = points[2]-circles[7].x;
                }
            }
            circles[2].x= circles[2].x+offsetX;
            circles[4].x=circles[4].x+offsetX;
            circles[7].x = circles[7].x+offsetX;
        }else{//向左拖动
            PointF a= new PointF();
            a.x = circles[2].x+offsetX;
            a.y = circles[2].y;

            PointF b =new PointF();
            b.x = circles[7].x+offsetX;
            b.y = circles[7].y;

            int distance_0_2 = getTwoPointsDistance(a,circles[0]);
            int distance_5_7 = getTwoPointsDistance(b,circles[5]);
            Line line_0_5 = new Line(circles[0].x,circles[0].y,circles[5].x,circles[5].y);
            Line line_2_5 = new Line(circles[2].x,circles[2].y+offsetY,circles[5].x,circles[5].y);
            Line line_0_7 = new Line(circles[0].x,circles[0].y+offsetY,circles[7].x,circles[7].y);
            if(distance_0_2>=4*circleR
                    &&distance_5_7>=4*circleR
                    &&line_0_5.calculate(a.x,a.y)>=4*circleR
                    &&line_0_5.calculate(b.x,b.y)>=4*circleR
                    &&line_2_5.calculate(circles[7].x+offsetX,circles[7].y)>=4*circleR
                    &&line_0_7.calculate(circles[5].x+offsetX,circles[5].y)>=4*circleR
                ){
                circles[2].x= circles[2].x+offsetX;
                circles[4].x=circles[4].x+offsetX;
                circles[7].x = circles[7].x+offsetX;
            }
        }
        circles[1].x = (circles[0].x+circles[2].x)/2;
        circles[6].x = (circles[5].x+circles[7].x)/2;

    }
    //移动点的位置5
    private void move5(PointF circle, float offsetX, float offsetY){
        Line line_0_7 = new Line(circles[0].x,circles[0].y ,circles[7].x ,circles[7].y);
        Line line_0_2 = new Line(circles[0].x,circles[0].y, circles[2].x ,circles[2].y);
        Line line_2_7 = new Line(circles[2].x ,circles[2].y,circles[7].x ,circles[7].y);
        float endX;
        float endY;
        endX = circle.x+offsetX;
        endY = circle.y+offsetY;
        //第一步 判断是否离开图片显示区
        boolean isInDisplayArea = false;
        if(endX>=points[0]&&endX<=points[2]&&endY>=points[1]&&endY<=points[5]){
            isInDisplayArea = true;
        }
        //第二步 判断相邻点是否越界
        boolean isInPoint = false;
        if(endX<=circles[7].x-4*circleR&&endY>=circles[0].y+4*circleR){
            isInPoint = true;
        }
        //第三部 判断是否越过对角线
        boolean isInLine = false;
       if(line_0_7.calculate(endX, endY)>=4*circleR&&line_0_2.calculate(endX, endY)>=4*circleR&&line_2_7.calculate(endX, endY)>=4*circleR){
            isInLine = true;
        }
        if(isInDisplayArea&&isInPoint&&isInLine){
            circle.x = endX;
            circle.y = endY;
            circles[3].x = (circles[0].x+circles[5].x)/2;
            circles[3].y = (circles[0].y+circles[5].y)/2;
            circles[6].x = (circles[5].x+circles[7].x)/2;
            circles[6].y = (circles[5].y+circles[7].y)/2;
        }
    }
    //移动点的位置6
    private void move6(float offsetX, float offsetY){
        if(offsetY>=0){//向下拖动
            if(circles[5].y+offsetY>points[5]||circles[7].y+offsetY>points[5]){
                if(circles[5].y>=circles[7].y){
                    offsetY = points[5]-circles[5].y;
                }else{
                    offsetY = points[5]-circles[7].y;
                }
            }
            circles[5].y= circles[5].y+offsetY;
            circles[6].y=circles[6].y+offsetY;
            circles[7].y = circles[7].y+offsetY;
        }else{//向上拖动
            PointF a= new PointF();
            a.x = circles[5].x;
            a.y = circles[5].y+offsetY;

            PointF b =new PointF();
            b.x = circles[7].x;
            b.y = circles[7].y+offsetY;

            int distance_0_5 = getTwoPointsDistance(a,circles[0]);
            int distance_2_7 = getTwoPointsDistance(b,circles[2]);
            Line line_0_2 = new Line(circles[0].x,circles[0].y,circles[2].x,circles[2].y);
            Line line_2_5 = new Line(circles[2].x,circles[2].y+offsetY,circles[5].x,circles[5].y);
            Line line_0_7 = new Line(circles[0].x,circles[0].y+offsetY,circles[7].x,circles[7].y);

            if(distance_0_5>=4*circleR
                    &&distance_2_7>=4*circleR
                    &&line_0_2.calculate(circles[5].x,circles[5].y)>=4*circleR
                    &&line_0_2.calculate(circles[7].x,circles[7].y)>=4*circleR
                    &&line_2_5.calculate(circles[7].x,circles[7].y+offsetY)>=4*circleR
                    &&line_0_7.calculate(circles[5].x,circles[5].y+offsetY)>=4*circleR){
                circles[5].y= circles[5].y+offsetY;
                circles[6].y=circles[6].y+offsetY;
                circles[7].y = circles[7].y+offsetY;
            }
        }
        circles[3].y = (circles[0].y+circles[5].y)/2;
        circles[4].y = (circles[2].y+circles[7].y)/2;
    }
    //移动点的位置7
    private void move7(PointF circle, float offsetX, float offsetY){
        Line line_2_5 = new Line(circles[2].x,circles[2].y ,circles[5].x ,circles[5].y);
        Line line_0_2 = new Line(circles[0].x,circles[0].y, circles[2].x ,circles[2].y);
        Line line_0_5 = new Line(circles[0].x ,circles[0].y,circles[5].x ,circles[5].y);
        float endX;
        float endY;
        endX = circle.x+offsetX;
        endY = circle.y+offsetY;
        boolean isRightLine1 = false;
        boolean isBootomLine2 = false;
        //第一步 判断是否离开图片显示区
        boolean isInDisplayArea = false;
        if(endX>=points[0]&&endX<=points[2]&&endY>=points[1]&&endY<=points[5]){
            isInDisplayArea = true;
        }
        //第二步 判断相邻点是否越界
        boolean isInPoint = false;
        if(endX>=circles[5].x+4*circleR&&endY>=circles[2].y+4*circleR){
            isInPoint = true;
        }
        //第三部 判断是否越过对角线
        boolean isInLine = false;
       if(line_2_5.calculate(endX, endY)>=4*circleR&&line_0_2.calculate(endX, endY)>=4*circleR&&line_0_5.calculate(endX, endY)>=4*circleR){
            isInLine = true;
        }
        if(isInDisplayArea&&isInPoint&&isInLine){
            circle.x = endX;
            circle.y = endY;
            circles[4].x = (circles[7].x+circles[2].x)/2;
            circles[4].y = (circles[7].y+circles[2].y)/2;
            circles[6].x = (circles[5].x+circles[7].x)/2;
            circles[6].y = (circles[5].y+circles[7].y)/2;
        }
    }
    public void setXiaoChuBiParsener(XiaoChuParsener xiaoChuBiParsener) {
        this.xiaoChuBiParsener = xiaoChuBiParsener;
    }

    public YingYangSeParsener getYingYangSeParsener() {
        return yingYangSeParsener;
    }

    private class Line{
        //假设直线为一元一次方程 ：AX+By+C = 0
        private boolean isY;//y值固定  直线 y = NY
        private float Y;//Y值固定时Y的值
        private boolean isX;//x值固定  直线 x = N;
        private float X;//X值固定时X的值
        private float A;
        private float B;
        private float C;
        public Line(float x1,float y1,float x2,float y2){
            if(y1==y2){
                isY = true;
                Y = y1;
                A =0;
                B = 1;
                C = -Y;
            }else if(x1==x2){
                isX = true;
                X = x1;
                A=1;
                B=0;
                C = -X;
            }else{
                //假设 y = ax +n;
                float a = (y1-y2)/(x1-x2);
                float n = y1-a*x1;
                A = a;
                B = -1;
                C = n;
            }
        }
        public float calculate(float x,float y){
            float distance = (float) Math.abs((A*x+B*y+C)/Math.sqrt(A*A+B*B));
            return distance ;
        }
    }
    public void setBitmap(Bitmap bitmap){
        this.bitmapInit = this.bitmap = bitmap;
        setInitInfo();
    }
    private void setInitInfo(){
        if(bitmapXuanZhuan!=null){
            bitmapWidth = bitmapXuanZhuan.getWidth();
            bitmapHeight = bitmapXuanZhuan.getHeight();
        }else{
            bitmapWidth = bitmapInit.getWidth();
            bitmapHeight = bitmapInit.getHeight();
        }
        setImagePosition();
        initCirclesPosition();
        invalidate();
    }
    private void setRotateInfo(){
        setInitInfo();
    }
    private int getRemapedWidth(){
        float h;
        float w;
        w = (circles[2].x-circles[0].x)/scale;
        h = (circles[2].y-circles[0].y)/scale;
        int widthTop = (int)Math.sqrt(w*w+h*h);
        w = (circles[7].x-circles[5].x)/scale;
        h = (circles[7].y-circles[5].y)/scale;
        int widthbottom = (int)Math.sqrt(w*w+h*h);
        int width = widthTop>=widthbottom?widthTop:widthbottom;
        return width;
    }
    private int getRemapedHeight(){
        float h;
        float w;
        w = (circles[5].x-circles[0].x)/scale;
        h = (circles[5].y-circles[0].y)/scale;
        int heightLeft =(int)Math.sqrt(w*w+h*h);
        w = (circles[7].x-circles[2].x)/scale;
        h = (circles[7].y-circles[2].y)/scale;
        int heightRight = (int)Math.sqrt(w*w+h*h);
        int height = heightLeft>=heightRight?heightLeft:heightRight;
        return height;
    }
//    private void  setPreviewInfo(){
//        bitmapWidth = previewBitmap.getWidth();
//        bitmapHeight = previewBitmap.getHeight();
//        setImagePosition();
//        initCirclesPosition();
//        invalidate();
//    }
    private void setImagePosition(){
        int width_ = width - leftPadding-rightPadding;
        int height_ = height - topPadding-bottomPadding;
        if((float)bitmapWidth/bitmapHeight>=(float)width_/height_){//图片的宽度比例大于图片显示区域的宽度比例，图片上下居中
            scale = (float) width_/bitmapWidth;
            imageWidth = width_;
            imageHeight = (int)(bitmapHeight*scale);
            imageLeftPadding = leftPadding;
            imageTopPadding = topPadding+(height_-imageHeight)/2;
        }else{//图片左右居中
            scale = (float) height_/bitmapHeight;
            imageHeight = height_;
            imageWidth = (int)(bitmapWidth*scale);
            imageLeftPadding = leftPadding+(width_-imageWidth)/2;
            imageTopPadding = topPadding;
        }
        //图片四个顶点
        points[0] = imageLeftPadding;
        points[1] = imageTopPadding;

        points[2] = imageLeftPadding+imageWidth;
        points[3] = imageTopPadding;

        points[4] = imageLeftPadding;
        points[5] = imageTopPadding+imageHeight;

        points[6] = imageLeftPadding+imageWidth;
        points[7] = imageTopPadding+imageHeight;
    }


    private void getJiaozhenBitmap(){
        Mat mat = new Mat();
        Utils.bitmapToMat(bitmapInit,mat);
        //获取映射前坐标
        ArrayList<Point> srcPoints = new ArrayList<Point>();
        srcPoints.add(new Point((circles[0].x-imageLeftPadding)/scale,(circles[0].y-imageTopPadding)/scale));
        srcPoints.add(new Point((circles[2].x-imageLeftPadding)/scale,(circles[2].y-imageTopPadding)/scale));
        srcPoints.add(new Point((circles[5].x-imageLeftPadding)/scale,(circles[5].y-imageTopPadding)/scale));
        srcPoints.add(new Point((circles[7].x-imageLeftPadding)/scale,(circles[7].y-imageTopPadding)/scale));
        //获取映射后的坐标
        float h;
        float w;
        w = (circles[2].x-circles[0].x)/scale;
        h = (circles[2].y-circles[0].y)/scale;
        int widthTop = (int)Math.sqrt(w*w+h*h);
        w = (circles[7].x-circles[5].x)/scale;
        h = (circles[7].y-circles[5].y)/scale;
        int widthbottom = (int)Math.sqrt(w*w+h*h);
        int width = widthTop>=widthbottom?widthTop:widthbottom;
        w = (circles[5].x-circles[0].x)/scale;
        h = (circles[5].y-circles[0].y)/scale;
        int heightLeft =(int)Math.sqrt(w*w+h*h);
        w = (circles[7].x-circles[2].x)/scale;
        h = (circles[7].y-circles[2].y)/scale;
        int heightRight = (int)Math.sqrt(w*w+h*h);
        int height = heightLeft>=heightRight?heightLeft:heightRight;
        ArrayList<Point> dstPoints = new ArrayList<Point>();

        dstPoints.add(new Point(0,0));
        dstPoints.add(new Point(width,0 ));
        dstPoints.add(new Point(0,height));
        dstPoints.add(new Point(width,height));
        Mat warp_dst = new Mat();
        Mat warp_x = Converters.vector_Point_to_Mat(srcPoints, CvType.CV_32F);
        Mat warp_y = Converters.vector_Point_to_Mat(dstPoints, CvType.CV_32F);
        Mat warp_mat = Imgproc.getPerspectiveTransform(warp_x,warp_y);
        Imgproc.warpPerspective(mat, warp_dst ,warp_mat, new Size(new Point(width,height)));
//        Imgproc.warpPerspective(mat, warp_dst ,warp_mat, mat.size());
        if(CURRENT_OPTION_TYPE==1){
            if(bitmapJiaoZheng!=null){
                bitmapJiaoZheng.recycle();
                bitmapJiaoZheng = null;
            }
            bitmapJiaoZheng = Bitmap.createBitmap(warp_dst.width(), warp_dst.height(), Bitmap.Config.ARGB_8888);
            Utils.matToBitmap(warp_dst, bitmapJiaoZheng);
        }
        if(CURRENT_OPTION_TYPE==7){
            if(bitmapCaiJian!=null){
                bitmapCaiJian.recycle();
                bitmapCaiJian = null;
            }
            bitmapCaiJian = Bitmap.createBitmap(warp_dst.width(), warp_dst.height(), Bitmap.Config.ARGB_8888);
            Utils.matToBitmap(warp_dst, bitmapCaiJian);
        }
        OpenCVUtil.releaseMat(mat);
        OpenCVUtil.releaseMat(warp_dst);
        OpenCVUtil.releaseMat(warp_x);
        OpenCVUtil.releaseMat(warp_y);
    }

    /**
     * 旋转 每次逆时针旋转90度。
     */
    public void rotate(){
        Mat mat = new Mat();
        if(bitmapXuanZhuan!=null){
            Utils.bitmapToMat(bitmapXuanZhuan, mat);
        }else{
            Utils.bitmapToMat(bitmapInit, mat);
        }
        Mat dst = new Mat();
        RemapHelper.setRotate(mat.getNativeObjAddr(), dst.getNativeObjAddr(),90);
        if(bitmapXuanZhuan!=null){
            bitmapXuanZhuan.recycle();
            bitmapXuanZhuan = null;
        }
        bitmapXuanZhuan = Bitmap.createBitmap(dst.width(), dst.height(), Bitmap.Config.ARGB_8888);
        Utils.matToBitmap(dst, bitmapXuanZhuan);
        mat.release();
        dst.release();
        mat=null;
        setRotateInfo();
    }
    public void saveBitmap(String path){
//        Bitmap b=bitmap;
//        if(getContext() instanceof MirroPictureActivity){
//            b = bitmap;
//        }else if(getContext() instanceof CutPictureActivity){
//            if(type==0){//未操作
//                b= bitmap;
//            }else if(type==1){//操作中
//                preViewClick();
//                b = previewBitmap;
//            }else if(type==2){//正在预览
//                b =previewBitmap;
//            }
//        }else if(getContext() instanceof MirroPictureActivity){
//            if(type==0){//未操作
//                b= bitmap;
//            }else if(type==1){//操作中
//                preViewClick();
//                b = previewBitmap;
//            }else if(type==2){//正在预览
//                b =previewBitmap;
//            }
//        }else if(getContext() instanceof DuiHuaActivity){
//            if(type==0){//未操作
//                b= bitmap;
//            }else if(type==1){//操作中
//                preViewClick();
//                b = previewBitmap;
//            }else if(type==2){//正在预览
//                b =previewBitmap;
//            }
//        }
//        Mat mat= new Mat();
//        Utils.bitmapToMat(b,mat);
//        Mat temp = new Mat();
//        Imgproc.cvtColor(mat, temp, COLOR_RGBA2BGRA);
//        Imgcodecs.imwrite(path,temp);
//        temp.release();
//        mat.release();
    }
    //放大部分
    private void drawScalePart(Canvas canvas){
        float offsetX;
        if(onTouchX+ common.Dp2Px(getContext(), -60)>0){
            offsetX = common.Dp2Px(getContext(), -60);
        }else{
            offsetX = 0;
        }
        float offsetY;
        if(onTouchY+ common.Dp2Px(getContext(), -60)>0){
            offsetY = common.Dp2Px(getContext(), -60);
        }else{
            offsetY = 0;
        }

        Bitmap bt =getBitmap();
        Bitmap big = Bitmap.createScaledBitmap(bt, imageBigWidth,imageBigHeight,true);
        canvas.drawBitmap(big, onTouchX+offsetX-imageBigWidth/2, onTouchY+offsetY-imageBigHeight/2, paintForImage);
        canvas.drawCircle(onTouchX+offsetX, onTouchY+offsetY , imageBigWidth/2, paintForBig);
        canvas.drawCircle(onTouchX+offsetX, onTouchY+offsetY,circleR,paintForLine);
        switch(position){
            case 0:
                PointF point_circle_0_2 = getCirePoint(circles[0],circles[2]);
                canvas.drawLine(circles[0].x+offsetX,circles[0].y+offsetY ,point_circle_0_2.x+offsetX ,point_circle_0_2.y+offsetY,paintForLine);
                PointF point_circle_0_5 = getCirePoint(circles[0],circles[5]);
                canvas.drawLine(circles[0].x+offsetX,circles[0].y +offsetY,point_circle_0_5.x +offsetX,point_circle_0_5.y+offsetY,paintForLine);
                break;
            case 2:
                PointF point_circle_2_0 = getCirePoint(circles[2],circles[0]);
                canvas.drawLine(circles[2].x+offsetX,circles[2].y +offsetY,point_circle_2_0.x+offsetX ,point_circle_2_0.y+offsetY,paintForLine);
                PointF point_circle_2_7 = getCirePoint(circles[2],circles[7]);
                canvas.drawLine(circles[2].x+offsetX,circles[2].y+offsetY ,point_circle_2_7.x +offsetX,point_circle_2_7.y+offsetY,paintForLine);
                break;
            case 5:
                PointF point_circle_5_0 = getCirePoint(circles[5],circles[0]);
                canvas.drawLine(circles[5].x+offsetX,circles[5].y+offsetY ,point_circle_5_0.x+offsetX ,point_circle_5_0.y+offsetY,paintForLine);
                PointF point_circle_5_7 = getCirePoint(circles[5],circles[7]);
                canvas.drawLine(circles[5].x+offsetX,circles[5].y +offsetY,point_circle_5_7.x+offsetX ,point_circle_5_7.y+offsetY,paintForLine);
                break;
            case 7:
                PointF point_circle_7_2 = getCirePoint(circles[7],circles[2]);
                canvas.drawLine(circles[7].x+offsetX,circles[7].y+offsetY ,point_circle_7_2.x+offsetX ,point_circle_7_2.y+offsetY,paintForLine);
                PointF point_circle_7_5 = getCirePoint(circles[7],circles[5]);
                canvas.drawLine(circles[7].x+offsetX,circles[7].y+offsetY ,point_circle_7_5.x+offsetX,point_circle_7_5.y+offsetY,paintForLine);
                break;
        }
    }
    //放大部分
    private void drawYinYangScalePart(Canvas canvas){
        float offsetX;
        if(yinYangSeLocationX+ common.Dp2Px(getContext(), -60)>0){
            offsetX = common.Dp2Px(getContext(), -60);
        }else{
            offsetX = 0;
        }
        float offsetY;
        if(yinYangSeLocationY+ common.Dp2Px(getContext(), -60)>0){
            offsetY = common.Dp2Px(getContext(), -60);
        }else{
            offsetY = 0;
        }

        Bitmap bt =getYinYangseScaleBitmap();
        Bitmap big = Bitmap.createScaledBitmap(bt, imageBigWidth,imageBigHeight,true);
        canvas.drawBitmap(big, yinYangSeLocationX+offsetX-imageBigWidth/2, yinYangSeLocationY+offsetY-imageBigHeight/2, paintForImage);
        canvas.drawCircle(yinYangSeLocationX+offsetX, yinYangSeLocationY+offsetY , imageBigWidth/2, paintForBig);
        canvas.drawCircle(yinYangSeLocationX+offsetX, yinYangSeLocationY+offsetY,circleR,paintForLine);
    }

    //找到 point1 为圆心的圆与线段的交点
    private PointF getCirePoint(PointF point1 , PointF point2){
        PointF point = new PointF();
        Line line = new Line(point1.x,point1.y ,point2.x ,point2.y);
        if(line.isX){
            point.x = point1.x;
            if(point1.y<point2.y){
                point.y = point1.y+imageBigWidth/2;
            }else{
                point.y = point1.y-imageBigWidth/2;
            }
        }else if(line.isY){
            point.y = point1.y;
            if(point1.x<point2.x){
                point.x = x+imageBigWidth/2;
            }else{
                point.x = x-imageBigWidth/2;
            }
        }else{
            float bigR = imageBigWidth/2;
            float dx = (float)Math.sqrt(bigR*bigR*line.B*line.B/(line.B*line.B+line.A*line.A));
            if(point1.x<point2.x){
                point.x = point1.x+dx;
            }else{
                point.x = point1.x-dx;
            }
            point.y = -line.C/line.B-line.A*point.x/line.B;
        }
        return point;
    }
    private Bitmap getBitmap() {
        int left = (int)((onTouchX-imageLeftPadding-bigImagescaleSize/2)/scale);
        int top = (int)((onTouchY-imageTopPadding -bigImagescaleSize/2)/scale);
        //依据原有的图片丶创建一个新的图片   格式是：Config.ARGB_4444
        Bitmap bt = Bitmap.createBitmap((int)(bigImagescaleSize/scale),(int)(bigImagescaleSize/scale), Bitmap.Config.ARGB_4444);
        //创建一个画布
        Canvas canvas = new Canvas(bt);
        //创建一个画笔
        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        //画笔的颜色
        paint.setAntiAlias(true);
        paint.setFilterBitmap(true);
        //求得圆的半径
        canvas.drawCircle((int)(bigImagescaleSize/(scale*2)), (int)(bigImagescaleSize/(scale*2)), (int)(bigImagescaleSize/(scale*2)), paint);
        //重置画笔
        paint.reset();
        //调用截图图层的方法
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        //画图片
        canvas.drawBitmap(bitmapInit, -left, -top, paint);
        return bt;
    }
    private Bitmap getYinYangseScaleBitmap() {
        int left = (int)((yinYangSeLocationX-imageLeftPadding-bigImagescaleSize/2)/scale);
        int top = (int)((yinYangSeLocationY-imageTopPadding -bigImagescaleSize/2)/scale);
        //依据原有的图片丶创建一个新的图片   格式是：Config.ARGB_4444
        Bitmap bt = Bitmap.createBitmap((int)(bigImagescaleSize/scale),(int)(bigImagescaleSize/scale), Bitmap.Config.ARGB_4444);
        //创建一个画布
        Canvas canvas = new Canvas(bt);
        //创建一个画笔
        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        //画笔的颜色
        paint.setAntiAlias(true);
        paint.setFilterBitmap(true);
        //求得圆的半径
        canvas.drawCircle((int)(bigImagescaleSize/(scale*2)), (int)(bigImagescaleSize/(scale*2)), (int)(bigImagescaleSize/(scale*2)), paint);
        //重置画笔
        paint.reset();
        //调用截图图层的方法
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        //画图片
        if(bitmapYinYangSe!=null){
            canvas.drawBitmap(bitmapYinYangSe, -left, -top, paint);
        }else{
            canvas.drawBitmap(bitmapInit, -left, -top, paint);
        }
        return bt;
    }
    private void setWidthAndHeight(){
        int remapWidth = getRemapedWidth();
        int remapHeight = getRemapedHeight();
//
//        if(remapWidth>remapHeight){
//            et_width.setText(""+DEFAULT_MAX_WITH);
//            et_heihgt.setText(""+remapHeight*DEFAULT_MAX_WITH/remapWidth);
//        }else{
//            et_width.setText(""+remapWidth*DEFAULT_MAX_WITH/remapHeight);
//            et_heihgt.setText(""+DEFAULT_MAX_WITH);
//        }
    }

    /**
     * 代码绘制标尺刻度
     */
    private void drawBiaoChiKeDu(Canvas canvas){
        if(biaoChiWidth==0&&biaoChiHeight==0){
            if(imageWidth>=imageHeight){
                biaoChiWidth = default_biaochi_length;
                biaoChiHeight = imageHeight*default_biaochi_length/imageWidth;
            }else{
                biaoChiHeight = default_biaochi_length;
                biaoChiWidth = imageWidth*default_biaochi_length/imageHeight;
            }
        }
        if(biaoChiWidth>=biaoChiHeight){
            /**
             * 绘制背景
             */
            paintForBiaoChi.setColor(getContext().getResources().getColor(R.color.colorFFFFFF));
            //绘制竖向白色背景
            canvas.drawRect(new Rect(imageLeftPadding-biaochiBackgroudnWidth,imageTopPadding,imageLeftPadding,imageTopPadding+imageHeight+biaochiBackgroudnWidth),paintForBiaoChi);
            //绘制横向白色背景
            canvas.drawRect(new Rect(imageLeftPadding-biaochiBackgroudnWidth,imageTopPadding+imageHeight,imageLeftPadding+imageWidth,imageTopPadding+imageHeight+biaochiBackgroudnWidth),paintForBiaoChi);
            /**
             * 绘制刻度值
             */
            paintForBiaoChi.setColor(getContext().getResources().getColor(R.color.color333333));
            int beishu = biaoChiWidth/1200+1;//1200的多少倍
            int initKeduSize = 10;//每个刻度表示的长度，单位毫米
            int keduSize = initKeduSize*beishu;//每个刻度表示的长度
            float horizontalKeduNumber = (float)biaoChiWidth/keduSize;
            float verticalKeduNumber = (float)biaoChiHeight/keduSize;
            float spaceKeDu = imageHeight/verticalKeduNumber;
            //绘制刻竖向刻度线
            int verticalKeDuNum = (int)verticalKeduNumber;
            for(int i=0;i<=verticalKeDuNum;i++){
                if(keduSize*i%1000==0){
                    paintForBiaoChi.setStrokeWidth(common.Dp2Px(getContext(),1f));
                    canvas.drawLine(imageLeftPadding-biaochiKeduLong,imageTopPadding+imageHeight-i*spaceKeDu,imageLeftPadding,imageTopPadding+imageHeight-i*spaceKeDu,paintForBiaoChi);
                }if(keduSize*i%100==0){
                    paintForBiaoChi.setStrokeWidth(common.Dp2Px(getContext(),0.5f));
                    canvas.drawLine(imageLeftPadding-biaochiKeduMiddle,imageTopPadding+imageHeight-i*spaceKeDu,imageLeftPadding,imageTopPadding+imageHeight-i*spaceKeDu,paintForBiaoChi);
                }else{
                    paintForBiaoChi.setStrokeWidth(common.Dp2Px(getContext(),0.5f));
                    canvas.drawLine(imageLeftPadding-biaochiKeduShort,imageTopPadding+imageHeight-i*spaceKeDu,imageLeftPadding,imageTopPadding+imageHeight-i*spaceKeDu,paintForBiaoChi);
                }
            }
            canvas.save();
            //绘制竖向刻度值
            for(int i=0;i<verticalKeDuNum;i++){
                if(i==0){
                    paintForBiaoChi.setColor(getContext().getResources().getColor(R.color.red));
                    Rect rect = new Rect();
                    String kedu = "0";
                    paintForBiaoChi.getTextBounds(kedu,0,kedu.length(),rect);
                    canvas.drawText(kedu,imageLeftPadding-biaochiKeduLong-rect.width()-common.Dp2Px(getContext(),2),imageTopPadding+imageHeight-i*spaceKeDu+rect.height()/2,paintForBiaoChi);
                }else{
                    if(keduSize*i%1000==0){//米
                        paintForBiaoChi.setColor(getContext().getResources().getColor(R.color.red));
                        Rect rect = new Rect();
                        String kedu = ""+keduSize*i/1000+"000";
                        paintForBiaoChi.getTextBounds(kedu,0,kedu.length(),rect);
                        canvas.drawText(kedu,imageLeftPadding-biaochiKeduLong-rect.width()-common.Dp2Px(getContext(),2),imageTopPadding+imageHeight-i*spaceKeDu+rect.height()/2,paintForBiaoChi);
                    }else if(keduSize*i%100==0){//分米
                        paintForBiaoChi.setColor(getContext().getResources().getColor(R.color.color333333));
                        Rect rect = new Rect();
                        String kedu = ""+keduSize*i/100+"00";
                        paintForBiaoChi.getTextBounds(kedu,0,kedu.length(),rect);
                        canvas.drawText(kedu,imageLeftPadding-biaochiKeduMiddle-rect.width()-common.Dp2Px(getContext(),2),imageTopPadding+imageHeight-i*spaceKeDu+rect.height()/2,paintForBiaoChi);
                    }
                }
            }
            //绘制横向刻度线
            int horizontalKeDuNum = (int)horizontalKeduNumber;
            for(int i=0;i<=horizontalKeDuNum;i++){
                if(keduSize*i%1000==0){
                    paintForBiaoChi.setStrokeWidth(common.Dp2Px(getContext(),1f));
                    canvas.drawLine(imageLeftPadding+i*spaceKeDu,imageTopPadding+imageHeight,imageLeftPadding+i*spaceKeDu,imageTopPadding+imageHeight+biaochiKeduLong,paintForBiaoChi);
                }else if(keduSize*i%100==0){
                    paintForBiaoChi.setStrokeWidth(common.Dp2Px(getContext(),0.5f));
                    canvas.drawLine(imageLeftPadding+i*spaceKeDu,imageTopPadding+imageHeight,imageLeftPadding+i*spaceKeDu,imageTopPadding+imageHeight+biaochiKeduMiddle,paintForBiaoChi);
                }else{
                    paintForBiaoChi.setStrokeWidth(common.Dp2Px(getContext(),0.5f));
                    canvas.drawLine(imageLeftPadding+i*spaceKeDu,imageTopPadding+imageHeight,imageLeftPadding+i*spaceKeDu,imageTopPadding+imageHeight+biaochiKeduShort,paintForBiaoChi);
                }
            }
            //绘制横向刻度值
            for(int i=0;i<horizontalKeDuNum;i++){
                if(i==0){
                    paintForBiaoChi.setColor(getContext().getResources().getColor(R.color.red));
                    Rect rect = new Rect();
                    String kedu = "0";
                    paintForBiaoChi.getTextBounds(kedu,0,kedu.length(),rect);
                    canvas.drawText(kedu,imageLeftPadding+i*spaceKeDu-rect.width()/2,imageTopPadding+imageHeight+rect.height()+biaochiKeduLong+common.Dp2Px(getContext(),2),paintForBiaoChi);
                }else{
                    if(keduSize*i%1000==0){//米
                        paintForBiaoChi.setColor(getContext().getResources().getColor(R.color.red));
                        Rect rect = new Rect();
                        String kedu = ""+keduSize*i/1000+"000";
                        paintForBiaoChi.getTextBounds(kedu,0,kedu.length(),rect);
                        canvas.drawText(kedu,imageLeftPadding+i*spaceKeDu-rect.width()/2,imageTopPadding+imageHeight+rect.height()+biaochiKeduLong+common.Dp2Px(getContext(),2),paintForBiaoChi);
                    }else if(keduSize*i%100==0){//分米
                        paintForBiaoChi.setColor(getContext().getResources().getColor(R.color.color333333));
                        Rect rect = new Rect();
                        String kedu = ""+keduSize*i/100+"00";
                        paintForBiaoChi.getTextBounds(kedu,0,kedu.length(),rect);
                        canvas.drawText(kedu,imageLeftPadding+i*spaceKeDu-rect.width()/2,imageTopPadding+imageHeight+rect.height()+biaochiKeduShort+common.Dp2Px(getContext(),2),paintForBiaoChi);
                    }
                }
            }
            paintForBiaoChi.setColor(getContext().getResources().getColor(R.color.color333333));
            Rect rect = new Rect();
            String unit = "单位:mm";
            paintForBiaoChi.getTextBounds(unit,0,unit.length(),rect);
            canvas.drawText(unit,imageLeftPadding-common.Dp2Px(getContext(),10),imageTopPadding+imageHeight+biaochiBackgroudnWidth-common.Dp2Px(getContext(),2),paintForBiaoChi);
        }else{
            //绘制竖线
            canvas.drawLine(imageLeftPadding,imageTopPadding+imageHeight,imageLeftPadding,imageTopPadding,paintForBiaoChi);
            //绘制横线线
            canvas.drawLine(imageLeftPadding-common.Dp2Px(getContext(),10),imageTopPadding+imageHeight,imageLeftPadding+imageWidth,imageTopPadding+imageHeight,paintForBiaoChi);
        }

    }
    public void onMeasure(int widthSpec,int heightSpec){
        super.onMeasure(widthSpec,heightSpec);
        width = MeasureSpec.getSize(widthSpec);
        height = MeasureSpec.getSize(heightSpec);
        setInitInfo();
        if(CURRENT_OPTION_TYPE==4&&!isYinYangSeDragedPosition){
            isYinYangSeDragedPosition = false;
            yinYangSeLocationX =imageLeftPadding+imageWidth/2;
            yinYangSeLocationY = imageTopPadding+imageHeight/2;
        }
        if(CURRENT_OPTION_TYPE==3){
            xiaoChuBiSetLocationX =imageLeftPadding+imageWidth/2;
            xiaoChuBiSetLocaitonY = imageTopPadding+imageHeight/2;
        }
    }


    public int getCURRENT_OPTION_TYPE() {
        return CURRENT_OPTION_TYPE;
    }

    public void setCURRENT_OPTION_TYPE(int CURRENT_OPTION_TYPE) {
        if(this.CURRENT_OPTION_TYPE==CURRENT_OPTION_TYPE){
            return;
        }
        saveCurrentToBitmapInit();
        this.CURRENT_OPTION_TYPE = CURRENT_OPTION_TYPE;
        switch(this.CURRENT_OPTION_TYPE){
            case 1:
                break;
            case 2:
                tiaoSeHue =0;
                tiaoSeLight=0;
                break;
            case 3:
                xiaoChuBiParsener.resetData();
                break;
            case 4:

                break;
            case 5:
                break;
            case 6:
                break;
            case 7:
                break;
        }
        setInitInfo();
        invalidate();
    }

    /**
     * 矫正图片
     */
    public void setJiaoZheng(){
        getJiaozhenBitmap();
        if(CURRENT_OPTION_TYPE==1){//矫正
            bitmapInit.recycle();
            bitmapInit=null;
            bitmapInit = bitmapJiaoZheng;
            bitmapJiaoZheng =null;
        }
        if(CURRENT_OPTION_TYPE==7){//裁剪
            bitmapInit.recycle();
            bitmapInit=null;
            bitmapInit = bitmapCaiJian;
            bitmapCaiJian =null;
        }
        CURRENT_OPTION_TYPE = 0;
        invalidate();
    }
    /**
     * 调色
     * @param values type=1时设置色相 -180到180
     *               type=2时设置亮度
     */
    public void setHueAndLight(int type,int values){
        if(type==1){
            this.tiaoSeHue = values;
        }else{
            this.tiaoSeLight = values;
        }
        Mat srcMat = new Mat();
        Mat destMat =new Mat();
        Utils.bitmapToMat(bitmapInit,srcMat);
        RemapHelper.setChangeHue(srcMat.nativeObj,destMat.nativeObj,tiaoSeHue,0,tiaoSeLight);
        if(bitmapTiaoSe!=null){
            bitmapTiaoSe.recycle();
            bitmapTiaoSe=null;
        }
        bitmapTiaoSe =Bitmap.createBitmap(destMat.width(),destMat.height(), Bitmap.Config.ARGB_8888);
        Utils.matToBitmap(destMat,bitmapTiaoSe);
        OpenCVUtil.releaseMat(srcMat);
        OpenCVUtil.releaseMat(destMat);
        invalidate();
    }
    /**
     * 设置白平衡
     * @param values type=1时设置白平衡
     *               type=2时设置亮度
     */
    public void setBaipinghengAndLight(int type,int light,float values){
        if(type==1){
            this.tiaoSeBaipingheng = values;
        }else{
            this.tiaoSeLight = light;
        }
        Mat srcMat = new Mat();
        Mat destMat =new Mat();
        Utils.bitmapToMat(bitmapInit,srcMat);
        RemapHelper.setChangeHue(srcMat.nativeObj,destMat.nativeObj,0,0,tiaoSeLight);
        if(bitmapTiaoSe!=null){
            bitmapTiaoSe.recycle();
            bitmapTiaoSe=null;
        }
        bitmapTiaoSe =Bitmap.createBitmap(destMat.width(),destMat.height(), Bitmap.Config.ARGB_8888);
        RemapHelper.setBaipingheng(destMat.nativeObj,tiaoSeBaipingheng);
        Utils.matToBitmap(destMat,bitmapTiaoSe);
        OpenCVUtil.releaseMat(srcMat);
        OpenCVUtil.releaseMat(destMat);
        invalidate();
    }



    //设置阴阳色调节
    public void setYinyangse(float lightPart,boolean isUp){
        if(!isUp){
            if(bitmapYinYangSe!=null) {
                if (bitmapYinYangSeTemp != bitmapYinYangSe) {
                    bitmapYinYangSe.recycle();
                    bitmapYinYangSe = null;
                }
            }

            bitmapYinYangSe= Bitmap.createBitmap(bitmapInit.getWidth(), bitmapInit.getHeight(), Bitmap.Config.ARGB_8888);
            if(bitmapYinYangSeTemp!=null){
                OpenCVUtil.regulateParterLightless(bitmapYinYangSeTemp,bitmapYinYangSe,(int)((yinYangSeLocationX-imageLeftPadding)/scale),(int)((yinYangSeLocationY-imageTopPadding)/scale),(int)(yinYangSeR/scale),lightPart);
            }else{
                OpenCVUtil.regulateParterLightless(bitmapInit,bitmapYinYangSe,(int)((yinYangSeLocationX-imageLeftPadding)/scale),(int)((yinYangSeLocationY-imageTopPadding)/scale),(int)(yinYangSeR/scale),lightPart);
            }
        }else{
            if(bitmapYinYangSeTemp!=null){
                bitmapYinYangSeTemp.recycle();
                bitmapYinYangSeTemp=null;
            }
            bitmapYinYangSeTemp = bitmapYinYangSe;
        }
        invalidate();
    }
    //设置局部亮度值
    public void setPartLight(float lightPart){
        CURRENT_OPTION_TYPE = 0;
        this.lightPart = lightPart;
        Log.e("------------->","lightPart = "+lightPart);
        invalidate();
    }
    //保存当前的状态
    private void saveCurrentToBitmapInit(){
        switch (CURRENT_OPTION_TYPE){
            case 0:
                break;
            case 1:
                if(bitmapJiaoZheng!=null){
                    bitmapInit.recycle();
                    bitmapInit =null;
                    bitmapInit = bitmapJiaoZheng;
                    bitmapJiaoZheng=null;
                }
                break;
            case 2:
                if(bitmapTiaoSe!=null){
                    bitmapInit.recycle();
                    bitmapInit =null;
                    bitmapInit = bitmapTiaoSe;
                    bitmapTiaoSe=null;
                }
                break;
            case 3:
                if(xiaoChuBiParsener.getXiaoChuBiStep()==2&&xiaoChuBiParsener.getBitmapXiaoChu()!=null){
                    bitmapInit.recycle();
                    bitmapInit =null;
                    bitmapInit = xiaoChuBiParsener.getBitmapXiaoChu();
                    xiaoChuBiParsener.setBitmapXiaoChu(null);
                }
                xiaoChuBiParsener.setXiaoChuBiStep(1);
                break;
            case 4:
                if(bitmapYinYangSe!=null){
                bitmapInit.recycle();
                bitmapInit =null;
                bitmapInit = bitmapYinYangSe;
                bitmapYinYangSe=null;
            }
                break;
            case 5:
                if(bitmapBiaochi!=null){
                    bitmapInit.recycle();
                    bitmapInit =null;
                    bitmapInit = bitmapBiaochi;
                    bitmapBiaochi=null;
                }
                break;
            case 6:
                if(bitmapXuanZhuan!=null){
                    bitmapInit.recycle();
                    bitmapInit =null;
                    bitmapInit = bitmapXuanZhuan;
                    bitmapXuanZhuan=null;
                }
                break;
            case 7:
                if(bitmapCaiJian!=null){
                    bitmapInit.recycle();
                    bitmapInit =null;
                    bitmapInit = bitmapCaiJian;
                    bitmapCaiJian=null;
                }
                break;
        }
    }
    public int sp2px(int spVal) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, spVal, this.getContext().getResources().getDisplayMetrics());
    }
    /**
     * 涂抹选区，上一步下一步
     * @param type
     */
    public void xiaoChuBiSelectArea(int type){
        xiaoChuBiParsener.xiaoChuBiSelectArea(type,bitmapInit);
    }

    public Bitmap getBitmapInit() {
        return bitmapInit;
    }

    public float getScale() {
        return scale;
    }


    public void saveXiaoChuBi(){
        if(xiaoChuBiParsener.getBitmapXiaoChu()!=null){
            setCURRENT_OPTION_TYPE(0);
        }
    }

//    public void saveBitmap(OnImageSaveListener onImageSaveListener){
//        Log.e("-------------->","saveBitmap");
//        if(CURRENT_OPTION_TYPE==5&&biaoChiWidth!=0&&biaoChiHeight!=0){
//            String savePath = FileUtil.createFilePathCache(getContext(),"pictures")+System.currentTimeMillis()+".png";
//            String graduationPath = FileUtil.createFilePathCache(getContext(),"graduation")+System.currentTimeMillis()+".png";
//            OpenCVUtil.saveBiaoChiPictureNoGraduation(2,getContext(),bitmapInit,biaoChiWidth,biaoChiHeight,scale,savePath);
//            OpenCVUtil.saveBiaoChiPictureHaveGraduation(2,getContext(),bitmapInit,biaoChiWidth,biaoChiHeight,scale,graduationPath);
//            onImageSaveListener.savePictureGraduation(savePath,graduationPath,biaoChiWidth,biaoChiHeight);
//
//        }else{
//            String savePath = FileUtil.createFilePathCache(getContext(),"pictures")+System.currentTimeMillis()+".png";
//            Bitmap b=bitmapInit;
//            Mat mat= new Mat();
//            Utils.bitmapToMat(b,mat);
//            Mat temp = new Mat();
//            Imgproc.cvtColor(mat, temp, COLOR_RGBA2BGRA);
//            Imgcodecs.imwrite(savePath,temp);
//            temp.release();
//            mat.release();
//            onImageSaveListener.savePictureNoGraduation(savePath);
//        }
//
//    }
    public void setBiaochi(int width,int height){
        biaoChiWidth = width;
        biaoChiHeight = height;
        bitmapBiaochi= Bitmap.createBitmap(bitmapInit.getWidth(), bitmapInit.getHeight(), Bitmap.Config.ARGB_8888);
        OpenCVUtil.getBiaochiBitmap(getContext(),bitmapInit,bitmapBiaochi,biaoChiWidth,biaoChiHeight,scale);
        bitmapInit = bitmapBiaochi;
        bitmapBiaochi =null;
        setInitInfo();
    }
}
